package com.hifive.util;

import android.app.Activity;
import android.content.Context;
import android.provider.Settings.Secure;
import android.telephony.TelephonyManager;

/** 
 * non activity class. only for String getDeviceID(activity) function
 * @author PCH
 *
 */
public class DeviceID {
    /**
     * return the dev ID.
     * if tablet, use ANDROID_ID
     */
    public String getDeviceID (Activity activity) {
        final TelephonyManager tm = (TelephonyManager)activity.getSystemService(Context.TELEPHONY_SERVICE); 
        String deviceID = tm.getLine1Number();
        if(deviceID==null) {
            deviceID = tm.getDeviceId();
            if (deviceID == null) { 
                // using tablet
                deviceID = Secure.getString(activity.getContentResolver(), Secure.ANDROID_ID); 
            }
        }
        return deviceID;
    }

}
