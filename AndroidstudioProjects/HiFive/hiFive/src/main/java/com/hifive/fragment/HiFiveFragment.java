package com.hifive.fragment;

import android.app.ActionBar;
  import android.app.Activity;
  import android.app.AlertDialog;
  import android.content.ContentUris;
  import android.content.ContentValues;
  import android.content.DialogInterface;
  import android.content.Intent;
  import android.database.Cursor;
  import android.net.Uri;
  import android.os.Bundle;
  import android.support.v4.app.Fragment;
  import android.util.Log;
  import android.view.LayoutInflater;
  import android.view.View;
  import android.view.View.OnClickListener;
  import android.view.ViewGroup;
  import android.widget.Button;
  import android.widget.CheckBox;
 import android.widget.CompoundButton;
 import android.widget.ImageView;
 import android.widget.PopupWindow;
  import android.widget.TextView;
  import android.widget.Toast;

  import com.facebook.FacebookRequestError;
  import com.facebook.Request;
  import com.facebook.Response;
  import com.facebook.Session;
  import com.facebook.SessionDefaultAudience;
  import com.facebook.SessionState;
  import com.facebook.UiLifecycleHelper;
  import com.facebook.model.GraphUser;
  import com.facebook.widget.ProfilePictureView;
  import com.hifive.Constants;
  import com.hifive.MainActivity;
  import com.hifive.R;
  import com.hifive.activity.HiFiveInfoActivity;
  import com.hifive.activity.ProfileEditActivity;
  import com.hifive.provider.ProfileProvider;
  import com.hifive.util.CameraUtil;
  import com.hifive.util.FBApiUtil;
  import com.hifive.util.GlobalApplication;


public class HiFiveFragment extends Fragment {
          private static final String LOG_TAG = "hifive.HifiveFragment";

          //private PopupWindow mpopup;
          //private CameraUtil mCamera;



        //private TextView userNameView;
          private UiLifecycleHelper uiHelper;
          private Session.StatusCallback sessionCallback = new Session.StatusCallback() {
              @Override
              public void call(final Session session, final SessionState state, final Exception exception) {
                  onSessionStateChange(session, state, exception);
              }
          };
          private static final String PENDING_ANNOUNCE_KEY = "pendingAnnounce";
          private static final Uri M_FACEBOOK_URL = Uri.parse("http://m.facebook.com");
          private static final int REAUTH_ACTIVITY_CODE = 100;
          private static final String PERMISSION = "publish_actions";

          // views
          public static ProfilePictureView profilePictureView;
          public static ProfilePictureView profilePictureView2;
          private Button btMyEdit;
          private static Button btMyOrg;
          private static Button btMyEmail;
          private static Button btMyPhone;
          private static Button btMyFb;
          private static Button btMyLkdn;
          private static TextView tvMyTitle;
          private static TextView tvMyName;

          private CheckBox cbShLkdnUrl;
          private CheckBox cbShEmail;
          private CheckBox cbShPhone;
          private CheckBox cbShFbId;

          private Button btDbgHF;

          private static Button btRcvrOrg;
          private static Button btRcvrEmail;
          private static Button btRcvrPhone;
          private static Button btRcvrFb;
          private static Button btRcvrLkdn;
          private static Button btRcvrHFInfo;
          private static Button btSave;
          private static Button btDelete;

          private static TextView tvRcvrName;
          private static TextView tvRcvrPos;
          private static TextView tvHifiveInst;
          private static ImageView ivHifiveInst;

          //FBApiUtil fbApiUtil;

          byte[] profilePic;
          //FBApplication app = (FBApplication) getActivity().getApplication();

          private MainActivity activity;

          @Override
          public void onCreate(Bundle savedInstanceState) {
              super.onCreate(savedInstanceState);
              Log.d(LOG_TAG, "onCreate");
              activity = (MainActivity) getActivity();
              uiHelper = new UiLifecycleHelper(getActivity(), sessionCallback);
              uiHelper.onCreate(savedInstanceState);

          }

          @Override
          public void onResume() {
              Log.d(LOG_TAG, "onResume");
              super.onResume();
              uiHelper.onResume();
              //setRecvUI(getActivity());

          }
          @Override
          public void onPause() {
              Log.d(LOG_TAG, "onPause");
              super.onPause();
              uiHelper.onPause();
          }
          @Override
          public void onDestroy() {
              Log.d(LOG_TAG, "onDestroy");
              super.onDestroy();
              uiHelper.onDestroy();
              activity = null;
          }
          @Override
          public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
              Log.d(LOG_TAG, "onCreateView");
              View rootView = inflater.inflate(R.layout.fragment_hifive, container, false);
              hiFiveViewInit(rootView);

              /*
              ActionBar actionBar = getActivity().getActionBar();
              actionBar.setCustomView(R.layout.actionbar_profile);
              ProfilePictureView actProfView= (ProfilePictureView) actionBar.getCustomView().findViewById(R.id.actionbar_profile_pic);
              actProfView.setProfileId(MainActivity.HFID);
              actionBar.setDisplayOptions(ActionBar.DISPLAY_SHOW_CUSTOM
                      | ActionBar.DISPLAY_SHOW_HOME);
              */
              //fbInit();

              return rootView;
          }
      public static void checkProfExst(final Activity mActivity) {
          if(((GlobalApplication) mActivity.getApplication()).getIsProfExist()){
              Log.d(LOG_TAG, "setProfDatas");
              setProfDatas(mActivity);
          }
          else{
              Log.d(LOG_TAG, "open Profile Edit");
              Intent i = new Intent(mActivity, ProfileEditActivity.class);
              mActivity.startActivity(i);
          }
      }
          private static void hideAllRcvrUi() {
              profilePictureView2.setVisibility(View.GONE);
              btRcvrOrg.setVisibility(View.GONE);
              btRcvrEmail.setVisibility(View.GONE);
              btRcvrPhone.setVisibility(View.GONE);
              btRcvrFb.setVisibility(View.GONE);
              btRcvrLkdn.setVisibility(View.GONE);
              btRcvrHFInfo.setVisibility(View.GONE);
              tvRcvrName.setVisibility(View.GONE);
              tvRcvrPos.setVisibility(View.GONE);
              btSave.setVisibility(View.GONE);
              btDelete.setVisibility(View.GONE);
              tvHifiveInst.setVisibility(View.VISIBLE);
              ivHifiveInst.setVisibility(View.VISIBLE);

          }
          // listener for click "edit"
          Button.OnClickListener dbgHFOnClickListener =
              new OnClickListener() {
                  public void onClick(View v) {
                      Toast.makeText(getActivity(), "dbgHF button", Toast.LENGTH_SHORT).show();
                      ((MainActivity) getActivity()).processIntentMock();
                      //Intent i = new Intent(getActivity(), ProfileEditActivity.class);
                      //startActivity(i);

              }
          };

        private void setChBoxListener() {
            cbShLkdnUrl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                       @Override
                                                       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                                                           ((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK,cbShLkdnUrl.isChecked());
                                                           if(isChecked){
                                                               btMyLkdn.setBackgroundResource(R.drawable.linkedinlogo);
                                                           }else{
                                                               btMyLkdn.setBackgroundResource(R.drawable.linkedinlogodis);
                                                           }

                                                       }
                                                   }
            );
            cbShEmail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                     @Override
                                                     public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                                                         ((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_EMAIL,cbShEmail.isChecked());
                                                         if(isChecked){
                                                             btMyEmail.setBackgroundResource(R.drawable.emailicon);
                                                         }else{
                                                             btMyEmail.setBackgroundResource(R.drawable.emailicondis);
                                                         }

                                                     }
                                                 }
            );

            cbShPhone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                     @Override
                                                     public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                                                         ((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_PHONE,cbShPhone.isChecked());
                                                         if(isChecked){
                                                             btMyPhone.setBackgroundResource(R.drawable.phonelogo);
                                                         }else{
                                                             btMyPhone.setBackgroundResource(R.drawable.phonelogodis);
                                                         }
                                                     }
                                                 }
            );

            cbShFbId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

                                                    @Override
                                                    public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
                                                        ((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_FB_ID,cbShFbId.isChecked());
                                                        if(isChecked){
                                                            btMyFb.setBackgroundResource(R.drawable.facebooklogo);
                                                        }else{
                                                            btMyFb.setBackgroundResource(R.drawable.facebooklogodis);
                                                        }
                                                    }
                                                }
            );


        }

        private static void setProfDatas(final Activity mActivity) {
            Log.d(LOG_TAG, "setProfDatas");
            String sTxt=null;
            sTxt=((GlobalApplication) mActivity.getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION);
            if(sTxt!=null)	btMyOrg.setText(sTxt);
            sTxt=((GlobalApplication) mActivity.getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_TITLE);
            sTxt=sTxt+" at";
            if(sTxt!=null)	tvMyTitle.setText(sTxt);
            sTxt=((GlobalApplication) mActivity.getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME);
            sTxt=sTxt+" "+((GlobalApplication) mActivity.getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_LASTNAME);
            if(sTxt!=null)	{
                tvMyName.setText(sTxt);
            }

        }
        private void saveShChBoxVal() {
            //((GlobalApplication) getActivity().getApplication()).setIsShPref(Constants.SHPREFS[0],cbShLkdnUrl.isChecked());
            //((GlobalApplication) getActivity().getApplication()).setIsShPref(Constants.SHPREFS[1],cbShEmail.isChecked());
            //((GlobalApplication) getActivity().getApplication()).setIsShPref(Constants.SHPREFS[2],cbShPhone.isChecked());
            //((GlobalApplication) getActivity().getApplication()).setIsShPref(Constants.SHPREFS[3],cbShFbId.isChecked());

            ((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK,cbShLkdnUrl.isChecked());
            ((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_EMAIL,cbShEmail.isChecked());
            ((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_PHONE,cbShPhone.isChecked());
            ((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_FB_ID,cbShFbId.isChecked());

        }
        private void setShChBoxDefault() {
            cbShEmail.setChecked(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_EMAIL));
            cbShPhone.setChecked(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_PHONE));
            cbShFbId.setChecked(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_FB_ID));
            cbShLkdnUrl.setChecked(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK));
            if(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_EMAIL)){
                btMyEmail.setBackgroundResource(R.drawable.emailicon);
            }else{
                btMyEmail.setBackgroundResource(R.drawable.emailicondis);
            }
            if(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_PHONE)){
                btMyPhone.setBackgroundResource(R.drawable.phonelogo);
            }else{
                btMyPhone.setBackgroundResource(R.drawable.phonelogodis);
            }
            if(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_FB_ID)){
                btMyFb.setBackgroundResource(R.drawable.facebooklogo);
            }else{
                btMyFb.setBackgroundResource(R.drawable.facebooklogodis);
            }
            if(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK)){
                btMyLkdn.setBackgroundResource(R.drawable.linkedinlogo);
            }else{
                btMyLkdn.setBackgroundResource(R.drawable.linkedinlogodis);
            }

         }
         // listener for click "edit"
         Button.OnClickListener myEditOnClickListener =
                 new OnClickListener() {
                     public void onClick(View v) {
                         Toast.makeText(getActivity(), "click profile edit button", Toast.LENGTH_SHORT).show();
                         Intent i = new Intent(getActivity(), ProfileEditActivity.class);
                         startActivityForResult(i, Constants.INTENT_PROFILEEDIT_ACTIVITY);

                     }
                 };

         // listener for click "occupation link"
         Button.OnClickListener myOrgOnClickListener =
                 new OnClickListener() {
                     public void onClick(View v) {
                         String sTxt=((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION_LINK);
                         openURL(sTxt);
                         Toast.makeText(getActivity(), "click occupation link :"+sTxt, Toast.LENGTH_SHORT).show();


                     }
                 };



         // listener for click "email  link"
         Button.OnClickListener myEmailOnClickListener =
                 new OnClickListener() {
                     public void onClick(View v) {
                         Intent mailer = new Intent(Intent.ACTION_SEND);
                         String sTxt=((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_EMAIL);
                         mailer.setType("text/plain");
                         mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{sTxt});
                         mailer.putExtra(Intent.EXTRA_SUBJECT, "");
                         mailer.putExtra(Intent.EXTRA_TEXT, "");
                         startActivity(Intent.createChooser(mailer, "Send email..."));
                         Toast.makeText(getActivity(), "send email:"+sTxt, Toast.LENGTH_SHORT).show();


                     }
                 };
         // listener for click "fphone in  link"
         Button.OnClickListener myPhoneOnClickListener =
                 new OnClickListener() {
                     public void onClick(View v) {
                         String sTxt=((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_PHONE);
                         Intent callIntent = new Intent(Intent.ACTION_DIAL);
                         callIntent.setData(Uri.parse("tel:"+Uri.encode(sTxt.trim())));
                         callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                         startActivity(callIntent);
                     }
                 };
         // listener for click "facbook profile in  link"
         Button.OnClickListener myFbOnClickListener =
                 new OnClickListener() {
                     public void onClick(View v) {
                         FBApiUtil.openFBProfile(getActivity(),MainActivity.HFID);
                     }
                 };
         // listener for click "linked in  link"
         Button.OnClickListener myLkdnOnClickListener =
                 new OnClickListener() {
                     public void onClick(View v) {
                         String sTxt=((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK);
                         openURL(sTxt);
                         Toast.makeText(getActivity(), "click  link :"+sTxt, Toast.LENGTH_SHORT).show();


                     }
                 };






          // listener for click "occupation link"
          Button.OnClickListener occupatioinLinkOnClickListener =
              new OnClickListener() {
                  public void onClick(View v) {
                      String sTxt=((GlobalApplication) getActivity().getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION_LINK);
                      openURL(sTxt);
                      Toast.makeText(getActivity(), "click occupation link :"+sTxt, Toast.LENGTH_SHORT).show();


              }
          };
          // listener for click "linked in  link"
          Button.OnClickListener linkedInLinkOnClickListener =
              new OnClickListener() {
                  public void onClick(View v) {
                      String sTxt=((GlobalApplication) getActivity().getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK);
                      openURL(sTxt);
                      Toast.makeText(getActivity(), "click  link :"+sTxt, Toast.LENGTH_SHORT).show();


              }
          };
          // listener for click "facbook profile in  link"
          Button.OnClickListener fbLinkOnClickListener =
              new OnClickListener() {
                  public void onClick(View v) {
                      String sTxt=((GlobalApplication) getActivity().getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_FB_ID);
                      //FBApiUtil.openFBProfile(getActivity(),MainActivity.HFID);
                      FBApiUtil.openFBProfile(getActivity(),sTxt);

              }
          };
          // listener for click "linked in  link"
          Button.OnClickListener emailLinkOnClickListener =
              new OnClickListener() {
                  public void onClick(View v) {
                      Intent mailer = new Intent(Intent.ACTION_SEND);
                      String sTxt=((GlobalApplication) getActivity().getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_EMAIL);
                      mailer.setType("text/plain");
                      mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{sTxt});
                      mailer.putExtra(Intent.EXTRA_SUBJECT, "");
                      mailer.putExtra(Intent.EXTRA_TEXT, "");
                      startActivity(Intent.createChooser(mailer, "Send email..."));
                      Toast.makeText(getActivity(), "send email:"+sTxt, Toast.LENGTH_SHORT).show();


              }
          };
          // listener for click "facbook profile in  link"
          Button.OnClickListener phoneLinkOnClickListener =
              new OnClickListener() {
                  public void onClick(View v) {
                      String sTxt=((GlobalApplication) getActivity().getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_PHONE);
                      Intent callIntent = new Intent(Intent.ACTION_DIAL);
                      callIntent.setData(Uri.parse("tel:"+Uri.encode(sTxt.trim())));
                      callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                      startActivity(callIntent);
              }
          };
          /*lister for facebook user name button */
          Button.OnClickListener rcvrHFInfoOnClickListener =
                     new OnClickListener() {
                         public void onClick(View v) {
                          Toast.makeText(getActivity(), "click hifive info button", Toast.LENGTH_SHORT).show();
                             Intent i = new Intent(getActivity(), HiFiveInfoActivity.class);
                             startActivity(i);
                         }
                 };
                 /*lister for facebook user name button */
          Button.OnClickListener saveOnClickListener =
                     new OnClickListener() {
                         public void onClick(View v) {
                              insertOrUpdateRcvdHF();
                              clearRcvrInfo();
                         }
                 };
      /*lister for facebook user name button */
      Button.OnClickListener deleteOnClickListener =
              new OnClickListener() {
                  public void onClick(View v) {
                      clearRcvrInfo();
                  }
              };
      private void clearRcvrInfo() {
          hideAllRcvrUi();
          for(int i=0;i<Constants.HFDBNumElmt;i++){
              ((GlobalApplication) getActivity().getApplication()).setRecvHFData(ProfileProvider.Profile.PROJECTION[i + 1], null);
          }

      }
          public void insertOrUpdateRcvdHF(){
              Cursor c = getActivity().getContentResolver().query(
                      ProfileProvider.Profile.CONTENT_URI,
                      ProfileProvider.Profile.PROJECTION,
                      ProfileProvider.Profile.COLUMN_NAME_USERID + "=?",
                      new String[] {((GlobalApplication) getActivity().getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_USERID)},
                      null);
              c.moveToFirst();
              ContentValues values = setContentValues();
              if (c.getCount() != 0) {
                  int update = getActivity().getContentResolver().update(
                          ProfileProvider.Profile.CONTENT_URI,
                          values,
                          ProfileProvider.Profile.COLUMN_NAME_USERID + "=?",
                          new String[] {((GlobalApplication) getActivity().getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_USERID)}
                          );

                  Log.d(LOG_TAG, "update success:"+update);
              }else{
                  Uri tmpUri = getActivity().getContentResolver().insert(
                          ProfileProvider.Profile.CONTENT_URI,
                          values);
                  long myProfID =ContentUris.parseId(tmpUri);
                  Log.d(LOG_TAG, "inserted myProfID:" + String.valueOf(myProfID));
              }
          }
          public ContentValues setContentValues() {

           ContentValues values = new ContentValues();
           for(int i=0;i<Constants.HFDBNumElmt;i++){
               String tmpStr=((GlobalApplication) getActivity().getApplication()).getRecvHFData(ProfileProvider.Profile.PROJECTION[i+1]);
               if(tmpStr!=null){
                   values.put(ProfileProvider.Profile.PROJECTION[i+1], tmpStr);
               }
           }
           return values;
          }
          private void openURL(String URLAddr) {
              if (!URLAddr.startsWith("http://") && !URLAddr.startsWith("https://"))
                  URLAddr = "http://" + URLAddr;
              Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLAddr));
              startActivity(browserIntent);
          }
    private void  setMyCardData(){
        String key;
        String value;
        Log.d(LOG_TAG, "setMyCardData!!");

        key=ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME;
        value=((GlobalApplication) getActivity().getApplication()).getTransHFData(key);
        key=ProfileProvider.Profile.COLUMN_NAME_LASTNAME;
        value=value+" "+((GlobalApplication) getActivity().getApplication()).getTransHFData(key);
        if(value!=null){
            tvMyName.setText(value);
        }


        key=ProfileProvider.Profile.COLUMN_NAME_TITLE;
        value=((GlobalApplication) getActivity().getApplication()).getTransHFData(key);
        if(value!=null){
            //Log.d(LOG_TAG, "set title "+value);
            tvMyTitle.setText(value);
        }

        key=ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION;
        value=((GlobalApplication) getActivity().getApplication()).getTransHFData(key);
        if(value!=null) {
            //Log.d(LOG_TAG, "set btMyOrg "+value);
            btMyOrg.setText(value);
        }
    }

          private static void showRcvrUi(final Activity mActivity) {
              String key;
              String value;


              btRcvrHFInfo.setVisibility(View.VISIBLE);

              key=ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME;
              value=((GlobalApplication) mActivity.getApplication()).getRecvHFData(key);
              key=ProfileProvider.Profile.COLUMN_NAME_LASTNAME;
              value=value+" "+((GlobalApplication) mActivity.getApplication()).getRecvHFData(key);
              if(value!=null){
                  tvRcvrName.setText(value);
              }
              tvRcvrName.setVisibility(View.VISIBLE);

              key=ProfileProvider.Profile.COLUMN_NAME_TITLE;
              value=((GlobalApplication) mActivity.getApplication()).getRecvHFData(key);
              value=value+" at";
              if(value!=null){
                  tvRcvrPos.setText(value);
              }
              tvRcvrPos.setVisibility(View.VISIBLE);

              profilePictureView2.setVisibility(View.VISIBLE);

              key=ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION;
              value=((GlobalApplication) mActivity.getApplication()).getRecvHFData(key);
              if(((GlobalApplication) mActivity.getApplication()).getRecvHFData(key)==null){
                  btRcvrOrg.setVisibility(View.GONE);
              }
              else{
                  btRcvrOrg.setVisibility(View.VISIBLE);
                  btRcvrOrg.setText(value);
              }
              key=ProfileProvider.Profile.COLUMN_NAME_EMAIL;
              if(((GlobalApplication) mActivity.getApplication()).getRecvHFData(key)==null){
                  btRcvrEmail.setVisibility(View.GONE);
              }
              else{
                  btRcvrEmail.setVisibility(View.VISIBLE);
              }
              key=ProfileProvider.Profile.COLUMN_NAME_PHONE;
              if(((GlobalApplication) mActivity.getApplication()).getRecvHFData(key)==null){
                  btRcvrPhone.setVisibility(View.GONE);
              }
              else{
                  btRcvrPhone.setVisibility(View.VISIBLE);
              }
              key=ProfileProvider.Profile.COLUMN_NAME_FB_ID;
              value=((GlobalApplication) mActivity.getApplication()).getRecvHFData(key);
              if(value==null){
                  btRcvrFb.setVisibility(View.GONE);
                  //profilePictureView2.setVisibility(View.GONE);
              }
              else{
                  btRcvrFb.setVisibility(View.VISIBLE);
                  //profilePictureView2.setVisibility(View.VISIBLE);

                  profilePictureView2.setProfileId(value);
              }
              key=ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK;
              if(((GlobalApplication) mActivity.getApplication()).getRecvHFData(key)==null){
                  btRcvrLkdn.setVisibility(View.GONE);
              }
              else{
                  btRcvrLkdn.setVisibility(View.VISIBLE);
              }
              btSave.setVisibility(View.VISIBLE);
              btDelete.setVisibility(View.VISIBLE);
              tvHifiveInst.setVisibility(View.GONE);
              ivHifiveInst.setVisibility(View.GONE);

          }
          public static void viewOrHideRecvUI(final Activity mActivity) {
              String key=ProfileProvider.Profile.COLUMN_NAME_USERID;
              String recvID=((GlobalApplication) mActivity.getApplication()).getRecvHFData(key);
              if(recvID==null) {
                  Log.d(LOG_TAG, "hide recv UI");
                  hideAllRcvrUi();
               }else
               {
                   Log.d(LOG_TAG, "view recv UI :"+recvID);
                   showRcvrUi(mActivity);
                    //profilePictureView2.setVisibility(View.VISIBLE);
                   //fbUserNameButton2.setVisibility(View.VISIBLE);//hide the button before any hifive event
               }
          }

          public static void setRecvUI(final Activity mActivity) {

                //viewOrHideRecvUI(mActivity,((GlobalApplication) mActivity.getApplication()).getRecvHFID());
                viewOrHideRecvUI(mActivity);

          }
          /*handler function that is called after facebook post*/
          public static  void onFBPostComplete(final Activity mActivity) {
               Log.d(LOG_TAG, "onFBPostComplete:"+MainActivity.recvHFID);
               //setRecvUI(mActivity);
          }
        //  private void fbInit() {

       //   	Session session = Session.getActiveSession();
       //       if (session != null && session.isOpened()) {
       //           makeMeRequest(session);
                  /*if(MainActivity.recvHFID!=null)	{
                                                                                                                                                                  Log.d(LOG_TAG, "fbInit recvHFID!=nul 0");
                                                                                                                                                                  if( ((GlobalApplication) getActivity().getApplication()).getPendingHifive()){

                                                                                                                                                                      FBApiUtil.updateUiFromId(getActivity(),((GlobalApplication) getActivity().getApplication()).getRecvHFID(),session) ;
                                                                                                                                                                      //FBApiUtil.handleGraphApiAnnounce(getActivity(),session,((GlobalApplication) getActivity().getApplication()).getHFID(),((GlobalApplication) getActivity().getApplication()).getRecvHFID());
                                                                                                                                                                      ((GlobalApplication) getActivity().getApplication()).setPendingHifive(false);
                                                                                                                                                                  }
                                                                                                                                                              }*/

       //       }

               //updateHfEvent(MainActivity.recvHFID);
      //    }
          private void hiFiveViewInit(View view) {
              Log.d(LOG_TAG, "hiFiveViewInit ");
              profilePictureView = (ProfilePictureView) view.findViewById(R.id.hifive_mycard_profile_pic);
              profilePictureView.setCropped(true);
              profilePictureView.setProfileId(MainActivity.HFID);

              btMyEdit     =  (Button)view.findViewById(R.id.hifive_mycard_edit);
              btMyOrg       =  (Button)view.findViewById(R.id.hifive_mycard_org);
              btMyEmail     =  (Button)view.findViewById(R.id.hifive_mycard_email);
              btMyPhone     =  (Button)view.findViewById(R.id.hifive_mycard_phone);
              btMyFb        =  (Button)view.findViewById(R.id.hifive_mycard_fb);
              btMyLkdn      =  (Button)view.findViewById(R.id.hifive_mycard_lkdn);
              tvMyTitle        =  (TextView)view.findViewById(R.id.hifive_mycard_title);
              tvMyName      =  (TextView)view.findViewById(R.id.hifive_mycard_name);

              cbShEmail = (CheckBox) view.findViewById(R.id.hifive_mycard_email_cb);
              cbShPhone = (CheckBox) view.findViewById(R.id.hifive_mycard_phone_cb);
              cbShLkdnUrl = (CheckBox) view.findViewById(R.id.hifive_mycard_lkdn_cb);
              cbShFbId = (CheckBox) view.findViewById(R.id.hifive_mycard_fb_cb);

              btMyEdit.setOnClickListener(myEditOnClickListener);
              btMyOrg.setOnClickListener(myOrgOnClickListener);
              btMyEmail.setOnClickListener(myEmailOnClickListener);
              btMyPhone.setOnClickListener(myPhoneOnClickListener);
              btMyFb.setOnClickListener(myFbOnClickListener);
              btMyLkdn.setOnClickListener(myLkdnOnClickListener);
              //btSavedProfile  =  (Button)view.findViewById(R.id.hifive_saved_profile);
              btDbgHF   =  (Button)view.findViewById(R.id.hifive_dbgHF);

              //Recveived Card Infos
              profilePictureView2 = (ProfilePictureView) view.findViewById(R.id.hifive_rcvdcard_profile_pic);
              profilePictureView2.setCropped(true);
              btRcvrOrg       =  (Button)view.findViewById(R.id.hifive_rcvdcard_org);
              btRcvrEmail     =  (Button)view.findViewById(R.id.hifive_rcvdcard_email);
              btRcvrPhone     =  (Button)view.findViewById(R.id.hifive_rcvdcard_phone);
              btRcvrFb        =  (Button)view.findViewById(R.id.hifive_rcvdcard_fb);
              btRcvrLkdn      =  (Button)view.findViewById(R.id.hifive_rcvdcard_lkdn);
              btRcvrHFInfo    =  (Button)view.findViewById(R.id.hifive_rcvdcard_hifive);
              btSave          =  (Button)view.findViewById(R.id.hifive_rcvdcard_save);
              btDelete          =  (Button)view.findViewById(R.id.hifive_rcvdcard_delete);
              tvRcvrName   	=  (TextView)view.findViewById(R.id.hifive_rcvdcard_name);
              tvRcvrPos   	=  (TextView)view.findViewById(R.id.hifive_rcvdcard_title);
              tvHifiveInst   	=  (TextView)view.findViewById(R.id.hifive_instruction_text);
              ivHifiveInst   	=  (ImageView)view.findViewById(R.id.hifive_instruction_image);



              btDbgHF.setOnClickListener(dbgHFOnClickListener);
              btRcvrOrg.setOnClickListener(occupatioinLinkOnClickListener);
              btRcvrLkdn.setOnClickListener(linkedInLinkOnClickListener);
              btRcvrPhone.setOnClickListener(phoneLinkOnClickListener);
              btRcvrEmail.setOnClickListener(emailLinkOnClickListener);
              btRcvrFb.setOnClickListener(fbLinkOnClickListener);
              btRcvrHFInfo.setOnClickListener(rcvrHFInfoOnClickListener);
              btSave.setOnClickListener(saveOnClickListener);
              btDelete.setOnClickListener(deleteOnClickListener);

              setChBoxListener();
              setShChBoxDefault();

              //viewOrHideRecvUI(((GlobalApplication) getActivity().getApplication()).getRecvHFID());
              setRecvUI(getActivity());
              //setMyCardData();
              setProfDatas(getActivity());


         }
    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent intent) {
        Log.d(LOG_TAG, "onActivityResult requestCode:"+String.valueOf(requestCode)+"resultCode:"+String.valueOf(resultCode));
        if (resultCode == Activity.RESULT_OK) {
            switch(requestCode){
                case Constants.INTENT_PROFILEEDIT_ACTIVITY:
                       //setMyCardData();
                       setProfDatas(getActivity());
                    break;
            }
        }
    }
         private void onSessionStateChange(final Session session, SessionState state, Exception exception) {
          if (session != null && session.isOpened()) {
              if (state.equals(SessionState.OPENED_TOKEN_UPDATED)) {
                 // tokenUpdated();
              } else {
                  //makeMeRequest(session);
                  //updateHfEvent(MainActivity.recvHFID);
              }
          } else {
              profilePictureView.setProfileId(null);
              //fbUserNameButton.setText("user profile");
              profilePictureView2.setProfileId(null);
              //fbUserNameButton2.setText("user profile");
              //userNameView.setText("user profile");

          }
      }
        // public void  updateHfEvent(String recvHFID) {
      //	   profilePictureView2.setProfileId(recvHFID);
       //  }
         /*request to get basic user info*/
      /*    private void makeMeRequest(final Session session) {
                                                          Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
                                                              @Override
                                                              public void onCompleted(GraphUser user, Response response) {
                                                                  if (session == Session.getActiveSession()) {
                                                                      if (user != null) {
                                                                          profilePictureView.setProfileId(user.getId());
                                                                          fbUserNameButton.setText("    "+user.getFirstName());
                                                                          ((GlobalApplication) getActivity().getApplication()).setUserName(user.getFirstName());

                                                                      }
                                                                  }
                                                                  if (response.getError() != null) {
                                                                      handleError(response.getError());
                                                                  }
                                                              }
                                                          });
                                                          request.executeAsync();

                                                      }
                                                      private void requestPublishPermissions(Session session) {
                                                          if (session != null) {
                                                              Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(this, PERMISSION)
                                                                      // demonstrate how to set an audience for the publish permissions,
                                                                      // if none are set, this defaults to FRIENDS
                                                                      .setDefaultAudience(SessionDefaultAudience.FRIENDS)
                                                                      .setRequestCode(REAUTH_ACTIVITY_CODE);
                                                              session.requestNewPublishPermissions(newPermissionsRequest);
                                                          }
                                                      }
                                                      private void handleError(FacebookRequestError error) {
                                                          DialogInterface.OnClickListener listener = null;
                                                          String dialogBody = null;

                                                          if (error == null) {
                                                              dialogBody = getString(R.string.error_dialog_default_text);
                                                          } else {
                                                              switch (error.getCategory()) {
                                                                  case AUTHENTICATION_RETRY:
                                                                      // tell the user what happened by getting the message id, and
                                                                      // retry the operation later
                                                                      String userAction = (error.shouldNotifyUser()) ? "" :
                                                                              getString(error.getUserActionMessageId());
                                                                      dialogBody = getString(R.string.error_authentication_retry, userAction);
                                                                      listener = new DialogInterface.OnClickListener() {
                                                                          @Override
                                                                          public void onClick(DialogInterface dialogInterface, int i) {
                                                                              Intent intent = new Intent(Intent.ACTION_VIEW, M_FACEBOOK_URL);
                                                                              startActivity(intent);
                                                                          }
                                                                      };
                                                                      break;

                                                                  case AUTHENTICATION_REOPEN_SESSION:
                                                                      // close the session and reopen it.
                                                                      dialogBody = getString(R.string.error_authentication_reopen);
                                                                      listener = new DialogInterface.OnClickListener() {
                                                                          @Override
                                                                          public void onClick(DialogInterface dialogInterface, int i) {
                                                                              Session session = Session.getActiveSession();
                                                                              if (session != null && !session.isClosed()) {
                                                                                  session.closeAndClearTokenInformation();
                                                                              }
                                                                          }
                                                                      };
                                                                      break;

                                                                  case PERMISSION:
                                                                      // request the publish permission
                                                                      dialogBody = getString(R.string.error_permission);
                                                                      listener = new DialogInterface.OnClickListener() {
                                                                          @Override
                                                                          public void onClick(DialogInterface dialogInterface, int i) {
                                                                              //pendingAnnounce = true;
                                                                              requestPublishPermissions(Session.getActiveSession());
                                                                          }
                                                                      };
                                                                      break;

                                                                  case SERVER:
                                                                  case THROTTLING:
                                                                      // this is usually temporary, don't clear the fields, and
                                                                      // ask the user to try again
                                                                      dialogBody = getString(R.string.error_server);
                                                                      break;

                                                                  case BAD_REQUEST:
                                                                      // this is likely a coding error, ask the user to file a bug
                                                                      dialogBody = getString(R.string.error_bad_request, error.getErrorMessage());
                                                                      break;

                                                                  case OTHER:
                                                                  case CLIENT:
                                                                  default:
                                                                      // an unknown issue occurred, this could be a code error, or
                                                                      // a server side issue, log the issue, and either ask the
                                                                      // user to retry, or file a bug
                                                                      dialogBody = getString(R.string.error_unknown, error.getErrorMessage());
                                                                      break;
                                                              }
                                                          }

                                                          new AlertDialog.Builder(getActivity())
                                                                  .setPositiveButton(R.string.error_dialog_button_text, listener)
                                                                  .setTitle(R.string.error_dialog_title)
                                                                  .setMessage(dialogBody)
                                                                  .show();
                                                      }
                                                  */
      }