package com.hifive;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.OptionalDataException;
import java.net.MalformedURLException;
import java.net.URL;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Calendar;
import java.util.Date;

import android.annotation.SuppressLint;
import android.app.AlertDialog;
import android.content.ContentResolver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.PackageManager.NameNotFoundException;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Location;
import android.net.Uri;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.NfcAdapter;
import android.nfc.NfcAdapter.CreateNdefMessageCallback;
import android.nfc.NfcAdapter.OnNdefPushCompleteCallback;
import android.nfc.NfcEvent;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.os.Parcelable;
import android.support.v4.app.ActionBarDrawerToggle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.text.format.DateFormat;
import android.text.format.Time;
import android.util.AttributeSet;
import android.util.Base64;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import com.hifive.util.GPSTracker;
import com.facebook.FacebookRequestError;
import com.facebook.Request;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.UiLifecycleHelper;
import com.facebook.model.GraphUser;

import com.hifive.accounts.GenericAccountService;
//import com.hifive.activity.LoginActivity;
import com.hifive.fragment.HiFiveFragment;
import com.hifive.fragment.LogOffFragment;
import com.hifive.fragment.ProfileListFragment;
import com.hifive.fragment.SplashFragment;
//import com.hifive.fragment.ProfileEditFragment;
import com.hifive.fragment.ProfileFragment;
//import com.hifive.fragment.UserPrefFragment;
import com.hifive.provider.EventProvider;
import com.hifive.provider.ProfileProvider;
import com.hifive.provider.ProfileProvider.Profile;
import com.hifive.util.CameraUtil;
//FIXMELATERVER import com.hifive.provider.ProfileProvider;
import com.hifive.util.EventUtil;
import com.hifive.util.FBApiUtil;
import com.hifive.util.GlobalApplication;
import com.hifive.util.PictureUtil;
import com.hifive.util.SharedPreferencesWrapper;

public class MainActivity extends FragmentActivity implements 
        CreateNdefMessageCallback,
        OnNdefPushCompleteCallback {
    private static final String LOG_TAG = "hifive.MainActivity";
    private static final int MESSAGE_SENT = 1;
    
    public static String HFID;
    public static String  recvHFID=null;
    public static SharedPreferencesWrapper mSharedPref;
    
    // [UI] for Navigation menu
    private DrawerLayout mDrawerLayout;
    private ListView mDrawerList;
    private ActionBarDrawerToggle mDrawerToggle;
    private CharSequence mDrawerTitle;
    private CharSequence mTitle;
    private CameraUtil mCamera;
    private static final Uri M_FACEBOOK_URL = Uri.parse("http://m.facebook.com");
    private boolean mockTog=false;
    private FragmentActivity mainActivty=this;
    Bitmap userProfBitmap=null;
	
   // private String[] recvHFStringMock;
    /*FIXMENEXTVER    private String[] mPageTitles = { "News Feed", "Friend", "Profile", "Developer"};

                            //[UI] fragment pages symbolic
                            public static final int NEWSFEED_PAGE = 0;
                            public static final int FRIEND_PAGE = 1;
                            public static final int PROFILE_PAGE = 2;
                            public static final int DEVELOPER_PAGE = 3;
                         */
    //private String[] mPageTitles = {"LogIn/LogOut","HiFive","User Profile","Saved Profiles"};
    private String[] mPageTitles = {"LogIn/LogOut","HiFive","Saved Profiles"};


    //[UI] fragment pages symbolic
    public static final int SPLASH_PAGE = 0;
    public static final int HIFIVE_PAGE = 1;
    //public static final int PROFILE_PAGE = 2;
    public static final int SAVED_PROFILES_PAGE = 2;
    public static final int LOGOUT_PAGE = 4;
    
    
    public static final int FRAGMENT_COUNT=2;
    private Fragment[] fragments = new Fragment[FRAGMENT_COUNT];
	//use the UiLifecycleHelper to track the session and trigger a session state change listener. 
	//First, create a private variable for the UiLifecycleHelper object and the session state change listener implementation:
    private boolean isResumed = false;
    private boolean userLoggedIn = false;
    private UiLifecycleHelper uiHelper;
	private Session.StatusCallback callback = 
		    new Session.StatusCallback() {
		    @Override
		    public void call(Session session, 
		            SessionState state, Exception exception) {
		        onSessionStateChange(session, state, exception);
		    }
	};


    // [Text] create adapter
    //TextView mInfoText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        /*// Add code to print out the key hash
        try {
            PackageInfo info = getPackageManager().getPackageInfo(
                    "com.facebook.samples.hellofacebook", 
                    PackageManager.GET_SIGNATURES);
            for (Signature signature : info.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                Log.d("KeyHash:", Base64.encodeToString(md.digest(), Base64.DEFAULT));
                }
        } catch (NameNotFoundException e) {

        } catch (NoSuchAlgorithmException e) {

        }*/
        uiHelper = new UiLifecycleHelper(this, callback);
		uiHelper.onCreate(savedInstanceState);
		setContentView(R.layout.activity_main);
		//hideAllFragments();
		//checkLogInStatus();
		//Intent intent = getIntent();
		//HFID = intent.getStringExtra("HFID");
		//((GlobalApplication) getApplication()).setHFID(HFID);

		//Toast.makeText(getApplicationContext(), "Your HF ID is:"+HFID , Toast.LENGTH_SHORT).show();

        // [LOGIN] get the HFID
        //loginInit(); Not used for 1st FB version

        // [UI] Initialize navigation menu and select page NEWSFEED_PAGE
        navigationMenuInit(savedInstanceState);
        // [NFC] Nfc initialization
        Log.d(LOG_TAG, "navigationMenuInit done");
        NfcInit();
        Log.d(LOG_TAG, "NfcInit done");
        // [SYNC] create syncAccount
        //FIXMELATERVER GenericAccountService.CreateSyncAccount(getBaseContext());

        // [SYSTEM] Shared Preferences init
        sharePrefInit();
        transHFDataInit();
        Log.d(LOG_TAG, "onCreate HFID:"+HFID);

    }
    @Override
    protected void onResumeFragments() {
        super.onResumeFragments();
        checkLogInStatus();
    }

   private void checkLogInStatus() {
    	Log.d(LOG_TAG, "checkLogInStatus");
        Session session = Session.getActiveSession();
        if (session != null && session.isOpened()) {
            // if the session is already open, try to show the selection fragment
            selectItem(HIFIVE_PAGE);
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            getUserFBInfo(session);
            Log.d(LOG_TAG, "checkLogInStatus HIFIVE_PAGE");
            userLoggedIn=true;
        } else {
            // otherwise present the splash screen and ask the user to login, unless the user explicitly skipped.
            selectItem(SPLASH_PAGE);
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
            Log.d(LOG_TAG, "checkLogInStatus SPLASH_PAGE");
            userLoggedIn=false;

        }
    }
    /*
    private void showFragment(int fragmentIndex, boolean addToBackStack) {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction transaction = fm.beginTransaction();
        for (int i = 0; i < fragments.length; i++) {
            if (i == fragmentIndex) {
                transaction.show(fragments[i]);
            } else {
                transaction.hide(fragments[i]);
            }
        }
        if (addToBackStack) {
            transaction.addToBackStack(null);
        }
        transaction.commit();
    } */

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        //getMenuInflater().inflate(R.menu.topbar, menu);
        return super.onCreateOptionsMenu(menu);
    }

    /**
     * [LOGIN] if Profile database is empty,
     * ask user to login so app can generate self profile
     */
    /*FIXMELATERVER
    private void loginInit (){
        Cursor c = getContentResolver().query(
                ProfileProvider.Profile.CONTENT_URI,
                ProfileProvider.Profile.PROJECTION,
                ProfileProvider.Profile._ID + "=?",
                new String[] {"1"},
                null);
        c.moveToFirst();

        if (c.getCount() == 0) {
            Log.d(LOG_TAG, "no contact info database, go to login page");
            Intent i = new Intent(this, LoginActivity.class);
            i.putExtra("HFID", HFID);
            startActivityForResult(i, Constants.INTENT_LOGIN_ACTIVITY);
        } else {
            HFID = c.getString(c.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_USERID));
            Log.d(LOG_TAG, "get HFID from existing self-profile:"+HFID);
        }
        Toast.makeText(getApplicationContext(), "Your HF ID is:"+HFID , Toast.LENGTH_SHORT).show();
    }
    */
    private void transHFDataInit (){
        Cursor c = getContentResolver().query(
                ProfileProvider.Profile.CONTENT_URI,
                ProfileProvider.Profile.PROJECTION,
                ProfileProvider.Profile._ID + "=?",
                new String[] {"1"},
                null);
        c.moveToFirst();
    	if (c.getCount() != 0) {
        	for(int i=0;i<Constants.HFDataNumElmt;i++){
        		String tmpStr =c.getString(c.getColumnIndexOrThrow(ProfileProvider.Profile.PROJECTION[i+1]));
        		((GlobalApplication) getApplication()).setTransHFData(ProfileProvider.Profile.PROJECTION[i+1],tmpStr);
        	}
        }

    }
    /* This is called when UiLifecycleHelper detects facebook session state changes such as log in */
    private void onSessionStateChange(Session session, SessionState state, Exception exception) {
    	Log.d(LOG_TAG, "onSessionStateChange HFID:"+HFID);
    	if (isResumed) {
            FragmentManager manager = getSupportFragmentManager();
            int backStackSize = manager.getBackStackEntryCount();
            for (int i = 0; i < backStackSize; i++) {
                manager.popBackStack();
            }
	    	if (state.equals(SessionState.OPENED)) {
	    		//showFragment(MAIN, false);
	    		selectItem(HIFIVE_PAGE);
	    		mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
	    		Log.d(LOG_TAG, "LOCK_MODE_UNLOCKED onSessionStateChange ");
	    		userLoggedIn=true;
	    		//Toast.makeText(getBaseContext(), "sucessfully logged in with Facebook", Toast.LENGTH_SHORT).show();

	        } else if (state.isClosed()) {
	        	//showFragment(SPLASH, false);
	        	selectItem(SPLASH_PAGE);
	        	mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
	        	userLoggedIn=false;
	        	Log.d(LOG_TAG, "LOCK_MODE_LOCKED_CLOSED onSessionStateChange ");
	        	//If not logged in with facebook call LogIn Activity
	        	/*Intent i = new Intent(this, LoginActivity.class);
	            i.putExtra("HFID", HFID);
	            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);//clear all past activity
	            startActivityForResult(i, Constants.INTENT_LOGIN_ACTIVITY);
	        	Log.d(LOG_TAG, "go to logIn activity");*/
	        }
    	}
	}
    /*private void hideAllFragments() {

        FragmentManager fm = getSupportFragmentManager();


        //fragments[HIFIVE_PAGE] = fm.findFragmentById(R.id.fragment_hifive);
        //fragments[PROFILE_PAGE] = fm.findFragmentById(R.id.fragment_profile);
        //fragments[SAVED_PROFILES_PAGE] = fm.findFragmentById(R.id.fragment_profilelist);
        //fragments[LOGOUT_PAGE] = fm.findFragmentById(R.id.fragment_logout);
        fragments[SPLASH] = fm.findFragmentById(R.id.splashFragment);
        fragments[MAIN] = fm.findFragmentById(R.id.mainFragment);


        FragmentTransaction transaction = fm.beginTransaction();
        for(int i = 0; i < fragments.length; i++) {
            transaction.hide(fragments[i]);
        }
        transaction.commit();
    }*/
    /*
   private void checkLogInStatus() {
    	Log.d(LOG_TAG, "checkLogInStatus");
        Session session = Session.getActiveSession();
        if (session != null && session.isOpened()) {
        //if (session != null) {
        	//Toast.makeText(getBaseContext(), "sucessfully logged in with Facebook", Toast.LENGTH_SHORT).show();
    	}else{
            //If not logged in with facebook call LogIn Activity
        	Intent i = new Intent(this, LoginActivity.class);
            i.putExtra("HFID", HFID);
            i.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK | Intent.FLAG_ACTIVITY_CLEAR_TASK);//clear all past activity
            startActivityForResult(i, Constants.INTENT_LOGIN_ACTIVITY);
        	Log.d(LOG_TAG, "not logged in go to logIn activity");
        }
    }*/

    /**
     * [UI] navigation menu: initialization
     */
    private void navigationMenuInit(Bundle savedInstanceState) {
        mTitle = mDrawerTitle = getTitle();
        mDrawerLayout = (DrawerLayout) findViewById(R.id.drawer_layout);
        mDrawerList = (ListView) findViewById(R.id.left_drawer);
        // set a custom shadow that overlays the main content when the drawer opens
        mDrawerLayout.setDrawerShadow(R.drawable.drawer_shadow, GravityCompat.START);
        // set up the drawer's list view with items and click listener
        mDrawerList.setAdapter(new ArrayAdapter<String>(this,
                R.layout.adapter_drawer, mPageTitles));
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());

        // enable ActionBar app icon to behave as action to toggle nav drawer
        getActionBar().setDisplayHomeAsUpEnabled(true);
        getActionBar().setHomeButtonEnabled(true);

        // ActionBarDrawerToggle ties together the the proper interactions
        // between the sliding drawer and the action bar app icon
        mDrawerToggle = new ActionBarDrawerToggle(
                this,                  /* host Activity */
                mDrawerLayout,         /* DrawerLayout object */
                R.drawable.ic_drawer,  /* nav drawer image to replace 'Up' caret */
                R.string.drawer_open,  /* "open drawer" description for accessibility */
                R.string.drawer_close  /* "close drawer" description for accessibility */
                ) {
            public void onDrawerClosed(View view) {
                getActionBar().setTitle(mTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }

            public void onDrawerOpened(View drawerView) {
                getActionBar().setTitle(mDrawerTitle);
                invalidateOptionsMenu(); // creates call to onPrepareOptionsMenu()
            }
        };
        mDrawerLayout.setDrawerListener(mDrawerToggle);

        if (savedInstanceState == null) {
            //selectItem(NEWSFEED_PAGE);//FIXMELATERVER
            //selectItem(HIFIVE_PAGE);//FIXMELATERVER
            selectItem(SPLASH_PAGE);//FIXMELATERVER
            mDrawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);


        }

    }
    /**
     * [UI] navigation menu: The click listener for ListView in the navigation drawer
     */
    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            selectItem(position);
        }
    }

    public void selectItem(int position) {
        // update the main content by replacing fragments
        Fragment fragment = null;
        Log.d(LOG_TAG, "selectItem to : "+ String.valueOf(position));
        ((GlobalApplication) getApplication()).setFragmentPosition(position);
        /*FIXMELATERVER
        switch (position) {
        case NEWSFEED_PAGE:
            fragment = new NewsFeedFragment();
            break;
        case FRIEND_PAGE:
            fragment = new FriendFragment();
            break;
        case PROFILE_PAGE:
            fragment = new ProfileFragment();
            break;
        case DEVELOPER_PAGE:
            fragment = new DeveloperFragment();
            break;
        default:
            break;
        }*/

        switch (position) {
        case SPLASH_PAGE:
        	Log.d(LOG_TAG, "SPLASH_PAGE");
            fragment = new SplashFragment();
            Log.d(LOG_TAG, "SPLASH_PAGE done");
            break;
        case HIFIVE_PAGE:
        	Log.d(LOG_TAG, "HIFIVE_PAGE");
            fragment = new HiFiveFragment();
            Log.d(LOG_TAG, "HIFIVE_PAGE done");
            break;
        //case PROFILE_PAGE:
        //	//fragment = fragmentManager.findFragmentById(R.id.userSettingsFragment);
        //    fragment = new ProfileFragment();
        //    break;
        case SAVED_PROFILES_PAGE:
        	//fragment = fragmentManager.findFragmentById(R.id.userSettingsFragment);
            fragment = new ProfileListFragment();
            break;
        //case LOGOUT_PAGE:
        	//fragment = fragmentManager.findFragmentById(R.id.userSettingsFragment);
        //	fragment = new LogOffFragment();
        //    break;
        default:
            break;
        }
        FragmentManager fragmentManager = getSupportFragmentManager();
        fragmentManager.beginTransaction().replace(R.id.content_frame, fragment).commit();

        // update selected item and title, then close the drawer
        mDrawerList.setItemChecked(position, true);
        setTitle(mPageTitles[position]);
        mDrawerLayout.closeDrawer(mDrawerList);
        Log.d(LOG_TAG, "selectItem finish ");
    }
    public Fragment getVisibleFragment(){
	    FragmentManager fragManager = this.getSupportFragmentManager();
	    int count = this.getSupportFragmentManager().getBackStackEntryCount();
	    Fragment frag = fragManager.getFragments().get(count-1);
	    return frag;
    }
    @Override
    public void setTitle(CharSequence title) {
        mTitle = title;
        getActionBar().setTitle(mTitle);
    }
    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        // Sync the toggle state after onRestoreInstanceState has occurred.
        mDrawerToggle.syncState();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        // Pass any configuration change to the drawer toggals
        mDrawerToggle.onConfigurationChanged(newConfig);
    }

    /**
     *  [NFC] Check available Nfc Adapter.
     *  register the callback for sending Ndef message
     */
    private void NfcInit() {
        NfcAdapter mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        if (mNfcAdapter == null) {
            Toast.makeText(getApplicationContext(), "Sorry NFC is not available on this device", Toast.LENGTH_LONG).show();
        } else {
            // Register callback to set NDEF message
            mNfcAdapter.setNdefPushMessageCallback(this, this);
            // Register callback to listen for message-sent success
            mNfcAdapter.setOnNdefPushCompleteCallback(this, this);
        }
    }

    /**
     * [SYSTEM] check key values stored in share preference
     * if it's empty, init it
     */
    private void sharePrefInit(){
        mSharedPref = new SharedPreferencesWrapper(this);
        if (!mSharedPref.contains(Constants.HFID)) {
        	if(HFID==null)	mSharedPref.putString(Constants.HFID, "");
        	else			mSharedPref.putString(Constants.HFID, HFID);
            Log.d(LOG_TAG, "HFID is saved on pref");
        } else {
        	if(HFID==null)
        	{
        		HFID=mSharedPref.getString(Constants.HFID, "");
        		Log.d(LOG_TAG, "HFID load from shared preference:"+HFID);
        	}
        }
/*FIXMELATERVER
        if (!mSharedPref.contains(Constants.ISFBSHST)) {
        	mSharedPref.putBoolean(Constants.ISFBSHST,((GlobalApplication) getApplication()).getFbShareStatus());
        }else{
        	((GlobalApplication) getApplication()).setFbShareStatus(mSharedPref.getBoolean(Constants.ISFBSHST, true));
        }
        if (!mSharedPref.contains(Constants.ISUPPICST)) {
        	mSharedPref.putBoolean(Constants.ISUPPICST,((GlobalApplication) getApplication()).getUploadPicStatus());
        }else{
        	((GlobalApplication) getApplication()).setUploadPicStatus(mSharedPref.getBoolean(Constants.ISUPPICST, true));
        }
        if (!mSharedPref.contains(Constants.IMGSRC)) {
        	mSharedPref.putInt(Constants.IMGSRC,((GlobalApplication) getApplication()).getImgSrc());
        }else{
        	((GlobalApplication) getApplication()).setImgSrc(mSharedPref.getInt(Constants.IMGSRC, 0));
        }
*/
        //for(int i=0;i<Constants.SHPREFS.length;i++) {
        for(int i=0;i<ProfileProvider.Profile.PRIVACYDATA.length;i++) {
        	        if (!mSharedPref.contains(ProfileProvider.Profile.PRIVACYDATA[i])) {
	        	mSharedPref.putBoolean(ProfileProvider.Profile.PRIVACYDATA[i],((GlobalApplication) getApplication()).getIsShPref(ProfileProvider.Profile.PRIVACYDATA[i]));
	        }else{
	        	((GlobalApplication) getApplication()).setIsShPref(ProfileProvider.Profile.PRIVACYDATA[i],mSharedPref.getBoolean(ProfileProvider.Profile.PRIVACYDATA[i],false));
	        }
        }

        /*FIXMELATERVER
        if (!mSharedPref.contains(Constants.SERVER_SELECT)) {
            mSharedPref.putInt(Constants.SERVER_SELECT, Constants.USE_OPENSHIFT);
        }
        if (!mSharedPref.contains(Constants.CUSTOM_SERVER)) {
            mSharedPref.putString(Constants.CUSTOM_SERVER, "");
            Log.d(LOG_TAG, "CUSTOM SERVER url doens't exist");
        }
        */
    }
    /**
     * [SYSTEM] save share preference
     * if it's empty, init it
     */
    private void sharePrefSave(){

    	Log.d(LOG_TAG,"sharePrefSave");
    	mSharedPref = new SharedPreferencesWrapper(this);
        mSharedPref.putString(Constants.HFID, HFID);
        //mSharedPref.putBoolean(Constants.ISFBSHST, ((GlobalApplication) getApplication()).getFbShareStatus());
        //mSharedPref.putBoolean(Constants.ISUPPICST, ((GlobalApplication) getApplication()).getUploadPicStatus());
        //mSharedPref.putInt(Constants.IMGSRC, ((GlobalApplication) getApplication()).getImgSrc());
        Log.d(LOG_TAG,"sharePrefSave:"+String.valueOf(ProfileProvider.Profile.PRIVACYDATA.length));
        for(int i=0;i<ProfileProvider.Profile.PRIVACYDATA.length;i++) {
	        if (mSharedPref.contains(ProfileProvider.Profile.PRIVACYDATA[i])) {
	        	mSharedPref.putBoolean(ProfileProvider.Profile.PRIVACYDATA[i],((GlobalApplication) getApplication()).getIsShPref(ProfileProvider.Profile.PRIVACYDATA[i]));
	        }
	    }

    }

    private byte[] constHFData() {
    	Log.d(LOG_TAG,"constHFData start 0:");
    	byte[] byteArray=null;
    	Log.d(LOG_TAG,"constHFData start 1:");
        String[] HFString=new String[Constants.HFDataNumElmt];
        for(int i=0;i<Constants.HFDataNumElmt;i++){
        	String tmpStr=((GlobalApplication) getApplication()).getTransHFData(ProfileProvider.Profile.PROJECTION[i+1]);
        	if(i>Constants.HFPrivacyElmtIndSt-2){
        		boolean isSh=((GlobalApplication) getApplication()).getIsShPref(ProfileProvider.Profile.PROJECTION[i+1]);
        		if(isSh){
        			HFString[i]=((GlobalApplication) getApplication()).getTransHFData(ProfileProvider.Profile.PROJECTION[i+1]);
        		}else{
        			HFString[i]=null;
        		}

        	}else{
        		HFString[i]=((GlobalApplication) getApplication()).getTransHFData(ProfileProvider.Profile.PROJECTION[i+1]);
        	}

        }
        HFString[0]=HFID; //FIXME better way when login activity is completed
        	//HFString[0]=HFID;
        	//HFString[1]="abc";
        	//HFString[2]="bcd";
        	//HFString[3]="cde";


            Log.d(LOG_TAG,"constHFData USERID:"+ HFString[0]);
            Log.d(LOG_TAG,"constHFData fN:"+ HFString[1]);
            Log.d(LOG_TAG,"constHFData LN:"+ HFString[2]);
            Log.d(LOG_TAG,"constHFData EM:"+ HFString[3]);
        	byteArray=strArry2bytArry(HFString);
        //} else {
        //	Log.d(LOG_TAG, "No DB found");
        //}
        return byteArray;

    }
    private byte[] strArry2bytArry(String[] inStrArry){
    	byte[] byteArray=null;
    	ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();
    	ObjectOutputStream objectOutputStream;
		try {
			objectOutputStream = new ObjectOutputStream(byteArrayOutputStream);
	    	objectOutputStream.writeObject(inStrArry);
	    	objectOutputStream.flush();
	    	objectOutputStream.close();
	    } catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		byteArray = byteArrayOutputStream.toByteArray();
    	return byteArray;

    }
    private String[] bytArry2strArry(byte[] inByteArry){
    	ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(inByteArry);
    	String[] stringArray2 = new String[Constants.HFDataNumElmt];

		try {
			ObjectInputStream objectInputStream = new ObjectInputStream(byteArrayInputStream);
	    	stringArray2 = (String[]) objectInputStream.readObject();
			objectInputStream.close();
		} catch (OptionalDataException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return stringArray2;

    }
    /**
     * [NFC] Implementation for the CreateNdefMessageCallback interface
     */
    @Override
    public NdefMessage createNdefMessage(NfcEvent event) {

    	byte[] HFData;
    	//HFData[0]=HFID.getBytes();
    	Log.d(LOG_TAG,"createNdefMessage");
    	HFData=constHFData();
    	Log.d(LOG_TAG,"createNdefMessage 1");
        //NdefMessage msg = new NdefMessage(NdefRecord.createMime("application/com.hifive", HFID.getBytes())
        NdefMessage msg = new NdefMessage(NdefRecord.createMime("application/com.hifive", HFData)
        		         /*
          * The Android Application Record (AAR) is commented out. When a device
          * receives a push with an AAR in it, the application specified in the AAR
          * is guaranteed to run. The AAR overrides the tag dispatch system.
          * You can add it back in to guarantee that this
          * activity starts when receiving a beamed message. For now, this code
          * uses the tag dispatch system.
          */
          ,NdefRecord.createApplicationRecord("com.hifive")
        );
        return msg;
    }

    /**
     * [NFC] Implementation for the OnNdefPushCompleteCallback interface
     */
    @Override
    public void onNdefPushComplete(NfcEvent arg0) {
        // A handler is needed to send messages to the activity when this
        // callback occurs, because it happens from a binder thread
        mHandler.obtainMessage(MESSAGE_SENT).sendToTarget();
    }

    /**
     * [NFC] This handler receives a message from onNdefPushComplete
     */
    @SuppressLint("HandlerLeak") private final Handler mHandler = new Handler() {
        @SuppressLint("HandlerLeak") @Override
        public void handleMessage(Message msg) {
            switch (msg.what) {
            case MESSAGE_SENT:
                Toast.makeText(getApplicationContext(), "Message sent!", Toast.LENGTH_SHORT).show();
                break;
            }
        }
    };
    void setRcvrHFData(String[] recvHFString) {
    	for(int i=0;i<Constants.HFDataNumElmt;i++){
    		((GlobalApplication) getApplication()).setRecvHFData(ProfileProvider.Profile.PROJECTION[i+1],recvHFString[i]);
        }
    	Location HFLocation= ((GlobalApplication) getApplication()).getLocation();
    	((GlobalApplication) getApplication()).setRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFLONGITUDE,String.valueOf(HFLocation.getLongitude()));
    	((GlobalApplication) getApplication()).setRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFLATITUDE,String.valueOf(HFLocation.getLatitude()));

    	Time time = new Time();
        time.setToNow();
        String currTime=time.format("20%y/%m/%d/%Hh%Mm%Ss");
        //String currTime=time.toString();


        Date currentDate = Calendar.getInstance().getTime();
        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(this);
        String formattedCurrentDate = dateFormat.format(currentDate);
        ((GlobalApplication) getApplication()).setRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFTIME,currTime);
        //((GlobalApplication) getApplication()).setRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFTIME,formattedCurrentDate);

    }
    /**
     * [NFC] Parses the NDEF Message from the intent
     */
    void processIntent(Intent intent) {
        Parcelable[] rawMsgs = intent.getParcelableArrayExtra(NfcAdapter.EXTRA_NDEF_MESSAGES);
        // only one message sent during the beam
        NdefMessage msg = (NdefMessage) rawMsgs[0];
        //recvHFID = new String(msg.getRecords()[0].getPayload());
        byte[] recvHFData = msg.getRecords()[0].getPayload();
        String[] recvHFString=bytArry2strArry(recvHFData);
        processHFStr(recvHFString);
       /*

        //Toast.makeText(getBaseContext(), "received Hifive !", Toast.LENGTH_SHORT).show();
        //mInfoText.setText("receive HFID: "+recvHFID);
        //FIXMELATERVER String mUriString = EventUtil.add(this, recvHFID);
        //FIXMELATERVER EventUtil.upload2WS(mUriString);
        setHFLocInfo();
        setRcvrHFData(recvHFString);

        recvHFID=recvHFString[0];

        Log.d(LOG_TAG,"rcv USERID:"+ recvHFString[0]);
        Log.d(LOG_TAG,"rcv fN:"+ recvHFString[1]);
        Log.d(LOG_TAG,"rcv LN:"+ recvHFString[2]);
        Log.d(LOG_TAG,"rcv EM:"+ recvHFString[3]);
        Log.d(LOG_TAG,"rcv phone:"+ recvHFString[4]);
        Log.d(LOG_TAG,"rcv fb:"+ recvHFString[5]);
        Log.d(LOG_TAG,"rcv lnkd:"+ recvHFString[6]);
        Log.d(LOG_TAG,"rcv org:"+ recvHFString[7]);


        if(((GlobalApplication)getApplication()).getFragmentPosition()!=HIFIVE_PAGE)	selectItem(HIFIVE_PAGE);//When receive Hifive event go to hifive page

        //((GlobalApplication) getApplication()).setPendingHifive(true);
        ((GlobalApplication) getApplication()).setRecvHFID(recvHFID);
        ((GlobalApplication) getApplication()).setHFID(HFID);
        HiFiveFragment.setRecvUI(this);
        if((((GlobalApplication) getApplication()).getUploadPicStatus())&&(((GlobalApplication) getApplication()).getImgSrc()==Constants.IMGFROMCAM)){
        	mCamera = new CameraUtil(this);
        	startActivityForResult(mCamera.dispatchTakePictureIntent(), Constants.INTENT_CAMERA_ACTIVITY);

        }else{
        	Session session = Session.getActiveSession();
            if (session != null && session.isOpened()) {
        		FBApiUtil.handleFbRequest(this,HFID,recvHFID,null,session) ;
        	}
        }
        */

    }
    void processHFStr(String[] recvHFString){
        setHFLocInfo();
        setRcvrHFData(recvHFString);
        recvHFID=recvHFString[0];
        /*Log.d(LOG_TAG,"rcv USERID:"+ recvHFString[0]);
        Log.d(LOG_TAG,"rcv fN:"+ recvHFString[1]);
        Log.d(LOG_TAG,"rcv LN:"+ recvHFString[2]);
        Log.d(LOG_TAG,"rcv EM:"+ recvHFString[3]);
        Log.d(LOG_TAG,"rcv phone:"+ recvHFString[4]);
        Log.d(LOG_TAG,"rcv fb:"+ recvHFString[5]);
        Log.d(LOG_TAG,"rcv lnkd:"+ recvHFString[6]);
        Log.d(LOG_TAG,"rcv org:"+ recvHFString[7]);
        */
        ((GlobalApplication) getApplication()).setRecvHFID(recvHFID);
        ((GlobalApplication) getApplication()).setHFID(HFID);
        if(((GlobalApplication)getApplication()).getFragmentPosition()!=HIFIVE_PAGE)	selectItem(HIFIVE_PAGE);//When receive Hifive event go to hifive page

        //((GlobalApplication) getApplication()).setPendingHifive(true);

        HiFiveFragment.setRecvUI(this);
        /*if((((GlobalApplication) getApplication()).getUploadPicStatus())&&(((GlobalApplication) getApplication()).getImgSrc()==Constants.IMGFROMCAM)){
        	mCamera = new CameraUtil(this);
        	startActivityForResult(mCamera.dispatchTakePictureIntent(), Constants.INTENT_CAMERA_ACTIVITY);

        }else{
        	Session session = Session.getActiveSession();
            if (session != null && session.isOpened()) {
        		FBApiUtil.handleFbRequest(this,HFID,recvHFID,null,session) ;
        	}
        }*/
    }
    /**
     * [NFC] Parses the NDEF Message from the intent
     */
    public void processIntentMock() {
    	String[] recvHFString=null;
    	if(mockTog){
	        String[] recvHFStringA = {
	        "100005172407577",
	        "Divyanshu",
	        "Jain",
	        "Anaheim Angels",
	        "www.facebook.com/Angels",
	        "Baseball Player",
	        "2139492217",
	        "kolocks@gmail.com",
	        "www.linkedin.com/in/justinykjang",
	        "100005172407577"
	        };
	        mockTog=false;
	        recvHFString=recvHFStringA;
    	}else{
	        String[] recvHFStringB = {
	        "100006295994956",
	        "Aak",
	        "Trout",
	        "Anaheim Angels",
	        "www.facebook.com/Angels",
	        "Baseball Player",
	        "2139492217",
	        "kolocks@gmail.com",
	        "www.linkedin.com/in/justinykjang",
	        "100006295994956"
	        };
	        mockTog=true;
	        recvHFString=recvHFStringB;
    	}
        processHFStr(recvHFString);
    }
    void setHFLocInfo() {
    	 GPSTracker gpsTracker = new GPSTracker(this);
    	 Location location=gpsTracker.getGpsLocation();
    	 ((GlobalApplication) getApplication()).setLocation(location);
    	 if (gpsTracker.canGetLocation())
         {

	    	 String country 			= gpsTracker.getCountryName(this);
	         String city 			= gpsTracker.getLocality(this);
	         //String postalCode 		= gpsTracker.getPostalCode(this);
	         String addressLine 		= gpsTracker.getAddressLine(this);
	         String locString= country+","+city+","+addressLine;
	         //((GlobalApplication) getApplication()).setLocString(locString);
	         Log.d(LOG_TAG,"setHFLocInfo:"+locString);
	         ((GlobalApplication) getApplication()).setRecvHFData(Profile.COLUMN_NAME_HFLOCSTR,locString);

         }else{
        	 Log.d(LOG_TAG,"setHFLocInfo cannot gps");

         }
    }
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        /*
         * [UI] Navigation menu
         * The action bar home/up action should open or close the drawer.
         * ActionBarDrawerToggle will take care of this.
         */

    	if(userLoggedIn)
    	{
	    	if (mDrawerToggle.onOptionsItemSelected(item)) {
	    		return true;
	    	}
	    	switch (item.getItemId()) {
	    	    case R.id.user_prof_pic:
	                Log.d(LOG_TAG,"profile icon");
	                //selectItem(PROFILE_PAGE);

	                return true;

	    	}
    	}
        return super.onOptionsItemSelected(item);
    }


    /**
     * temporary for testing database delete event function
     * will be deprecated
     */
    public void delete() {
        int mDeleteCnt = getContentResolver().delete(EventProvider.Event.CONTENT_URI, null, null);
        Toast.makeText(getApplicationContext(), "delete cnts:"+String.valueOf(mDeleteCnt), Toast.LENGTH_SHORT).show();
    }

    @Override
    public void onResume() {
    	Log.d(LOG_TAG, "onResume HFID:"+HFID);
        super.onResume();
        uiHelper.onResume();
        isResumed = true;
        checkLogInStatus();
        // Check to see that the Activity started due to an Android Beam
        if (NfcAdapter.ACTION_NDEF_DISCOVERED.equals(getIntent().getAction())) {
        	Log.d(LOG_TAG, "on Resume due to NFC"+HFID);
            processIntent(getIntent());
            Log.d(LOG_TAG, "processIntent end");
            setIntent(new Intent());
        }
    }
    @Override
    public void onPause() {
    	super.onPause();
        uiHelper.onPause();
        isResumed = false;
        sharePrefSave();
        Log.d(LOG_TAG, "onPause HFID:"+HFID);
    }
    private void getUserFBInfo(final Session session){
    	Log.d(LOG_TAG, "getUserFBInfo: ");

    	Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                if (session == Session.getActiveSession()) {
                    if (user != null) {
                        //Toast.makeText(getApplicationContext(), "FB UserID:"+user.getId() , Toast.LENGTH_SHORT).show();
                    	HFID=user.getId();
                    	/*try {
							URL imgUrl = new URL("http://graph.facebook.com/"
							        + user.getId() + "/picture?type=small");
							InputStream in = (InputStream) imgUrl.getContent();
                            userProfBitmap = BitmapFactory.decodeStream(in);
						} catch (MalformedURLException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}*/
                    	((GlobalApplication) getApplication()).setHFID(HFID);
                    	transHFDataInit();
                    	selectItem(HIFIVE_PAGE);
                        HiFiveFragment.checkProfExst(mainActivty);
                    	Log.d(LOG_TAG, "got FBID = "+HFID);

                    }
                }
                if (response.getError() != null) {
                    handleError(response.getError());
                    HFID=null;
                    Log.d(LOG_TAG, "fail to gets FBID = "+HFID);
                }

            }
        });
        request.executeAsync();
    }
    private void handleError(FacebookRequestError error) {
        DialogInterface.OnClickListener listener = null;
        String dialogBody = null;

        if (error == null) {
            dialogBody = getString(R.string.error_dialog_default_text);
        } else {
            switch (error.getCategory()) {
                case AUTHENTICATION_RETRY:
                    // tell the user what happened by getting the message id, and
                    // retry the operation later
                    String userAction = (error.shouldNotifyUser()) ? "" :
                            getString(error.getUserActionMessageId());
                    dialogBody = getString(R.string.error_authentication_retry, userAction);
                    listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, M_FACEBOOK_URL);
                            startActivity(intent);
                        }
                    };
                    break;

                case AUTHENTICATION_REOPEN_SESSION:
                    // close the session and reopen it.
                    dialogBody = getString(R.string.error_authentication_reopen);
                    listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Session session = Session.getActiveSession();
                            if (session != null && !session.isClosed()) {
                                session.closeAndClearTokenInformation();
                            }
                        }
                    };
                    break;


                case SERVER:
                case THROTTLING:
                    // this is usually temporary, don't clear the fields, and
                    // ask the user to try again
                    dialogBody = getString(R.string.error_server);
                    break;

                case BAD_REQUEST:
                    // this is likely a coding error, ask the user to file a bug
                    dialogBody = getString(R.string.error_bad_request, error.getErrorMessage());
                    break;

                case OTHER:
                case CLIENT:
                default:
                    // an unknown issue occurred, this could be a code error, or
                    // a server side issue, log the issue, and either ask the
                    // user to retry, or file a bug
                    dialogBody = getString(R.string.error_unknown, error.getErrorMessage());
                    break;
            }
        }

        new AlertDialog.Builder(this)
                .setPositiveButton(R.string.error_dialog_button_text, listener)
                .setTitle(R.string.error_dialog_title)
                .setMessage(dialogBody)
                .show();
    }
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent intent) {
    	Log.d(LOG_TAG, "onActivityResult HFID:"+HFID);
        uiHelper.onActivityResult(requestCode, resultCode, intent);
        if (resultCode == RESULT_OK) {
            switch(requestCode){
            case Constants.INTENT_LOGIN_ACTIVITY:
            	getUserFBInfo(Session.getActiveSession());
        		transHFDataInit();
        		((GlobalApplication) getApplication()).setHFID(HFID);
                selectItem(HIFIVE_PAGE);//When receive Hifive event go to hifive page
                HiFiveFragment.setRecvUI(this);
                //HFID = "FIXMENEXTVER";//Not used for now.

            	/*FIXMENEXTVER
                Bundle extras = intent.getExtras();
                //HFID = extras.getString(ProfileProvider.Profile.COLUMN_NAME_USERID);
                HFID = "FIXME";//Not used for now.
                //boolean newAcnt = extras.getBoolean("NewAccount");
                boolean newAcnt = "FIXME";
                Log.d(LOG_TAG, "get HFID from LOGIN_ACTIVITY:"+HFID);

                // Add a self profile to database
                ContentValues values = new ContentValues();
                Uri uri;
                
                if (newAcnt) {
                    // add empty account to client database
                    values.put(ProfileProvider.Profile.COLUMN_NAME_USERID, HFID);
                    uri = getContentResolver().insert(ProfileProvider.Profile.CONTENT_URI, values);
                    
                    // if new account, add account to the webserver database.
                    Bundle b = new Bundle();
                    // Disable sync backoff and ignore sync preferences. In other words...perform sync NOW!
                    b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                    b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                    b.putInt(Constants.SYNC_ADAPTER_OP, Constants.SYNC_METHOD_POST);
                    ContentResolver.requestSync(GenericAccountService.GetAccount(), ProfileProvider.CONTENT_AUTHORITY, b);
                    Toast.makeText(getBaseContext(), 
                            "profile: " + uri.toString() + " inserted!", Toast.LENGTH_SHORT).show();
                } else {
                    // if account exist, download account from webserver to the client database.
                    Bundle b = new Bundle();
                    // Disable sync backoff and ignore sync preferences. In other words...perform sync NOW!
                    b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                    b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                    b.putInt(Constants.SYNC_ADAPTER_OP, Constants.SYNC_METHOD_GET);
                    b.putString(Constants.REQUEST_PROFILE_ID, HFID);
                    ContentResolver.requestSync(GenericAccountService.GetAccount(), ProfileProvider.CONTENT_AUTHORITY, b);                  
                }
                */
                break;
            	case Constants.INTENT_CAMERA_ACTIVITY:
                //TODO: has to handle orientation change crash
                //mCamera.galleryAddPic();
                Session session = Session.getActiveSession();
                if (session != null && session.isOpened()) {
            		FBApiUtil.handleFbRequest(this,HFID,recvHFID,mCamera.getPath(),session) ;
            	}

                break;
            }
        }
        super.onActivityResult(requestCode, resultCode, intent);
    } 
}
