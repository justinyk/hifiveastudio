/*
 * Copyright (C) 2010 The Android Open Source Project
 *
 * Licensed under the Apache License, Version 2.0 (the "License"); you may not
 * use this file except in compliance with the License. You may obtain a copy of
 * the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS, WITHOUT
 * WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied. See the
 * License for the specific language governing permissions and limitations under
 * the License.
 */
package com.hifive;

import java.security.KeyPair;
import java.util.HashMap;
import java.util.List;

import android.util.Pair;

import com.hifive.provider.ProfileProvider;


public class Constants {

    /** server select */
    public static final String SERVER_SELECT = "SERVER_SELECT";
    public static final int USE_OPENSHIFT = 0;
    public static final int USE_CLOUDATCOST = 1;
    public static final int USE_CUSTOM = 2;
    public static final String CUSTOM_SERVER = "CUSTOM_SERVER";
    public static final String HFID = "HFID";
    public static final String ISFBSHST = "ISFBSHST";
    public static final String ISUPPICST = "ISUPPICST";
    public static final String IMGSRC = "IMGSRC";
    public static final String TELEPHONY_SERVICE = "phone";
   //public static final String[]  SHPREFS={
   //     "ISSHEMAIL",
   //     "ISSHPHONE",
   //     "ISSHFBID",
   //     "ISSHLKDNURL"
   // };
    private static final String FORMAT_JSON = "/?format=json";
    /** download event address */
    //public static final String URL_DOWNLOAD_EVENT = "http://hifive.no-ip.info/hfEvent/downloadEvent.php";
    public static final String URL_DOWNLOAD_EVENT_OPENSHIFT = "http://mysite-nfcbeam.rhcloud.com/api/v1/event/";
    public static final String URL_DOWNLOAD_EVENT_CAC = "http://162.248.10.226/hfBeam/processEvent/1/";
    
    /** upload event address */
    //public static final String URL_UPLOAD_EVENT = "http://hifive.no-ip.info/hfEvent/uploadEvent.php";
    public static final String URL_UPLOAD_EVENT_OPENSHIFT = "http://mysite-nfcbeam.rhcloud.com/api/v1/event/";
    public static final String URL_UPLOAD_EVENT_CAC = "http://162.248.10.226/hfBeam/processEvent/1/";
    
    /** download profile address */
    public static final String URL_DOWNLOAD_PROFILE_OPENSHIFT = "http://mysite-nfcbeam.rhcloud.com/api/v1/profile/";
    public static final String URL_DOWNLOAD_PROFILE_CAC = "http://162.248.10.226/hfBeam/getProfile/";
    /** upload profile address */
    public static final String URL_UPLOAD_PROFILE_OPENSHIFT = "http://mysite-nfcbeam.rhcloud.com/api/v1/profile/";
    public static final String URL_UPLOAD_PROFILE_CAC = "http://162.248.10.226/hfBeam/getProfile/";
    
    
    /* intent id for MainActitvity, include its fragment pages */
    /** login activity */
    public static final int INTENT_LOGIN_ACTIVITY = 1;
    /** camera activity */
    public static final int INTENT_CAMERA_ACTIVITY = 2;
    /** gallery activity */
    public static final int INTENT_GALLERY_ACTIVITY = 3;

    public static final int INTENT_PROFILEEDIT_ACTIVITY=1;
    
    /** 
     * Web Server event database success tag
     * return 1 or 0
     */
    public static final String TAG_SUCCESS = "success";
    /** Web Server event database table tag */
    public static final String TAG_EVENT_TABLE = "table";
    /** Web Server event database id1 tag */
    public static final String TAG_EVENT_ID1 = "id1";
    /** Web Server event database id2 tag */
    public static final String TAG_EVENT_ID2 = "id2";
    /** Web Server event database time tag */
    public static final String TAG_EVENT_TIME = "time";
    
    /** Web Server profile database profile id tag */
    public static final String TAG_PROFILE_USERID = "profileId";
    /** Web Server profile database profile first name tag */
    public static final String TAG_PROFILE_FIRSTNAME = "profileName";
    public static final String TAG_PROFILE_PICTURE = "_profilePic";
    
    
    /** Request profile of ID from database */
    public static final String REQUEST_PROFILE_ID = "request_profile_ID";
    
    //public static final String HTTP_REQUEST_METHOD = "HTTP_METHOD";
    /** JSONParser perform httpGet*/
    public static final int HTTP_GET = 1;
    /** JSONParser perform httpPost*/
    public static final int HTTP_POST = 2;
    /** JSONParser perform httpPut*/
    public static final int HTTP_PUT = 3;    
    /** JSONParser perform httpDelete*/
    public static final int HTTP_DELETE = 4;   
    
    /** SYNC ADAPER OPERATION METHOD */
    public static final String SYNC_ADAPTER_OP = "SYNC_METHOD";
    /** SYNC ADAPTER PERFORM GET */
    public static final int SYNC_METHOD_GET = 1;
    /** SYNC ADAPTER PERFORM POST */
    public static final int SYNC_METHOD_POST = 2;
    /** SYNC ADAPTER PERFORM PUT */
    public static final int SYNC_METHOD_PUT = 3;
    
	public static final int IMGFROMCAM = 0;
	public static final int IMGFROMHIFIVE = 1;
	public static final int HFDataNumElmt = 10;//10
	public static final int HFDBNumElmt = 15;//10
	
	public static final int HFPrivacyElmtIndSt = 7;//7
	public static final int HFPrivacyElmtIndEnd = 10;//7
	public static final int Frg = 15;//10
    /** Self profile CONTENT_URI, 
     *  Self profile is always pk=1
     */
    public static final String SELF_PROFILE_CONTENT_URI = ProfileProvider.Profile.CONTENT_URI+"/1";
    
    /**
     * get url for GET event
     * @return URL depends on the server select
     */
    public static String getUrlDownloadEvent() {
        switch (MainActivity.mSharedPref.getInt(Constants.SERVER_SELECT, Constants.USE_OPENSHIFT)) {
        case Constants.USE_OPENSHIFT:
            return URL_DOWNLOAD_EVENT_OPENSHIFT+FORMAT_JSON;
        case Constants.USE_CLOUDATCOST:
            return URL_DOWNLOAD_EVENT_CAC;
        case Constants.USE_CUSTOM:
            return MainActivity.mSharedPref.getString(Constants.CUSTOM_SERVER, "");            
        default:
            return null; 
        }
    }
    
    /**
     * get url for POST event
     * @return URL depends on the server select
     */
    public static String getUrlUploadEvent() {
        switch (MainActivity.mSharedPref.getInt(Constants.SERVER_SELECT, Constants.USE_OPENSHIFT)) {
        case Constants.USE_OPENSHIFT:
            return URL_UPLOAD_EVENT_OPENSHIFT+FORMAT_JSON;
        case Constants.USE_CLOUDATCOST:
            return URL_UPLOAD_EVENT_CAC;
        case Constants.USE_CUSTOM:
            return MainActivity.mSharedPref.getString(Constants.CUSTOM_SERVER, "");
        default:
            return null; 
        }
    }
    
    /**
     * get url for GET profile
     * @param is for specify id 
     * @return URL depends on the server select
     * Ex: http://mysite-nfcbeam.rhcloud.com/api/v1/profile/358239055570222/?format=json
     * or http://162.248.10.226/hfBeam/getProfile/358239055570222/
     */
    public static String getUrlDownloadProfile(String resource_url) {
        switch (MainActivity.mSharedPref.getInt(Constants.SERVER_SELECT, Constants.USE_OPENSHIFT)) {
        case Constants.USE_OPENSHIFT:
            return URL_DOWNLOAD_PROFILE_OPENSHIFT+resource_url+FORMAT_JSON;
        case Constants.USE_CLOUDATCOST:
            return URL_DOWNLOAD_PROFILE_CAC+resource_url+"/";
        case Constants.USE_CUSTOM:
            return MainActivity.mSharedPref.getString(Constants.CUSTOM_SERVER, "");            
        default:
            return null; 
        }
    }
    
    /**
     * get url for POST profile
     * @param is for specify id 
     * @return URL depends on the server select
     * Ex: http://mysite-nfcbeam.rhcloud.com/api/v1/profile/358239055570222/?format=json
     * or http://162.248.10.226/hfBeam/getProfile/358239055570222/
     */    
    public static String getUrlUploadProfile(String resource_url) {
        switch (MainActivity.mSharedPref.getInt(Constants.SERVER_SELECT, Constants.USE_OPENSHIFT)) {
        case Constants.USE_OPENSHIFT:
            return URL_UPLOAD_PROFILE_OPENSHIFT+resource_url+FORMAT_JSON;
        case Constants.USE_CLOUDATCOST:
            return URL_UPLOAD_PROFILE_CAC+resource_url+"/";
        case Constants.USE_CUSTOM:
            return MainActivity.mSharedPref.getString(Constants.CUSTOM_SERVER, "");
        default:
            return null; 
        }
    }
}
