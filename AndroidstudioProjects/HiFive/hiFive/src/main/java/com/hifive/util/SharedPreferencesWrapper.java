package com.hifive.util;

import com.hifive.Constants;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public class SharedPreferencesWrapper {

    private static SharedPreferences sharedPref;
    
    public SharedPreferencesWrapper (Activity mAct) {
        sharedPref = mAct.getPreferences(Context.MODE_PRIVATE);
        
    }
    
    public void putInt(String key, Integer defaultValue) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putInt(key, defaultValue);
        editor.commit();
    }
    public Integer getInt(String key, Integer defaultValue) {
        return sharedPref.getInt(key, defaultValue);
    }
    public void putBoolean(String key, boolean defaultValue) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putBoolean(key, defaultValue);
        editor.commit();
    }
    public boolean getBoolean(String key, boolean defaultValue) {
        return sharedPref.getBoolean(key, defaultValue);
    }
        
    public void putString(String key, String defaultValue) {
        SharedPreferences.Editor editor = sharedPref.edit();
        editor.putString(key, defaultValue);
        editor.commit();
    }
    public String getString(String key, String defaultValue) {
        return sharedPref.getString(key, defaultValue);
    }
    public void putBooleanArray(String[] keys, boolean[] values) {
        SharedPreferences.Editor editor = sharedPref.edit();
        for(int i=0;i<keys.length;i++) {
        	editor.putBoolean(keys[i], values[i]);
            editor.commit();
        }
    }
    public boolean[] getBooleanArray(String[] keys) {
        boolean[] values = new boolean[keys.length];
    	for(int i=0;i<keys.length;i++) {
    		values[i]=sharedPref.getBoolean(keys[i], false);
        }
        return values;
    }
        
    public boolean contains(String key) {
        return sharedPref.contains(key);
    }
    
}
