package com.hifive.provider;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import com.hifive.util.SelectionBuilder;

public class EventProvider extends ContentProvider {
    EventDatabase mDatabaseHelper;

    private static final String LOG_TAG = "hifive.EventProvider";

    /** Content provider authority. */
    public static final String CONTENT_AUTHORITY = "com.hifive.provider.EventProvider";
    /** Base URI. (content://com.hifive) */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    /** Path component for "event"-type resources.. */
    private static final String PATH_EVENTS = "events";

    /** Columns supported by "entries" records. */
    public static class Event implements BaseColumns {        
    	// TODO: not sure what is MIME
    	/** MIME type for lists of entries. */
        //public static final String CONTENT_TYPE =
        //        ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.basicsyncadapter.entries";
        /** MIME type for individual entries. */
        //public static final String CONTENT_ITEM_TYPE =
        //        ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.basicsyncadapter.entry";

    	/** Fully qualified URI for "event" resources. */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_EVENTS).build();
        /** Table name where records are stored for "entry" resources. */
        public static final String TABLE_NAME = "event";
        /** event participate 1 */
        public static final String COLUMN_NAME_ID1 = "id1";
        /** event participate 2 */
        public static final String COLUMN_NAME_ID2 = "id2";	
        /** event happen time */
        public static final String COLUMN_NAME_TIME = "time";
        /** event column projection */
   	 	public static final String[] PROJECTION = new String[] {  
		_ID,
		COLUMN_NAME_ID1,
		COLUMN_NAME_ID2, 
		COLUMN_NAME_TIME
	    };
    }
    // The constants below represent individual URI routes, as IDs. Every URI pattern recognized by
    // this ContentProvider is defined using sUriMatcher.addURI(), and associated with one of these
    // IDs.
    //
    // When a incoming URI is run through sUriMatcher, it will be tested against the defined
    // URI patterns, and the corresponding route ID will be returned.
    /** URI ID for route: /event */
    public static final int ROUTE_ENTRIES = 1;

    /** URI ID for route: /event/{ID} */
    public static final int ROUTE_ENTRIES_ID = 2;
    /**
    * UriMatcher, used to decode incoming URIs.
    */
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(CONTENT_AUTHORITY, "events", ROUTE_ENTRIES);
        sUriMatcher.addURI(CONTENT_AUTHORITY, "events/*", ROUTE_ENTRIES_ID);
    }        
        
    @Override
    public boolean onCreate() {
        mDatabaseHelper = new EventDatabase(getContext());
        return true;
    }
    
    /**
     * Determine the mime type for entries returned by a given URI.
     */
    @Override
    public String getType(Uri uri) {
    	/* TODO: not sure what is mime
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_ENTRIES:
                return Event.CONTENT_TYPE;
            case ROUTE_ENTRIES_ID:
                return Event.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        */
        return null;
    }
    /**
     * Perform a database query by URI.
     *
     * <p>Currently supports returning all entries (/entries) and individual entries by ID
     * (/entries/{ID}).
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
        SelectionBuilder builder = new SelectionBuilder();
        int uriMatch = sUriMatcher.match(uri);
        switch (uriMatch) {
            case ROUTE_ENTRIES_ID:
                // Return a single entry, by ID.
                String id = uri.getLastPathSegment();
                builder.where(Event._ID + "=?", id);
            case ROUTE_ENTRIES:
                // Return all known entries.
                builder.table(Event.TABLE_NAME)
                       .where(selection, selectionArgs);
                Cursor c = builder.query(db, projection, sortOrder);
                // Note: Notification URI must be manually set here for loaders to correctly
                // register ContentObservers.
                Context ctx = getContext();
                assert ctx != null;
                c.setNotificationUri(ctx.getContentResolver(), uri);
                return c;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }
    
    /**
     * Insert a new entry into the database.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        assert db != null;
        final int match = sUriMatcher.match(uri);
        Uri result;
        switch (match) {
            case ROUTE_ENTRIES:
                long id = db.insertOrThrow(Event.TABLE_NAME, null, values);
                result = Uri.parse(Event.CONTENT_URI + "/" + id);
                break;
            case ROUTE_ENTRIES_ID:
                throw new UnsupportedOperationException("Insert not supported on URI: " + uri);
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return result;
	}
	
    /**
     * Delete an entry by database by URI.
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;
        switch (match) {
            case ROUTE_ENTRIES:
                count = builder.table(Event.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(db);
                break;
            case ROUTE_ENTRIES_ID:
                String id = uri.getLastPathSegment();
                count = builder.table(Event.TABLE_NAME)
                       .where(Event._ID + "=?", id)
                       .where(selection, selectionArgs)
                       .delete(db);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;
    }

    /**
     * Update an etry in the database by URI.
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;
        switch (match) {
            case ROUTE_ENTRIES:
                count = builder.table(Event.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_ENTRIES_ID:
                String id = uri.getLastPathSegment();
                count = builder.table(Event.TABLE_NAME)
                        .where(Event._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;
    }


    /**
     * SQLite backend for @{link EventProvider}.
     *
     * Provides access to an disk-backed, SQLite datastore which is utilized by EventProvider. This
     * database should never be accessed by other parts of the application directly.
     */
    static class EventDatabase extends SQLiteOpenHelper {
        /** Schema version. */
        public static final int DATABASE_VERSION = 1;
        /** Filename for SQLite file. */
        public static final String DATABASE_NAME = "event.db";

        private static final String TYPE_TEXT = " TEXT";
        //private static final String TYPE_INTEGER = " INTEGER";
        private static final String COMMA_SEP = ",";
        /** SQL statement to create "event" table. */
        private static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + Event.TABLE_NAME + " (" +
                        Event._ID + " INTEGER PRIMARY KEY," +
                		Event.COLUMN_NAME_ID1 + TYPE_TEXT + COMMA_SEP +
                		Event.COLUMN_NAME_ID2 + TYPE_TEXT + COMMA_SEP +
                		Event.COLUMN_NAME_TIME + TYPE_TEXT + ")";

        /** SQL statement to drop "event" table. */
        private static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + Event.TABLE_NAME;

        public EventDatabase(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
            Log.d(LOG_TAG, "SQL create");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }
    }
}
