package com.hifive.util;

import android.app.Activity;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.net.Uri;
import android.os.Bundle;
import android.text.format.Time;
import android.widget.Toast;

import com.hifive.Constants;
import com.hifive.MainActivity;
import com.hifive.accounts.GenericAccountService;
import com.hifive.provider.EventProvider;

/** 
 * non activity class. Api function to the database or access server
 * @author PCH
 *
 */
public class EventUtil {
    
    /** 
     * Add local event to local database
     */
    public static String add(Activity mActivity, String id2) {
        Time mTime = new Time();
        mTime.setToNow();
        // Add a event to database
        ContentValues values = new ContentValues();
        values.put(EventProvider.Event.COLUMN_NAME_ID1, MainActivity.HFID);
        values.put(EventProvider.Event.COLUMN_NAME_ID2, id2);
        values.put(EventProvider.Event.COLUMN_NAME_TIME, mTime.format("%y:%m:%d:%H:%M:%S"));
    
        Uri uri = mActivity.getContentResolver().insert(EventProvider.Event.CONTENT_URI, values);
        Toast.makeText(mActivity, 
                "event: " + uri.toString() + " inserted!", Toast.LENGTH_SHORT).show();
        return uri.toString();
    }
    
    /** 
     * upload one event id to webserver now
     */
    public static void upload2WS (Long id){
        upload2WS(EventProvider.Event.CONTENT_URI + "/" + String.valueOf(id));
    }
    
    public static void upload2WS (String mUriString){
        Bundle b = new Bundle();
        // Disable sync backoff and ignore sync preferences. In other words...perform sync NOW!
        b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
        b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
        b.putInt(Constants.SYNC_ADAPTER_OP, Constants.SYNC_METHOD_POST);
        b.putString("PostEventURI", mUriString);

        ContentResolver.requestSync(GenericAccountService.GetAccount(), EventProvider.CONTENT_AUTHORITY, b);
    }

}
