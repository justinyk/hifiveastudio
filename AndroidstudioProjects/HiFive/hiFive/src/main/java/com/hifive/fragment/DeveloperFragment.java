package com.hifive.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.RadioGroup.OnCheckedChangeListener;
import android.widget.TextView;

import com.hifive.Constants;
import com.hifive.MainActivity;
import com.hifive.R;

public class DeveloperFragment extends Fragment {
    private static final String LOG_TAG = "hifive.developerFragment";

    View rootView;
    private TextView mUseURL;
    private EditText etCustomUrl;
    
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		rootView = inflater.inflate(R.layout.fragment_developer, container, false);
		
		mUseURL = (TextView)rootView.findViewById(R.id.use_url);

		
		RadioGroup mRadioGroup = (RadioGroup)rootView.findViewById(R.id.radio_serverswitch);
		mRadioGroup.setOnCheckedChangeListener(serverSwitchOnClickListener);
		
		// select openshift by default
		RadioButton mOpenShift = (RadioButton)rootView.findViewById(R.id.radio_openshift);
		RadioButton mCAC = (RadioButton)rootView.findViewById(R.id.radio_cac);
		RadioButton mCustom = (RadioButton)rootView.findViewById(R.id.radio_custom);
		etCustomUrl = (EditText)rootView.findViewById(R.id.custom_url);
		
		etCustomUrl.setText(MainActivity.mSharedPref.getString(Constants.CUSTOM_SERVER, ""));
		int mServerSelect = MainActivity.mSharedPref.getInt(Constants.SERVER_SELECT, Constants.USE_OPENSHIFT);
		switch (mServerSelect) {
		case Constants.USE_OPENSHIFT:
		    mOpenShift.setChecked(true);
		    break;
		case Constants.USE_CLOUDATCOST:
		    mCAC.setChecked(true);
		    break;
	    case Constants.USE_CUSTOM:
	         mCustom.setChecked(true);
	         break;
		}		
		
		return rootView;
	}
	
	RadioGroup.OnCheckedChangeListener serverSwitchOnClickListener = 
	    new OnCheckedChangeListener() {
	        public void onCheckedChanged(RadioGroup group, int checkedId) {
	            // checkedId is the RadioButton selected
	            switch(checkedId) {
	            case R.id.radio_openshift:
	                MainActivity.mSharedPref.putInt(Constants.SERVER_SELECT, Constants.USE_OPENSHIFT);
	                mUseURL.setText("use:  "+Constants.getUrlDownloadEvent());
	                break;
	            case R.id.radio_cac:
	                MainActivity.mSharedPref.putInt(Constants.SERVER_SELECT, Constants.USE_CLOUDATCOST);
	                mUseURL.setText("use:  "+Constants.getUrlDownloadEvent());
	                break;
                case R.id.radio_custom:                    
                    MainActivity.mSharedPref.putInt(Constants.SERVER_SELECT, Constants.USE_CUSTOM);
                    MainActivity.mSharedPref.putString(Constants.CUSTOM_SERVER, etCustomUrl.getText().toString());
                    Log.d(LOG_TAG, "URL save:"+MainActivity.mSharedPref.getString(Constants.CUSTOM_SERVER, ""));
                    mUseURL.setText("use:  "+Constants.getUrlDownloadEvent());
                    break;
            }
	    } 
	};
	

}
