package com.hifive.adapter;

import android.content.Context;
import android.database.Cursor;
import android.support.v4.widget.CursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.facebook.widget.ProfilePictureView;
import com.hifive.R;
import com.hifive.provider.ProfileProvider;

public class ProfileAdapter extends CursorAdapter {
    private static final String LOG_TAG = "hifive.ProfileAdapter";
	private LayoutInflater mInflater;
	private int mSelectedPosition;

 
	public ProfileAdapter(Context context, Cursor c, int flags) {
		super(context, c, flags);
		mInflater = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
	}
	 
	@Override
    public void bindView(View view, Context context, Cursor cursor) {
        if (cursor!=null) {

	        
        	ProfilePictureView profilePictureView = (ProfilePictureView) view.findViewById(R.id.profilelist_profile_pic);
        	profilePictureView.setCropped(true);
            profilePictureView.setVisibility(View.VISIBLE);
            
        	TextView tvRcvrName = (TextView) view.findViewById(R.id.profilelist_name);
        	TextView tvRcvrOrg = (TextView) view.findViewById(R.id.profilelist_org);
        	TextView tvHfTime = (TextView) view.findViewById(R.id.profilelist_hftime);
            
            String rcvrName = 
            		cursor.getString(cursor.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME))+
            		" "+
            		cursor.getString(cursor.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_LASTNAME))
            		;
            String rcvrOrg = cursor.getString(cursor.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION));
            String hfTime = cursor.getString(cursor.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_HFTIME));
            String userID = cursor.getString(cursor.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_USERID));
            
            if(userID!=null){
        		profilePictureView.setProfileId(userID);
        	}
			if (rcvrName != null){
				tvRcvrName.setText(rcvrName);
            }
            if (rcvrOrg != null){
            	tvRcvrOrg.setText(rcvrOrg);
            }
            if (hfTime != null){
            	tvHfTime.setText(hfTime);
            }
		}
	}
	 
	@Override
	public View newView(Context context, Cursor cursor, ViewGroup parent) {
		return mInflater.inflate(R.layout.adapter_profilelist, parent, false);
	}
	 
	public void setSelectedPosition(int position) {
		mSelectedPosition = position;
		Log.v(LOG_TAG, "position: "+String.valueOf(mSelectedPosition));
	    // something has changed.
		notifyDataSetChanged();
	}
	 
}
