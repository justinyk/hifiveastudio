package com.hifive.activity;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.List;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.util.Pair;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;


import com.facebook.Session;
import com.facebook.widget.ProfilePictureView;
import com.hifive.Constants;
import com.hifive.MainActivity;
import com.hifive.R;
import com.hifive.provider.ProfileProvider;
import com.hifive.provider.ProfileProvider.Profile;
import com.hifive.util.CameraUtil;
import com.hifive.util.FBApiUtil;
import com.hifive.util.GPSTracker;
import com.hifive.util.GlobalApplication;
import com.hifive.util.PictureUtil;

public class HiFiveInfoActivity extends Activity {
    private static final String LOG_TAG = "hifive.HiFiveInfo";

    public static ProfilePictureView profilePictureView;
    private Button buttonBack;
    private ImageButton buttonMap;
    private static EditText etComment;
    private static EditText etLocStr;
    private static TextView tvRcvrName;
    private static TextView tvRcvrPos;
    private static TextView tvRcvrOrg;
    private static TextView tvHfTime;
    private static final Location HFLoc = new Location("") {{
        setLatitude(37.7750);
        setLongitude(-122.4183);
    }};
    
    private Bitmap MapBitmap;
    
  
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_hifiveinfo);
		hifiveInfoViewInit();
	}

    private void hifiveInfoViewInit() {
    	Log.d(LOG_TAG, "profileViewInit");
    	String key;
    	String value;
    	
        profilePictureView = (ProfilePictureView) findViewById(R.id.hifiveinfo_rcvr_profile_pic);
        profilePictureView.setCropped(true);
        profilePictureView.setVisibility(View.VISIBLE);
        
        buttonBack       =  (Button)findViewById(R.id.hifiveinfo_back);
        buttonMap       =  (ImageButton)findViewById(R.id.hifiveinfo_map);
        tvRcvrName       =  (TextView)findViewById(R.id.hifivinfo_rcvr_name);
        tvRcvrPos       =  (TextView)findViewById(R.id.hifiveinfo_rcvr_pos);
        tvRcvrOrg       =  (TextView)findViewById(R.id.hifiveinfo_rcvr_org);
        tvHfTime       =  (TextView)findViewById(R.id.hifiveinfo_hftime);
        etLocStr       =  (EditText)findViewById(R.id.hifiveinfo_place_str);
        etComment       =  (EditText)findViewById(R.id.hifiveinfo_comment_edit);
        
        buttonBack.setOnClickListener(backOnClickListener);
        buttonMap.setOnClickListener(mapOnClickListener);
        
        key=ProfileProvider.Profile.COLUMN_NAME_HFLOCSTR;
    	value=((GlobalApplication) getApplication()).getRecvHFData(key);
    	if(value!=null){
    		etLocStr.setText(value);
    	}  
        key=ProfileProvider.Profile.COLUMN_NAME_HFCOMMENT;
    	value=((GlobalApplication) getApplication()).getRecvHFData(key);
    	if(value!=null){
    		etComment.setText(value);
    	}  
                
    	key=ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME;
    	value=((GlobalApplication) getApplication()).getRecvHFData(key);
    	key=ProfileProvider.Profile.COLUMN_NAME_LASTNAME;
    	value=value+" "+((GlobalApplication) getApplication()).getRecvHFData(key);
    	if(value!=null){
    		tvRcvrName.setText(value);
    	}
    	key=ProfileProvider.Profile.COLUMN_NAME_TITLE;
    	value=((GlobalApplication) getApplication()).getRecvHFData(key);
    	if(value!=null){
    		tvRcvrPos.setText(value);
    	}
    	
    	key=ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION;
    	value=((GlobalApplication) getApplication()).getRecvHFData(key);
    	if(value!=null){
    		Log.d(LOG_TAG,"tvRcvrOrg 00:"+value );
    		tvRcvrOrg.setText(value);
    		Log.d(LOG_TAG,"tvRcvrOrg 01:"+value );
    	}
    	key=ProfileProvider.Profile.COLUMN_NAME_HFTIME;
    	value=((GlobalApplication) getApplication()).getRecvHFData(key);
    	if(value!=null){
    		tvHfTime.setText(value);
    	}    	
    	Log.d(LOG_TAG,"tvRcvrOrg 0" );
    	key=ProfileProvider.Profile.COLUMN_NAME_FB_ID;
    	value=((GlobalApplication) getApplication()).getRecvHFData(key);
    	Log.d(LOG_TAG,"tvRcvrOrg 1" );
    	
    	if(value!=null){
    		profilePictureView.setProfileId(value);
    	}else{
    		Log.d("HiFiveInfoActivity","tvRcvrOrg 2" );
        	
    	}
    	new MapDownTask().execute();
    	
    	
        
        
    }
    Button.OnClickListener mapOnClickListener = 
    new OnClickListener() {
        public void onClick(View v) {
        	Toast.makeText(getBaseContext(), "click profile map button", Toast.LENGTH_SHORT).show();
        	openMap();
    }       
}; 
private void openMap(){
	 
	String labelLocation = "HighFive Location";
	String longtitude=((GlobalApplication) getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFLONGITUDE);
	String latitude=((GlobalApplication) getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFLATITUDE);
     final String uriString = "http://maps.google.com/maps?q=" + latitude + ',' + longtitude + "("+ labelLocation +")&z=12";
     showMyLocation(uriString);
}
private void showMyLocation(String uriString) {
	Uri intentUri = Uri.parse(uriString);
    Intent intent = new Intent();
    intent.setAction(Intent.ACTION_VIEW);
    intent.setData(intentUri);
    startActivity(intent);

}
// l
    // listener for click "back"
    Button.OnClickListener backOnClickListener = 
        new OnClickListener() {
            public void onClick(View v) {
            	Log.d(LOG_TAG,"backOnClickListener 0"+etComment.getText().toString() );
                ((GlobalApplication) getApplication()).setRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFCOMMENT,etComment.getText().toString());
                Log.d(LOG_TAG,"backOnClickListener 1" );
                ((GlobalApplication) getApplication()).setRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFLOCSTR,etLocStr.getText().toString());
                Log.d(LOG_TAG,"backOnClickListener 2" );
                finish();

        }       
    };


    public void setMapFig() {
    	String longtitude=((GlobalApplication) getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFLONGITUDE);
    	String latitude=((GlobalApplication) getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFLATITUDE);
    	String mapUrl=getMapUrl(longtitude,latitude);
    	Log.d(LOG_TAG,"mapUrl:"+mapUrl );
    	MapBitmap = getBitmapFromUrl(mapUrl);
    	Log.d(LOG_TAG,"mapUrl0:");
    	//buttonMap.setImageBitmap(MapBitmap);
    	//Log.d(LOG_TAG,"mapUrl1:");
    	
    }
    public void setDefaultHFPlace() {
    	String longtitude=((GlobalApplication) getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFLONGITUDE);
    	String latitude=((GlobalApplication) getApplication()).getRecvHFData(ProfileProvider.Profile.COLUMN_NAME_HFLATITUDE);
    	HFLoc.setLongitude(Double.parseDouble(longtitude));
    	HFLoc.setLatitude(Double.parseDouble(latitude));
    	GPSTracker gpsTracker = new GPSTracker(this);
    	((GlobalApplication) getApplication()).setLocation(HFLoc);
    	String country 			= gpsTracker.getCountryName(this);
	    String city 			= gpsTracker.getLocality(this);
	    String addressLine 		= gpsTracker.getAddressLine(this);
	    String locString= country+","+city+","+addressLine;
	    ((GlobalApplication) getApplication()).setLocString(locString);
        
    	
    }
    private String getMapUrl(String longtitude,String latitude) 
    {
    	String locStr=String.valueOf(latitude)+","+String.valueOf(longtitude);
    	String urlStr="http://maps.googleapis.com/maps/api/staticmap?center="+locStr+"&zoom=13&size=600x300&maptype=roadmap&markers=color:orange|"+locStr;
        return urlStr;
    }
    private Bitmap getBitmapFromUrl(String uStr) 
    {
        try {
            URL url = new URL(uStr);
            Log.d(LOG_TAG,"getBitmapFromUrl 0:");
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            Log.d(LOG_TAG,"getBitmapFromUrl 1:");
            connection.setDoInput(true);
            Log.d(LOG_TAG,"getBitmapFromUrl 2:");
            connection.connect();
            Log.d(LOG_TAG,"getBitmapFromUrl 3:");
            InputStream input = connection.getInputStream();
            Log.d(LOG_TAG,"getBitmapFromUrl 4:");
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            Log.d(LOG_TAG,"getBitmapFromUrl 5:");
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }

    public ContentValues setContentValues() {
	    ContentValues values = new ContentValues();
	    values.put(ProfileProvider.Profile.COLUMN_NAME_HFCOMMENT, etComment.getText().toString());
	    return values;
    }

    class MapDownTask extends AsyncTask<Void,Void,Void>
    {


        protected void onPreExecute() {
          //display progress dialog.

     }
        protected Void doInBackground(Void... params) {
        	setMapFig();
         return null;
     }



     protected void onPostExecute(Void result) {
       // dismiss progress dialog and update ui
     	Log.d(LOG_TAG,"mapUrl2:");
     	buttonMap.setImageBitmap(MapBitmap);
     	Log.d(LOG_TAG,"mapUrl3:");
     }
    }

}
