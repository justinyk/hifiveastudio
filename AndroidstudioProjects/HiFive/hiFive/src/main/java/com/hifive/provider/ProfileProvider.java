package com.hifive.provider;

import java.util.Arrays;

import android.content.ContentProvider;
import android.content.ContentValues;
import android.content.Context;
import android.content.UriMatcher;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.provider.BaseColumns;
import android.util.Log;

import com.hifive.Constants;
import com.hifive.util.SelectionBuilder;

public class ProfileProvider extends ContentProvider {
    ProfileDatabase mDatabaseHelper;

    private static final String LOG_TAG = "hifive.ProfileProvider";

    /** Content provider authority. */
    public static final String CONTENT_AUTHORITY = "com.hifive.provider.ProfileProvider";
    /** Base URI. (content://com.hifive) */
    public static final Uri BASE_CONTENT_URI = Uri.parse("content://" + CONTENT_AUTHORITY);
    /** Path component for "profile"-type resources.. */
    private static final String PATH_PROFILES = "profiles";

    /** Columns supported by "entries" records. */
    public static class Profile implements BaseColumns {        
    	// TODO: not sure what is MIME
    	/** MIME type for lists of entries. */
        //public static final String CONTENT_TYPE =
        //        ContentResolver.CURSOR_DIR_BASE_TYPE + "/vnd.basicsyncadapter.entries";
        /** MIME type for individual entries. */
        //public static final String CONTENT_ITEM_TYPE =
        //        ContentResolver.CURSOR_ITEM_BASE_TYPE + "/vnd.basicsyncadapter.entry";

    	/** Fully qualified URI for "profile" resources. */
        public static final Uri CONTENT_URI =
                BASE_CONTENT_URI.buildUpon().appendPath(PATH_PROFILES).build();
        
        /** Table name where records are stored for "entry" resources. */
        
        public static final String TABLE_NAME = "profile";
        /** user ID */
        public static final String COLUMN_NAME_USERID = "userID";
        /** first name*/
        public static final String COLUMN_NAME_FIRSTNAME = "firstName";
        /** last name*/
        public static final String COLUMN_NAME_LASTNAME = "lastName";
        /** phone number */
        public static final String COLUMN_NAME_PHONE = "phone";
        /** email address */
        public static final String COLUMN_NAME_EMAIL = "email";
        /** profile picture */
        public static final String COLUMN_NAME_PROFILE_PIC = "profilePic";
        /** FB ID */
        public static final String COLUMN_NAME_FB_ID = "fbId";
        public static final String COLUMN_NAME_ORGANIZATION = "organization";
        public static final String COLUMN_NAME_ORGANIZATION_LINK = "organizationLink";
        public static final String COLUMN_NAME_TITLE = "title";
        public static final String COLUMN_NAME_LNKDIN_LINK = "lnkdInLink";
        public static final String COLUMN_NAME_HFLONGITUDE = "hflongitude";
        public static final String COLUMN_NAME_HFLATITUDE = "hflatitude";
        public static final String COLUMN_NAME_HFTIME = "hftime";
        public static final String COLUMN_NAME_HFCOMMENT = "hfcomment";
        public static final String COLUMN_NAME_HFLOCSTR = "hflocstr";
        
   	 	public static final String[] PROJECTION = new String[] {  
		_ID,//0
		COLUMN_NAME_USERID,//1
		COLUMN_NAME_FIRSTNAME,//2
		COLUMN_NAME_LASTNAME,//3
		COLUMN_NAME_ORGANIZATION,//4
		COLUMN_NAME_ORGANIZATION_LINK,//5
		COLUMN_NAME_TITLE,//6
		COLUMN_NAME_PHONE,//Privacy element 7
		COLUMN_NAME_EMAIL,//8
		COLUMN_NAME_LNKDIN_LINK,//9
		COLUMN_NAME_FB_ID,//10
		COLUMN_NAME_HFLONGITUDE,//11
		COLUMN_NAME_HFLATITUDE,//12
		COLUMN_NAME_HFTIME,//13
		COLUMN_NAME_HFCOMMENT,//14
		COLUMN_NAME_HFLOCSTR,//15
		COLUMN_NAME_PROFILE_PIC//16
		};
   	 	
   	    public static final String[] PRIVACYDATA = Arrays.copyOfRange(PROJECTION, Constants.HFPrivacyElmtIndSt, Constants.HFPrivacyElmtIndEnd);
    }
    // The constants below represent individual URI routes, as IDs. Every URI pattern recognized by
    // this ContentProvider is defined using sUriMatcher.addURI(), and associated with one of these
    // IDs.
    //
    // When a incoming URI is run through sUriMatcher, it will be tested against the defined
    // URI patterns, and the corresponding route ID will be returned.
    /** URI ID for route: /profile */
    public static final int ROUTE_ENTRIES = 1;

    /** URI ID for route: /profile/{ID} */
    public static final int ROUTE_ENTRIES_ID = 2;
    /**
    * UriMatcher, used to decode incoming URIs.
    */
    private static final UriMatcher sUriMatcher = new UriMatcher(UriMatcher.NO_MATCH);
    static {
        sUriMatcher.addURI(CONTENT_AUTHORITY, "profiles", ROUTE_ENTRIES);
        sUriMatcher.addURI(CONTENT_AUTHORITY, "profiles/*", ROUTE_ENTRIES_ID);
    }        
        
    @Override
    public boolean onCreate() {
        mDatabaseHelper = new ProfileDatabase(getContext());
        return true;
    }
    
    /**
     * Determine the mime type for entries returned by a given URI.
     */
    @Override
    public String getType(Uri uri) {
    	/* TODO: not sure what is mime
        final int match = sUriMatcher.match(uri);
        switch (match) {
            case ROUTE_ENTRIES:
                return Profile.CONTENT_TYPE;
            case ROUTE_ENTRIES_ID:
                return Profile.CONTENT_ITEM_TYPE;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        */
        return null;
    }
    /**
     * Perform a database query by URI.
     *
     * <p>Currently supports returning all entries (/entries) and individual entries by ID
     * (/entries/{ID}).
     */
    @Override
    public Cursor query(Uri uri, String[] projection, String selection, String[] selectionArgs,
                        String sortOrder) {
        SQLiteDatabase db = mDatabaseHelper.getReadableDatabase();
        SelectionBuilder builder = new SelectionBuilder();
        int uriMatch = sUriMatcher.match(uri);
        switch (uriMatch) {
            case ROUTE_ENTRIES_ID:
                // Return a single entry, by ID.
                String id = uri.getLastPathSegment();
                builder.where(Profile._ID + "=?", id);
            case ROUTE_ENTRIES:
                // Return all known entries.
                builder.table(Profile.TABLE_NAME)
                       .where(selection, selectionArgs);
                Cursor c = builder.query(db, projection, sortOrder);
                // Note: Notification URI must be manually set here for loaders to correctly
                // register ContentObservers.
                Context ctx = getContext();
                assert ctx != null;
                c.setNotificationUri(ctx.getContentResolver(), uri);
                return c;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
    }
    
    /**
     * Insert a new entry into the database.
     */
    @Override
    public Uri insert(Uri uri, ContentValues values) {
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        assert db != null;
        final int match = sUriMatcher.match(uri);
        Uri result;
        switch (match) {
            case ROUTE_ENTRIES:
                long id = db.insertOrThrow(Profile.TABLE_NAME, null, values);
                result = Uri.parse(Profile.CONTENT_URI + "/" + id);
                break;
            case ROUTE_ENTRIES_ID:
                throw new UnsupportedOperationException("Insert not supported on URI: " + uri);
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return result;
	}
	
    /**
     * Delete an entry by database by URI.
     */
    @Override
    public int delete(Uri uri, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;
        switch (match) {
            case ROUTE_ENTRIES:
                count = builder.table(Profile.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .delete(db);
                break;
            case ROUTE_ENTRIES_ID:
                String id = uri.getLastPathSegment();
                count = builder.table(Profile.TABLE_NAME)
                       .where(Profile._ID + "=?", id)
                       .where(selection, selectionArgs)
                       .delete(db);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        // Send broadcast to registered ContentObservers, to refresh UI.
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;
    }

    /**
     * Update an entry in the database by URI.
     */
    @Override
    public int update(Uri uri, ContentValues values, String selection, String[] selectionArgs) {
        SelectionBuilder builder = new SelectionBuilder();
        final SQLiteDatabase db = mDatabaseHelper.getWritableDatabase();
        final int match = sUriMatcher.match(uri);
        int count;
        switch (match) {
            case ROUTE_ENTRIES:
                count = builder.table(Profile.TABLE_NAME)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            case ROUTE_ENTRIES_ID:
                String id = uri.getLastPathSegment();
                count = builder.table(Profile.TABLE_NAME)
                        .where(Profile._ID + "=?", id)
                        .where(selection, selectionArgs)
                        .update(db, values);
                break;
            default:
                throw new UnsupportedOperationException("Unknown uri: " + uri);
        }
        Context ctx = getContext();
        assert ctx != null;
        ctx.getContentResolver().notifyChange(uri, null, false);
        return count;
    }


    /**
     * SQLite backend for @{link ProfileProvider}.
     *
     * Provides access to an disk-backed, SQLite datastore which is utilized by ProfileProvider. This
     * database should never be accessed by other parts of the application directly.
     */
    static class ProfileDatabase extends SQLiteOpenHelper {
        /** Schema version. */
        public static final int DATABASE_VERSION = 1;
        /** Filename for SQLite file. */
        public static final String DATABASE_NAME = "profile.db";

        private static final String TYPE_TEXT = " TEXT";
        //private static final String TYPE_INTEGER = " INTEGER";
        private static final String TYPE_BLOB = " BLOB";
        private static final String COMMA_SEP = ",";
        /** SQL statement to create "profile" table. */
        private static final String SQL_CREATE_ENTRIES =
                "CREATE TABLE " + Profile.TABLE_NAME + " (" +
                        Profile._ID + " INTEGER PRIMARY KEY," +
                        Profile.COLUMN_NAME_USERID + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_FIRSTNAME + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_LASTNAME + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_PHONE + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_EMAIL + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_ORGANIZATION + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_ORGANIZATION_LINK + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_TITLE + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_LNKDIN_LINK + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_HFLONGITUDE + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_HFLATITUDE + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_HFTIME + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_HFCOMMENT + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_HFLOCSTR + TYPE_TEXT + COMMA_SEP +
                        Profile.COLUMN_NAME_PROFILE_PIC + TYPE_BLOB + COMMA_SEP +
                        Profile.COLUMN_NAME_FB_ID + TYPE_TEXT + ")";

        /** SQL statement to drop "profile" table. */
        private static final String SQL_DELETE_ENTRIES =
                "DROP TABLE IF EXISTS " + Profile.TABLE_NAME;

        public ProfileDatabase(Context context) {
            super(context, DATABASE_NAME, null, DATABASE_VERSION);
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(SQL_CREATE_ENTRIES);
            Log.d(LOG_TAG, "SQL create");
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            // This database is only a cache for online data, so its upgrade policy is
            // to simply to discard the data and start over
            db.execSQL(SQL_DELETE_ENTRIES);
            onCreate(db);
        }
    }
}
