package com.hifive.fragment;

import android.app.SearchManager;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.text.TextUtils;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnLongClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.PopupWindow;
import android.widget.SearchView;
import android.widget.SearchView.OnQueryTextListener;
import android.widget.Toast;
import android.widget.Button;
import com.hifive.Constants;
import com.hifive.R;
import com.hifive.adapter.ProfileAdapter;
import com.hifive.provider.ProfileProvider;
import com.hifive.util.EventUtil;
import com.hifive.util.FBApiUtil;
import com.hifive.util.GlobalApplication;

public class ProfileListFragment extends ListFragment implements
	LoaderManager.LoaderCallbacks<Cursor>{
	private static final String LOG_TAG = "hifive.profileListFrag";

    // Identifies a particular Loader being used in this component
    private static final int URL_LOADER = 0;
    //private static final String[] PROJECTION = EventProvider.Event.PROJECTION;
    private static final String[] PROJECTION = ProfileProvider.Profile.PROJECTION;
    private String SORTORDER = ProfileProvider.Profile.COLUMN_NAME_HFTIME;
    private PopupWindow mpopup;

    // The adapter that binds our data to the ListView
    private ProfileAdapter mAdapter;
    Button btnCancel;
    Button btnConfirm;
    private Button btSortAbc;
    private Button btSortTime;
    int selInd;
    
    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
        Bundle savedInstanceState) {
        /*
         * Initializes the CursorLoader. The URL_LOADER value is eventually passed
         * to onCreateLoader().
         */
        getLoaderManager().initLoader(URL_LOADER, null, this);

        // Now create an array adapter and set it to display using our row
        mAdapter = new ProfileAdapter(getActivity(), null, 0);

        setListAdapter(mAdapter);
       
        Log.d(LOG_TAG, "mAdapter length"+String.valueOf(mAdapter.getCount()));

        View view = inflater.inflate(R.layout.fragment_profilelist, null);
        btSortAbc = (Button)view.findViewById(R.id.profilelist_abc);
        btSortAbc.setOnClickListener(sortAbcOnClickListener);
        btSortTime = (Button)view.findViewById(R.id.profilelist_time);
        btSortTime.setOnClickListener(sortTimeOnClickListener);


        
        return view;
    };
    public boolean onCreateOptionsMenu(Menu menu) {
		return false;
    	
    }
    @Override
    public void onActivityCreated(Bundle savedState) {
        super.onActivityCreated(savedState);

        getListView().setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {

            public boolean onItemLongClick(AdapterView<?> arg0, View v,
                    int index, long arg3) {

                View popUpView = getActivity().getLayoutInflater().inflate(R.layout.popup_remove_hifive, null); // inflating popup layout
                mpopup = new PopupWindow(popUpView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true); //Creation of popup
                mpopup.setAnimationStyle(android.R.style.Animation_Dialog);   
                mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0);    // Displaying popup
                btnCancel = (Button)popUpView.findViewById(R.id.rmhifive_btncancel);
                btnCancel.setOnClickListener(cancelOnClickListener);
                selInd=index;
                btnConfirm = (Button) popUpView.findViewById(R.id.rmhifive_btnok);
                btnConfirm.setOnClickListener(confirmOnClickListener);

                
                 Log.d(LOG_TAG,"long click : " +index);
                return true;
            }
        }); 
        setHasOptionsMenu(true);
        //getActivity().setDefaultKeyMode(getActivity().DEFAULT_KEYS_SEARCH_LOCAL); 
        //handleIntent(getActivity().getIntent());
    }
    @Override 
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        inflater.inflate(R.menu.grid_default, menu); 
        SearchView searchView = (SearchView)menu.findItem(R.id.grid_default_search).getActionView();
        searchView.setOnQueryTextListener(queryListener);
    }
    
    private String grid_currentQuery = null; // holds the current query...

    final private OnQueryTextListener queryListener = new OnQueryTextListener() {       

        @Override
        public boolean onQueryTextChange(String newText) {
            if (TextUtils.isEmpty(newText)) {
                //getActivity().getActionBar().setSubtitle("List");               
                grid_currentQuery = null;
            } else {
                //getActivity().getActionBar().setSubtitle("List - Searching for: " + newText);
                grid_currentQuery = newText;

            }   
            getLoaderManager().restartLoader(0, null, ProfileListFragment.this); 
            return false;
        }

        @Override
        public boolean onQueryTextSubmit(String query) {            
            Toast.makeText(getActivity(), "Searching for: " + query + "...", Toast.LENGTH_SHORT).show();
            return false;
        }
    };
    
    private void handleIntent(Intent intent) {
    	 if (Intent.ACTION_SEARCH.equals(intent.getAction())) {
    	         String query =
    	               intent.getStringExtra(SearchManager.QUERY);
    	         doSearch(query);
    	      }
    } 
    private void doSearch(String queryStr) {
    	Log.d(LOG_TAG,"search string : " +queryStr);
    }
	Button.OnClickListener cancelOnClickListener = 
	new OnClickListener() {
	    public void onClick(View v) {
	    	mpopup.dismiss(); //dismissing the popup
	    	
	}       
	}; 
	Button.OnClickListener sortAbcOnClickListener = 
	new OnClickListener() {
	    public void onClick(View v) {
	    	Log.d(LOG_TAG, "sortAbcOnClickListener:"+SORTORDER);
	    	SORTORDER = ProfileProvider.Profile.COLUMN_NAME_LASTNAME;
	    	getLoaderManager().restartLoader(0, null, ProfileListFragment.this); 
	    	
	}       
	}; 
	Button.OnClickListener sortTimeOnClickListener = 
	new OnClickListener() {
	    public void onClick(View v) {
	    	Log.d(LOG_TAG, "sortTimeOnClickListener:"+SORTORDER);
	    	SORTORDER = ProfileProvider.Profile.COLUMN_NAME_HFTIME;
	    	getLoaderManager().restartLoader(0, null, ProfileListFragment.this); 
	    	
	}       
	}; 
	Button.OnClickListener confirmOnClickListener = 
	new OnClickListener() {
	    public void onClick(View v) {
	    	getActivity().getContentResolver().delete(
                    ProfileProvider.Profile.CONTENT_URI, 
                    ProfileProvider.Profile._ID + "=?", 
                    new String[] {String.valueOf(selInd+1)} 
                    );
        	mAdapter.notifyDataSetChanged();
        	mpopup.dismiss(); //dismissing the popup
        	
	    	
	}       
	}; 
	private void showPopUp(){
       // LayoutInflater layoutInflater 
       // = (LayoutInflater)getSystemService(Context.LAYOUT_INFLATER_SERVICE); 
    }
    @Override
    public Loader<Cursor> onCreateLoader(int loaderID, Bundle arg1) {
        /*
         * Takes action based on the ID of the Loader that's being created
         */
    	String[] selClause=null;
    	String grid_whereClause = null;
        Log.d(LOG_TAG, "onCreateLoader: Sort order:"+SORTORDER);
        if (!TextUtils.isEmpty(grid_currentQuery)) { 
        	selClause=new String[] { grid_currentQuery + "%" , grid_currentQuery + "%",((GlobalApplication) getActivity().getApplication()).getHFID() };
        	//grid_whereClause =  ProfileProvider.Profile.COLUMN_NAME_LASTNAME+" LIKE ?";
        	grid_whereClause = "("+ProfileProvider.Profile.COLUMN_NAME_LASTNAME+" LIKE ?"+" OR "+ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME+" LIKE ?"+")"+" AND "+ProfileProvider.Profile.COLUMN_NAME_USERID+" <> ?";
        	//grid_whereClause =  ProfileProvider.Profile.COLUMN_NAME_LASTNAME+" LIKE ?"+" OR "+ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME+" LIKE ?";
        	//+	"'"+grid_currentQuery + "%' ";
        }else{
        	selClause=new String[] { ((GlobalApplication) getActivity().getApplication()).getHFID() };
        	grid_whereClause =  ProfileProvider.Profile.COLUMN_NAME_USERID+" <> ?";
        }
        
        switch (loaderID) {
            case URL_LOADER:
                // Returns a new CursorLoader
                return new CursorLoader(
                            getActivity(),   // Parent activity context
                            ProfileProvider.Profile.CONTENT_URI,        // Table to query
                            PROJECTION,      // Projection to return
                            grid_whereClause,            // No selection clause
                            selClause,            // No selection arguments
                            SORTORDER             // Default sort order
            );
            default:
                // An invalid id was passed in
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
        mAdapter.setSelectedPosition(-1);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

    // Opens the second activity if an entry is clicked
    @Override
    public void onListItemClick(ListView l, View v, int position, long id) {
        Toast.makeText(getActivity().getBaseContext(), "onclick item "+String.valueOf(id), Toast.LENGTH_SHORT).show();
        super.onListItemClick(l, v, position, id);
        
        Cursor c = getActivity().getContentResolver().query(
                ProfileProvider.Profile.CONTENT_URI, 
                ProfileProvider.Profile.PROJECTION, 
                ProfileProvider.Profile._ID + "=?", 
                new String[] {String.valueOf(id)}, 
                null);
        c.moveToFirst();
        if (c.getCount() != 0) {
        	for(int i=0;i<Constants.HFDBNumElmt;i++){
        		String key=ProfileProvider.Profile.PROJECTION[i+1];
        		String tmpStr=c.getString(c.getColumnIndexOrThrow(key));
        		((GlobalApplication) getActivity().getApplication()).setRecvHFData(key,tmpStr);
        	}
        	Fragment mFragment = new HiFiveFragment(); 
        	getActivity().getSupportFragmentManager().beginTransaction()
        	             .replace(R.id.content_frame, mFragment ).commit();
        }else{
        	Toast.makeText(getActivity().getBaseContext(), "no db!"+String.valueOf(id), Toast.LENGTH_SHORT).show();
        }
        
        //EventUtil.upload2WS(id);
        
        /*
        Intent i = new Intent(getActivity(), HistoryDetailActivity.class);
        Uri historyUri = Uri.parse(HistoryProvider.CONTENT_URI + "/" + id);
        i.putExtra(HistoryProvider.CONTENT_ITEM_TYPE, historyUri);
        startActivity(i);
        */
    }
}
