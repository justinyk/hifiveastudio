package com.hifive.util;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.Date;

import com.facebook.internal.Utility;
import com.facebook.widget.ProfilePictureView;
import com.hifive.R;
import com.hifive.fragment.HiFiveFragment;

import android.app.Activity;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.media.ExifInterface;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.provider.MediaStore.Images.Media;
import android.util.Log;
import android.util.Pair;
import android.widget.ImageView;

/** 
 * non activity class. only for picture function handle
 * @author PCH
 *
 */
public class PictureUtil {
    private static final String LOG_TAG = "hifive.PictureUtil";
    private static final String HFALBUMNAME="HifiveFacebookImages";
    private static final int maxWidth=600;
    /**
     * get the real path from URI for the selected photo
     * @param mActivity
     * @param contentURI
     * @return
     */
    public static String getRealPathFromURI(Activity mActivity, Uri contentURI) {
        
        String result;
        String[] projection = {MediaStore.Images.Media.DATA};
        Cursor cursor = mActivity.getContentResolver().query(contentURI, projection, null, null, null);
        if (cursor == null) { // Source is Dropbox or other similar local file path
            result = contentURI.getPath();
        } else {
            Log.d(LOG_TAG, "photo query != null, cnt:"+cursor.getCount());
            cursor.moveToFirst(); 
            int idx = cursor.getColumnIndexOrThrow(MediaStore.Images.Media.DATA);
            Log.d(LOG_TAG, "get ColumnIndex:"+idx);
            result = cursor.getString(idx);
            Log.d(LOG_TAG, "result:"+result);
            cursor.close();
        }
        return result;
        
        /*
        String[] projection = { MediaColumns.DATA };
        Cursor cursor = mActivity.getContentResolver().query(contentURI, projection, null, null, null);
        int column_index = cursor.getColumnIndexOrThrow(MediaColumns.DATA);
        cursor.moveToFirst();
        return cursor.getString(column_index);
        */
    }
    
    /**
     * decode the path picture, scale it, square it
     * set to image view
     * return its byte stream
     * @param mCurrentPhotoPath
     * @param mImageView
     * @return
     */
    public static byte[] setPicture(String mCurrentPhotoPath, ImageView mImageView){
        // Get the dimensions of the View
        int targetSize = mImageView.getWidth();

        // Get the dimensions of the bitmap
        BitmapFactory.Options bmOptions = new BitmapFactory.Options();
        bmOptions.inJustDecodeBounds = true;
        BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        int photoW = bmOptions.outWidth;
        int photoH = bmOptions.outHeight;
        Log.d(LOG_TAG,"photo size: photoW:"+photoW+" photoH:"+photoH);
        
        int scaleFactor;
        if (photoW > photoH) {
            scaleFactor = photoH/targetSize;
        }else{
            scaleFactor = photoW/targetSize;
        }
        
        // Decode the image file into a Bitmap sized to fill the View
        bmOptions.inJustDecodeBounds = false;
        bmOptions.inSampleSize = scaleFactor;
        bmOptions.inPurgeable = true;

        Bitmap bitmap = BitmapFactory.decodeFile(mCurrentPhotoPath, bmOptions);
        
        photoW = bitmap.getWidth();
        photoH = bitmap.getHeight();
        Log.d(LOG_TAG,"scale photo size: photoW:"+photoW+" photoH:"+photoH);
        
        int x, y, width, height;
        if (photoW > photoH) {
            x = (photoW-photoH)/2;
            y = 0;
            width = photoH;
            height = photoH;
        } else {
            x = 0;
            y = (photoH-photoW)/2;
            width = photoW;
            height = photoW;
        }
        Bitmap squarePic = Bitmap.createBitmap(bitmap, x, y, width, height);
        mImageView.setImageBitmap(squarePic);
               
        ByteArrayOutputStream bos = new ByteArrayOutputStream();
        squarePic.compress(Bitmap.CompressFormat.PNG, 100, bos);
        
        return bos.toByteArray();
    }
    public static Bitmap annotateBmp(final Activity mActivity,String annotateTxt,Bitmap bm) {
	    Canvas c = new Canvas(bm);
	    Paint p = new Paint();

	    int width=bm.getWidth();
	    int height=bm.getHeight();
	    int sx=(int) (width*0.1);
	    int sy=(int) (height*0.85);
	    p.setColor(mActivity.getResources().getColor(R.color.silver));
	    p.setTypeface(Typeface.DEFAULT_BOLD);
	    p.setTextSize((int)(height*0.20));
	    p.setStyle(Paint.Style.STROKE);
	    p.setStrokeWidth(2);
	    c.drawText(annotateTxt, sx, sy, p);

	return bm;
    }
    
    private static String getMapUrl(Location loc) 
    {
    	double Lat=loc.getLatitude();
    	double Long=loc.getLongitude();
    	String locStr=String.valueOf(Lat)+","+String.valueOf(Long);
    	String urlStr="http://maps.googleapis.com/maps/api/staticmap?center="+locStr+"&zoom=13&size=600x300&maptype=roadmap&markers=color:orange|"+locStr;
        return urlStr;
    }
    private static Bitmap getBitmapFromUrl(String uStr) 
    {
        try {
            URL url = new URL(uStr);
            HttpURLConnection connection = (HttpURLConnection) url.openConnection();
            connection.setDoInput(true);
            connection.connect();
            InputStream input = connection.getInputStream();
            Bitmap myBitmap = BitmapFactory.decodeStream(input);
            return myBitmap;
        } catch (IOException e) {
            e.printStackTrace();
            return null;
        }
    }
    public static Bitmap getRotatedBitmap(final Activity mActivity,final String imgPath,final Bitmap iBitmap) {
    	Bitmap rBitmap=null;
    	try {
            ExifInterface exif = new ExifInterface(imgPath);
            int orientation = exif.getAttributeInt(ExifInterface.TAG_ORIENTATION, 1);
            Log.d("EXIF", "Exif: " + orientation);
            Matrix matrix = new Matrix();
            if (orientation == 6) {
                matrix.postRotate(90);
            }
            else if (orientation == 3) {
                matrix.postRotate(180);
            }
            else if (orientation == 8) {
                matrix.postRotate(270);
            }
            rBitmap = Bitmap.createBitmap(iBitmap, 0, 0, iBitmap.getWidth(), iBitmap.getHeight(), matrix, true); // rotating bitmap
            return rBitmap;
        }
        catch (Exception e) {
        	return rBitmap;
        }

    }
    public static  Bitmap scaleByWidth(Bitmap iBitmap,int dWidth) {
        double dwf= dWidth;
        double owf= iBitmap.getWidth();
        double ohf= iBitmap.getHeight();
        int dHeight = (int) ((dwf/owf)*ohf);
	    Bitmap oBitmap = Bitmap.createScaledBitmap(iBitmap,dWidth,dHeight,true);
    	return oBitmap;
    }
    public  Bitmap scaleByHeight(Bitmap iBitmap,int dHeight) {
   	 int dWidth = (dHeight/iBitmap.getHeight())*iBitmap.getWidth();
   	 Bitmap oBitmap = Bitmap.createScaledBitmap(iBitmap,dWidth,dHeight,true);
   	 return oBitmap;
   }
              
    public static Pair<File, Integer> getImageFileAndMinDimension(final Activity mActivity,final String imgPath) {
    	File photoFile = null;
    	if(imgPath!=null){
    		photoFile = new File(imgPath);
        }else{
        	photoFile = null;
        }
        Bitmap eventPicBitmap=null;
        Bitmap mapBitmap=null;
            InputStream is = null;
            try {
            	String mapUrl=getMapUrl(((GlobalApplication) mActivity.getApplication()).getLocation());
            	int baseWidth;    
            	if (photoFile != null) {
	                is = new FileInputStream(photoFile);
	                // We only want to get the bounds of the image, rather than load the whole thing.
	                BitmapFactory.Options options = new BitmapFactory.Options();
	                options.inJustDecodeBounds = false;//We want the bound and load image for edit
	                //eventPicBitmap=BitmapFactory.decodeStream(is, null, options);
	                eventPicBitmap=getRotatedBitmap(mActivity,imgPath,BitmapFactory.decodeStream(is, null, options));
	                baseWidth = (eventPicBitmap.getWidth()>maxWidth)?maxWidth:eventPicBitmap.getWidth();
	                eventPicBitmap=scaleByWidth(eventPicBitmap,baseWidth);
	            }else{
            		mapBitmap = getBitmapFromUrl(mapUrl);
            		baseWidth = maxWidth;
 	            }
            	mapBitmap=scaleByWidth(mapBitmap,baseWidth);
                
                
            	Bitmap user1ProfBitmap=getProfBitmapFromProfView(HiFiveFragment.profilePictureView);
                Bitmap user2ProfBitmap=getProfBitmapFromProfView(HiFiveFragment.profilePictureView2);
                String user1Name=((GlobalApplication) mActivity.getApplication()).getUserName();
                String user2Name=((GlobalApplication) mActivity.getApplication()).getRecvName();
                
                
                Bitmap hifiveBitmap = BitmapFactory.decodeResource(mActivity.getResources(),R.drawable.hfimage);
                int dWidth=200;
                
                hifiveBitmap=scaleByWidth(hifiveBitmap,dWidth);
            	
                //hifiveBitmap=createTransparentBitmapFromBitmap(hifiveBitmap,Color.WHITE);
                
                
                int profImgWidth=40;
            	user1ProfBitmap=scaleByWidth(user1ProfBitmap,profImgWidth);
            	user2ProfBitmap=scaleByWidth(user2ProfBitmap,profImgWidth);
            	
            	
                //user1ProfBitmap = Bitmap.createScaledBitmap(user1ProfBitmap,baseWidth,baseHeight,true);
                //user2ProfBitmap = Bitmap.createScaledBitmap(user2ProfBitmap,baseWidth,baseHeight,true);
                
                
                //user1ProfBitmap=annotateBmp(mActivity,user1Name,user1ProfBitmap);
                //user2ProfBitmap=annotateBmp(mActivity,user2Name,user2ProfBitmap);
                
                //eventPicBitmap = Bitmap.createScaledBitmap(eventPicBitmap,baseWidth*3+12,baseHeight*2,true);
                
                Bitmap fbHfBitmap= compileHFImage(user1ProfBitmap,user2ProfBitmap,mapBitmap,hifiveBitmap) ;
                if (photoFile != null) {
                	fbHfBitmap= addUserImage(fbHfBitmap,eventPicBitmap,3,3);
                }
                byte[]  fbHfOutputByteArray=bitmap2ByteArray(fbHfBitmap);
                String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        		String fileName = "HFFB" + timeStamp;
        		photoFile=writeToGallery(mActivity,HFALBUMNAME,fileName,fbHfOutputByteArray);
                Log.d("EatAction Seq", "photoStagingUri 13");
                return new Pair<File, Integer>(photoFile, Math.min(fbHfBitmap.getWidth(), fbHfBitmap.getHeight()));
                //String saved=MediaStore.Images.Media.insertImage(getContentResolver(), fbHfBitmap, "HFCompPic" , "HFCompPic");
               
                //return new Pair<File, Integer>(photoFile, Math.min(options.outWidth, options.outHeight));
            } catch (Exception e) {
                return null;
            } finally {
            	
                
                /*if(saved==null)
        		{
        			Toast.makeText(getBaseContext(), "fALED Saved  HFCompPic.png !!", Toast.LENGTH_SHORT).show();
        		}
        		else{
        			Toast.makeText(getBaseContext(), "Saved  HFCompPic.png !! TO"+saved, Toast.LENGTH_SHORT).show();
        		}
                Log.d("EatAction Seq", "photoStagingUri 14");*/
                Utility.closeQuietly(is);
            }

    }
    
    /**
     * create a transparent bitmap from an existing bitmap by replacing certain
     * color with transparent
     * 
     * @param bitmap
     *            the original bitmap with a color you want to replace
     * @return a replaced color immutable bitmap
     */
    public static Bitmap createTransparentBitmapFromBitmap(Bitmap bitmap,
        int replaceThisColor) {
    	Log.d(LOG_TAG, "createTransparentBitmapFromBitmap");
      if (bitmap != null) {
        int picw = bitmap.getWidth();
        int pich = bitmap.getHeight();
        int[] pix = new int[picw * pich];
        bitmap.getPixels(pix, 0, picw, 0, 0, picw, pich);
        Log.d(LOG_TAG, "image value:"+String.valueOf( pix[20 * picw + 20]));
        
        for (int y = 0; y < pich; y++) {
          // from left to right
          for (int x = 0; x < picw; x++) {
            int index = y * picw + x;
            int r = (pix[index] >> 16) & 0xff;
            int g = (pix[index] >> 8) & 0xff;
            int b = pix[index] & 0xff;

            if (pix[index] == replaceThisColor) {
              pix[index] = Color.TRANSPARENT;
            } else {
              break;
            }
          }

          // from right to left
          for (int x = picw - 1; x >= 0; x--) {
            int index = y * picw + x;
            int r = (pix[index] >> 16) & 0xff;
            int g = (pix[index] >> 8) & 0xff;
            int b = pix[index] & 0xff;

            if (pix[index] == replaceThisColor) {
              pix[index] = Color.TRANSPARENT;
            } else {
              break;
            }
          }
        }

        Bitmap bm = Bitmap.createBitmap(pix, picw, pich,
            Bitmap.Config.ARGB_4444);

        return bm;
      }
      return null;
    }
    
	private static void galleryAddPic(final Activity mActivity,String fpath) {
	    Intent mediaScanIntent = new Intent("android.intent.action.MEDIA_SCANNER_SCAN_FILE");
		File f = new File(fpath);
	    Uri contentUri = Uri.fromFile(f);
	    mediaScanIntent.setData(contentUri);
	    mActivity.sendBroadcast(mediaScanIntent);
}
    private static File writeToGallery(final Activity mActivity,String fpath, String fname,byte[] bytes) 
    {
    	Uri uriTarget = mActivity.getContentResolver().insert(Media.EXTERNAL_CONTENT_URI, new ContentValues());
    	try {
         String path = Environment.getExternalStorageDirectory().toString();
         String dirpath=path +"/"+ fpath;
         File myNewFolder = new File(dirpath);
         myNewFolder.mkdir();
         File file = new File(dirpath, fname    + ".png");
         OutputStream imageFileOS = new FileOutputStream(file);
         imageFileOS.write(bytes);
         imageFileOS.flush();
         imageFileOS.close();
         galleryAddPic(mActivity,file.getAbsolutePath());
         return file;
         
        } catch (FileNotFoundException e) {
         // TODO Auto-generated catch block
        	Log.d("EatAction Seq", "File not found!");
         e.printStackTrace();
         return null;
        } catch (IOException e) {
         // TODO Auto-generated catch block
         e.printStackTrace();
         	Log.d("EatAction Seq", "IO exception");
         return null;
        }
    }
    private static byte[] bitmap2ByteArray(Bitmap bmp) 
    {
    	byte[] bytes;
  		ByteArrayOutputStream bos = new ByteArrayOutputStream();
  		bmp.compress(Bitmap.CompressFormat.PNG, 100, bos);
  		bytes= bos.toByteArray();
  		return bytes;
    }
    private static Bitmap compileHFImage(Bitmap user1ProfBitmap,Bitmap user2ProfBitmap,Bitmap mapBitmap,Bitmap hifiveBitmap) {
        int width, height = 0; 
        
        width  = mapBitmap.getWidth();
    	height = mapBitmap.getHeight();
    	Bitmap finalImage = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
    	Canvas comboImage = new Canvas(finalImage);
    	comboImage.drawBitmap(mapBitmap, 0, 0, null);
	    int hpos=mapBitmap.getWidth()/2-hifiveBitmap.getWidth()/2;
	    int vpos=mapBitmap.getHeight()/2-hifiveBitmap.getHeight()/2;
	    comboImage.drawBitmap(hifiveBitmap, hpos, vpos, null);
	    int hpos_1=(int) (hpos+((double)103/(double)445)*(double)(hifiveBitmap.getHeight()));
	    int vpos_1=(int) (vpos+((double)-21/(double)336)*(double)(hifiveBitmap.getWidth()));
	    comboImage.drawBitmap(user1ProfBitmap, hpos_1, vpos_1, null); 
	    hpos_1=(int) (hpos+((double)320/(double)445)*(double)(hifiveBitmap.getHeight()));
	    vpos_1=(int) (vpos+((double)-31/(double)336)*(double)(hifiveBitmap.getWidth()));
	    comboImage.drawBitmap(user2ProfBitmap, hpos_1, vpos_1, null); 
	    return finalImage;
    }
    private static Bitmap addUserImage(Bitmap hfBitmap,Bitmap userBitmap,int hoff,int voff) {
    	
        int width, height = 0; 
        
        width  = userBitmap.getWidth()+hoff*2;
    	height = userBitmap.getHeight()+hfBitmap.getHeight()+3*voff;
    	
    	Bitmap finalImage = Bitmap.createBitmap(width, height, Bitmap.Config.ARGB_8888);
    	Canvas comboImage = new Canvas(finalImage);
    	
    	//comboImage.drawBitmap(user1ProfBitmap, hoff, voff, null); 
	    comboImage.drawBitmap(hfBitmap, hoff, voff, null);
	    comboImage.drawBitmap(userBitmap, hoff, hfBitmap.getHeight()+2*voff, null);
	    
	    return finalImage;
        

        
    }
    /**
     * Gets profile picture image bitmap from user facebook ID
     */
    private static Bitmap getProfBitmapFromProfView(final ProfilePictureView profView) {
    	ImageView fbImage = ( ( ImageView)profView.getChildAt( 0));
    	Bitmap    bitmap  = ( ( BitmapDrawable) fbImage.getDrawable()).getBitmap();
    	return bitmap;
    }
}
