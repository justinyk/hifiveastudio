package com.hifive.fragment;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.os.Bundle;
import android.view.View;
import com.hifive.R;
public class LogOffFragment extends Fragment {
    private static final String LOG_TAG = "hifive.LogOffFragment";
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    	Log.d(LOG_TAG, "onCreateView ");
        View rootView = inflater.inflate(R.layout.fragment_logout, container, false);
        return rootView;
    }

  

}