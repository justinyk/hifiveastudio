package com.hifive.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Typeface;
import android.graphics.drawable.BitmapDrawable;
import android.location.Location;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.util.Pair;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.facebook.FacebookRequestError;
import com.facebook.HttpMethod;
import com.facebook.Request;
import com.facebook.RequestBatch;
import com.facebook.Response;
import com.facebook.Session;
import com.facebook.SessionDefaultAudience;
import com.facebook.internal.Utility;
import com.facebook.model.GraphObject;
import com.facebook.model.GraphUser;
import com.facebook.model.OpenGraphAction;
import com.facebook.model.OpenGraphObject;
import com.facebook.widget.ProfilePictureView;
import com.hifive.R;
import com.hifive.activity.ProfileEditActivity;
import com.hifive.fragment.HiFiveFragment;
import com.hifive.fragment.ProfileFragment;
import com.hifive.provider.ProfileProvider;




/** 
 * Api function access facebook api
 * @author Justin
 *
 */
public class FBApiUtil  {
    
	private final Context mContext;
	private static final String LOG_TAG = "hifive.FbApiUtil";
	private static final String HIFIVE_ACTION_TYPE = "hifivebeam:hifive";
	private static final String PERMISSION = "publish_actions";
	private static boolean pendingAnnounce;
	private static ProgressDialog progressDialog;
	static final Uri M_FACEBOOK_URL = Uri.parse("http://m.facebook.com");
	private static final int REAUTH_ACTIVITY_CODE = 100;
	private static final int USER_GENERATED_MIN_SIZE = 480;
    public FBApiUtil(Context context) 
    {
        this.mContext = context;
    }

    public static void openFBProfile(final Activity mActivity,final String userFbId) {
		try{
			Intent followIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/"+userFbId));
			//followIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			mActivity.startActivity(followIntent);
		   /*
           final Handler handler = new Handler();
           handler.postDelayed(new Runnable()
           {
               @Override
               public void run() {
                   Intent followIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("fb://profile/"+userFbId));
                   followIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                   mActivity.startActivity(followIntent);
               }
           }, 1000 * 2);*/
		}catch (Exception e) {
			String facebookScheme = "https://m.facebook.com/" + userFbId;
			Intent facebookIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(facebookScheme)); 
			facebookIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
			mActivity.startActivity(facebookIntent);			
		}
		
	}
    /**
     * Used to inspect the response from posting an action
     */
    private interface PostResponse extends GraphObject {
        String getId();
    }
    /** 
     * update Hifive Fragment UI according to Hifive data 
     */
    public static void  updateHfUIData(String recvHFID,String name) {
//  	   HiFiveFragment.profilePictureView2.setProfileId(recvHFID);
//  	   HiFiveFragment.fbUserNameButton2.setText(name);
 	}
    /** 
     * view hided Hifive UI button 
     */
    public static void  viewHfUI() {
    	Log.d(LOG_TAG, "viewHfUI ");
  //  	HiFiveFragment.profilePictureView2.setVisibility(View.VISIBLE);
  //  	HiFiveFragment.fbUserNameButton2.setVisibility(View.VISIBLE);//view the button after the event
 	}   
    /** 
     * after hifive event access FB API with received facebook ID and get hifive opponents data (name , relationship with user)
     * When the query is complete call updateHfUIData and viewHfUI 
     */
    /*
    public static void updateUiFromId(final Activity mActivity,final String userFbId,Session session) 
    {
    	//final String firstName="";
    	Log.d("updateUiFromId","start ");
    
	    new Request(
		        session,
		        "/"+userFbId,
		        null,
		        HttpMethod.GET,
		        new Request.Callback() {
		            public void onCompleted(Response response) {
		              
		            	Log.d("getFirstNameFromId response", response.toString());
		                try
		                {
		                    GraphObject go  = response.getGraphObject();
		                    JSONObject  jso = go.getInnerJSONObject();
		                    String name = jso.getString("first_name");
		                    //updateHfUIData(userFbId,name);
		                    ((GlobalApplication) mActivity.getApplication()).setRecvName(name);
		                }
		                catch ( Throwable t )
		                {
		                	((GlobalApplication) mActivity.getApplication()).setRecvName("");
		                	t.printStackTrace();
		                }
		            }
		        }
		    ).executeAsync();
	 
    	new Request(
        	    session,
        	    "/me/friends/"+userFbId,
        	    null,
        	    HttpMethod.GET,
        	    new Request.Callback() {
        	        public void onCompleted(Response response) {
        	          
        	        	Log.d("isFriend response", response.toString());
		                try
		                {
		                    GraphObject go  = response.getGraphObject();
		                    JSONObject  jso = go.getInnerJSONObject();
		                    JSONArray   arr = jso.getJSONArray( "data" );
		                    JSONObject json_obj = arr.getJSONObject( 0 );
		                    String name   = json_obj.getString("name");
		                    //HiFiveFragment.fbUserNameButton2.setBackgroundResource(R.drawable.facebookbackground);
		                    ((GlobalApplication) mActivity.getApplication()).setIsFriend(true);
			            }catch ( Throwable t ){
		                	//HiFiveFragment.fbUserNameButton2.setBackgroundResource(R.drawable.facebookbackgroundaddfriend);
		                	((GlobalApplication) mActivity.getApplication()).setIsFriend(false);
		                }finally{
		                	//viewHfUI();
		                	//((GlobalApplication) mActivity.getApplication()).setIsFriend(false);
		                }
        	        }
        	    }
        	).executeAsync();

    }
    */
    
    public static void handleFbRequest(final Activity mActivity,final String userFbId,final String recvFbId,final String imgPath,final Session session) 
    {
    	//final String firstName="";
    	Log.d("updateUiFromId","start ");
    	
    	AsyncTask<Void, Void, List<Response>> task = new AsyncTask<Void, Void, List<Response>>() {
            @Override
            protected List<Response> doInBackground(Void... voids) {
            	Log.d("updateUiFromId","doInBackground ");
            	RequestBatch requestBatch = new RequestBatch();
                Request request = 	    new Request(
        		        session,
        		        "/"+recvFbId,
        		        null,
        		        HttpMethod.GET,
        		        new Request.Callback() {
        		            public void onCompleted(Response response) {
        		            }
        		        }
        		    );
                requestBatch.add(request);
                request = new Request(
                	    session,
                	    "/me/friends/"+recvFbId,
                	    null,
                	    HttpMethod.GET,
                	    new Request.Callback() {
                	        public void onCompleted(Response response) {
                	        }
                	    }
                	);
                requestBatch.add(request);
                
                return requestBatch.executeAndWait();

            }
            @Override
            protected void onPostExecute(List<Response> responses) {
            	Log.d("updateUiFromId","onPostExecute response:");
                // We only care about the last response, or the first one with an error.
            	int rcnt=0;
                for (Response response : responses) {
                	Log.d("updateUiFromId response:", response.toString());
                	if(rcnt==0){
		                try
		                {
		                    GraphObject go  = response.getGraphObject();
		                    JSONObject  jso = go.getInnerJSONObject();
		                    String name = jso.getString("first_name");
		                    //updateHfUIData(userFbId,name);
		                    ((GlobalApplication) mActivity.getApplication()).setRecvName(name);
		                }
		                catch ( Throwable t )
		                {
		                	((GlobalApplication) mActivity.getApplication()).setRecvName("");
		                	t.printStackTrace();
		                }
                	} else{
                		try
		                {
		                    GraphObject go  = response.getGraphObject();
		                    JSONObject  jso = go.getInnerJSONObject();
		                    JSONArray   arr = jso.getJSONArray( "data" );
		                    JSONObject json_obj = arr.getJSONObject( 0 );
		                    String name   = json_obj.getString("name");
		                    //HiFiveFragment.fbUserNameButton2.setBackgroundResource(R.drawable.facebookbackground);
		                    ((GlobalApplication) mActivity.getApplication()).setIsFriend(true);
			            }catch ( Throwable t ){
		                	//HiFiveFragment.fbUserNameButton2.setBackgroundResource(R.drawable.facebookbackgroundaddfriend);
		                	((GlobalApplication) mActivity.getApplication()).setIsFriend(false);
		                }
                	}
                	
                	rcnt++;
                }
                HiFiveFragment.setRecvUI(mActivity);
                //handleGraphApiAnnounce(mActivity,session,userFbId,recvFbId,imgPath);
            }
        };
        task.execute();
    	
    }
    
    /**
     * Interface representing the Meal Open Graph object.
     */
    private interface FriendGraphObject extends OpenGraphObject {
        public String getUrl();
        public void setUrl(String url);

        public String getId();
        public void setId(String id);
    }

    /**
     * Interface representing the Eat action.
     */
    private interface HiFiveAction extends OpenGraphAction {
        public FriendGraphObject getFriend();
        //public void setFriend(FriendGraphObject meal);
        public void setFriend(FriendGraphObject meal);
        
    }
    private static GraphObject getImageObject(String uri, boolean userGenerated) {
        GraphObject imageObject = GraphObject.Factory.create();
        imageObject.setProperty("url", uri);
        if (userGenerated) {
            imageObject.setProperty("user_generated", "true");
        }
        return imageObject;
    }
    private static List<JSONObject> getImageListForAction(String uri, boolean userGenerated) {
        return Arrays.asList(getImageObject(uri, userGenerated).getInnerJSONObject());
    }


    public static void handleGraphApiAnnounce(final Activity mActivity,Session session,final String userFbId,final String recvFbId,final String imgPath ) {
        List<String> permissions = session.getPermissions();
        if (!permissions.contains(PERMISSION)) {
            pendingAnnounce = true;
            requestPublishPermissions(mActivity,session);
            return;
        }
        // Show a progress dialog because sometimes the requests can take a while.
        Log.d(LOG_TAG, "progressDialog 0");
        progressDialog = ProgressDialog.show(mActivity, "",
        "Posting", true);
        Log.d(LOG_TAG, "progressDialog 1");
        //progressDialog = ProgressDialog.show(this, "",
        //this.getResources().getString(R.string.progress_dialog_text), true);
        // Run this in a background thread so we can process the list of responses and extract errors.
        AsyncTask<Void, Void, List<Response>> task = new AsyncTask<Void, Void, List<Response>>() {

            @Override
            protected List<Response> doInBackground(Void... voids) {
            	HiFiveAction hiFiveAction = createHiFiveAction(mActivity,userFbId,recvFbId );
                RequestBatch requestBatch = new RequestBatch();
                //if(imgPath!=null){
                if(((GlobalApplication) mActivity.getApplication()).getUploadPicStatus()){
                	String photoStagingUri = null;
                	try {
	                		
                			Pair<File, Integer> fileAndMinDimemsion = PictureUtil.getImageFileAndMinDimension(mActivity,imgPath);
	                		if (fileAndMinDimemsion != null) {
	                            Request photoStagingRequest =
	                                    Request.newUploadStagingResourceWithImageRequest(Session.getActiveSession(),
	                                            fileAndMinDimemsion.first, null);
	                            photoStagingRequest.setBatchEntryName("photoStaging");
	                            requestBatch.add(photoStagingRequest);
	                            // Facebook SDK * pro-tip *
	                            // We can use the result from one request in the batch as the input to another request.
	                            // In this case, the result from the staging upload is "uri", which we will use as the
	                            // input into the "url" field for images on the open graph action below.
	                            photoStagingUri = "{result=photoStaging:$.uri}";
	                            hiFiveAction.setImage(getImageListForAction(photoStagingUri,
	                                    fileAndMinDimemsion.second >= USER_GENERATED_MIN_SIZE));
	                    }
                	}catch(FileNotFoundException e) {
                        // NOOP - if we can't upload the image, just skip it for now
                    }
                	
                    
                }
                FriendGraphObject friend = hiFiveAction.getFriend();
                if (friend.getCreateObject()) {
                    Request createObjectRequest =
                            Request.newPostOpenGraphObjectRequest(Session.getActiveSession(), friend, null);
                    createObjectRequest.setBatchEntryName("createObject");
                    requestBatch.add(createObjectRequest);
                    hiFiveAction.setProperty("friend", "{result=createObject:$.id}");
                }
                
                Location HFLocation=((GlobalApplication) mActivity.getApplication()).getLocation();
                hiFiveAction.setProperty("location:latitude", String.valueOf(HFLocation.getLatitude()) );
                hiFiveAction.setProperty("location:longitude", String.valueOf(HFLocation.getLongitude()));
                hiFiveAction.setExplicitlyShared(true);
                Request request = Request.newPostOpenGraphActionRequest(Session.getActiveSession(), hiFiveAction, null);
                requestBatch.add(request);
                Log.d(LOG_TAG, "waiting for request resp:");
                return requestBatch.executeAndWait();

            }

            @Override
            protected void onPostExecute(List<Response> responses) {
                // We only care about the last response, or the first one with an error.
                Response finalResponse = null;
                for (Response response : responses) {
                    finalResponse = response;
                    if (response != null && response.getError() != null) {
                        break;
                    }
                }
                onPostActionResponse(mActivity,finalResponse);
            }
        };

        task.execute();
    }

    /**
     * Resets the view to the initial defaults.
     */
    private static void init(final Activity mActivity,Bundle savedInstanceState) {
    	//HiFiveFragment.onFBPostComplete(mActivity);
    	//MainActivity.resetHFUI(mActivity);
    }
    private static void onPostActionResponse(final Activity mActivity,Response response) {
        if (progressDialog != null) {
            progressDialog.dismiss();
            progressDialog = null;
        }
        if (mActivity == null) {
            // if the user removes the app from the website, then a request will
            // have caused the session to close (since the token is no longer valid),
            // which means the splash fragment will be shown rather than this one,
            // causing activity to be null. If the activity is null, then we cannot
            // show any dialogs, so we return.
            return;
        }

        PostResponse postResponse = response.getGraphObjectAs(PostResponse.class);

        if (postResponse != null && postResponse.getId() != null) {
            showSuccessResponse(mActivity,postResponse.getId());
            //init(null);
        } else {
        	Log.d(LOG_TAG, "onPostActionResponse no handleError !! ");
            handleError(mActivity,response.getError());
        }
        init(mActivity,null);
    }
    private static void handleError(final Activity mActivity,FacebookRequestError error) {
        DialogInterface.OnClickListener listener = null;
        String dialogBody = null;

        if (error == null) {
            dialogBody = mActivity.getString(R.string.error_dialog_default_text);
        } else {
            switch (error.getCategory()) {
                case AUTHENTICATION_RETRY:
                    // tell the user what happened by getting the message id, and
                    // retry the operation later
                    String userAction = (error.shouldNotifyUser()) ? "" :
                    	mActivity.getString(error.getUserActionMessageId());
                    dialogBody = mActivity.getString(R.string.error_authentication_retry, userAction);
                    listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Intent intent = new Intent(Intent.ACTION_VIEW, M_FACEBOOK_URL);
                            mActivity.startActivity(intent);
                        }
                    };
                    break;

                case AUTHENTICATION_REOPEN_SESSION:
                    // close the session and reopen it.
                    dialogBody = mActivity.getString(R.string.error_authentication_reopen);
                    listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            Session session = Session.getActiveSession();
                            if (session != null && !session.isClosed()) {
                                session.closeAndClearTokenInformation();
                            }
                        }
                    };
                    break;

                case PERMISSION:
                    // request the publish permission
                    dialogBody = mActivity.getString(R.string.error_permission);
                    listener = new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialogInterface, int i) {
                            pendingAnnounce = true;
                            requestPublishPermissions(mActivity,Session.getActiveSession());
                        }
                    };
                    break;

                case SERVER:
                case THROTTLING:
                    // this is usually temporary, don't clear the fields, and
                    // ask the user to try again
                    dialogBody = mActivity.getString(R.string.error_server);
                    break;

                case BAD_REQUEST:
                    // this is likely a coding error, ask the user to file a bug
                    dialogBody = mActivity.getString(R.string.error_bad_request, error.getErrorMessage());
                    break;

                case OTHER:
                case CLIENT:
                default:
                    // an unknown issue occurred, this could be a code error, or
                    // a server side issue, log the issue, and either ask the
                    // user to retry, or file a bug
                    dialogBody = mActivity.getString(R.string.error_unknown, error.getErrorMessage());
                    break;
            }
        }

        new AlertDialog.Builder(mActivity)
                .setPositiveButton(R.string.error_dialog_button_text, listener)
                .setTitle(R.string.error_dialog_title)
                .setMessage(dialogBody)
                .show();
    }

    private static void showSuccessResponse(final Activity mActivity,String postId) {
        String dialogBody;
        if (postId != null) {
            dialogBody = String.format(mActivity.getString(R.string.result_dialog_text_with_id), postId);
            
            //showResultDialog(dialogBody);
        } else {
            dialogBody = mActivity.getString(R.string.result_dialog_text_default);
            //showResultDialog(dialogBody);
        }
        Toast.makeText(mActivity.getApplicationContext(), dialogBody , Toast.LENGTH_SHORT).show();
        //mActivity.finish();
    }
    private static HiFiveAction createHiFiveAction(final Activity mActivity,String HFID,String recvHFID) {
    	HiFiveAction hiFiveAction = OpenGraphAction.Factory.createForPost(HiFiveAction.class, HIFIVE_ACTION_TYPE);
    	FriendGraphObject friend = GraphObject.Factory.create(FriendGraphObject.class);
    	String hifiveFriendUrl="http://www.rateitout.com/hifive/hf_connect/fbFriendObj.html";
        friend.setUrl(hifiveFriendUrl);
        hiFiveAction.setFriend(friend);
        String rname=((GlobalApplication) mActivity.getApplication()).getRecvName();
        String locString=((GlobalApplication) mActivity.getApplication()).getLocString();
        hiFiveAction.setMessage("I hifived "+rname+" at "+locString);
        GraphUser user = GraphObject.Factory.create(GraphUser.class);
        user.setId(recvHFID);
        List<GraphUser> tags = new ArrayList<GraphUser>();
    	tags.add(user);
        hiFiveAction.setTags(tags);
        return hiFiveAction;
    }
    private static void requestPublishPermissions(final Activity mActivity,Session session) {
        if (session != null) {
            Session.NewPermissionsRequest newPermissionsRequest = new Session.NewPermissionsRequest(mActivity, PERMISSION)
                    // demonstrate how to set an audience for the publish permissions,
                    // if none are set, this defaults to FRIENDS
                    .setDefaultAudience(SessionDefaultAudience.FRIENDS)
                    .setRequestCode(REAUTH_ACTIVITY_CODE);
            session.requestNewPublishPermissions(newPermissionsRequest);
        }
    }
    public static void makeMeRequest(final Activity mActivity,final Session session) {
        Request request = Request.newMeRequest(session, new Request.GraphUserCallback() {
            @Override
            public void onCompleted(GraphUser user, Response response) {
                if (session == Session.getActiveSession()) {
                    if (user != null) {
                        //Toast.makeText(getApplicationContext(), "FB UserID:"+user.getId() , Toast.LENGTH_SHORT).show();
                    	//FBID=user.getId();
                    	((GlobalApplication) mActivity.getApplication()).setFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME,user.getFirstName());
                    	((GlobalApplication) mActivity.getApplication()).setFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_LASTNAME,user.getLastName());
                    	((GlobalApplication) mActivity.getApplication()).setFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_EMAIL,(String)user.getProperty("email"));
                    	Object work = user.getProperty("work") ;
                    	JSONArray arr = (JSONArray) work;
                    	Log.d(LOG_TAG, "get FB work data "+work.toString());
                    	Log.d(LOG_TAG, "get FB work data 1 "+work.hashCode());
	                    
                    	try {
                    		JSONObject json_obj = arr.getJSONObject( 0 );
		                    JSONObject position   = json_obj.getJSONObject("position");
		                    String posStr   = position.getString("name");
		                    ((GlobalApplication) mActivity.getApplication()).setFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_TITLE,posStr);
	                    	String orgStr=json_obj.getJSONObject("employer").getString("name");
	                    	((GlobalApplication) mActivity.getApplication()).setFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION,orgStr);
	                    	String orgUrl="www.facebook.com/"+json_obj.getJSONObject("employer").getString("id");
	                    	((GlobalApplication) mActivity.getApplication()).setFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION_LINK,orgUrl);
	                    	
	                    	
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
                    	//GraphObject go  = response.getGraphObject();
	                    //JSONObject  jso = go.getInnerJSONObject();
	                    //String name = jso.getString("first_name");
                    	
                    	//((GlobalApplication) mActivity.getApplication()).setFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION,work.toString);
                    	
                    	ProfileEditActivity.setSyncedTxt(mActivity);
                    	Log.d(LOG_TAG, "get FB user data "+user.getFirstName());
                    	
                    }
                }
                if (response.getError() != null) {
                    handleError(mActivity,response.getError());
                    Log.d(LOG_TAG, "fail to gets FBID = ");
                }
                
            }
        });
        request.executeAsync();

    }
}