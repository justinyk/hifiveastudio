package com.hifive.activity;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Context;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.widget.ProfilePictureView;
import com.hifive.Constants;
import com.hifive.MainActivity;
import com.hifive.R;
import com.hifive.provider.ProfileProvider;
import com.hifive.util.CameraUtil;
import com.hifive.util.FBApiUtil;
import com.hifive.util.GlobalApplication;
import com.hifive.util.PictureUtil;

public class ProfileEditActivity extends Activity {
    private static final String LOG_TAG = "hifive.profEdit";

    private PopupWindow mpopup;
    private CameraUtil mCamera;
    
    
    // views
    //private ImageView imProfilePic;
    private Button buttonProfileSave;
    private Button buttonFbSync;
    private static EditText etFirstName;
    private static EditText etLastName;
    private static EditText etEmail;
    private static EditText etPhone;
    private static EditText etOrgName;
    private static EditText etOrgUrl;
    private static EditText etPosName;
    private static EditText etLkdnUrl;
    
    byte[] profilePic;
    String phone;
    String email;
    public static ProfilePictureView profilePictureView;
    //private CheckBox cbShProfPic;
    //private CheckBox cbShEmail;
    //private CheckBox cbShPhone;
    //private CheckBox cbShFbId;
    
    
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_profile_edit);
		profileViewInit();
	}

    /*private void saveShChBoxVal() {
    	((GlobalApplication) getApplication()).setIsShPref(Constants.SHPREFS[0],cbShProfPic.isChecked());
    	((GlobalApplication) getApplication()).setIsShPref(Constants.SHPREFS[1],cbShEmail.isChecked());
    	((GlobalApplication) getApplication()).setIsShPref(Constants.SHPREFS[2],cbShPhone.isChecked());
    	((GlobalApplication) getApplication()).setIsShPref(Constants.SHPREFS[3],cbShFbId.isChecked());
    }
    private void setShChBoxDefault() {
    	cbShProfPic.setChecked(((GlobalApplication) getApplication()).getIsShPref(Constants.SHPREFS[0]));
    	cbShEmail.setChecked(((GlobalApplication) getApplication()).getIsShPref(Constants.SHPREFS[1]));
    	cbShPhone.setChecked(((GlobalApplication) getApplication()).getIsShPref(Constants.SHPREFS[2]));
    	cbShFbId.setChecked(((GlobalApplication) getApplication()).getIsShPref(Constants.SHPREFS[3]));
    }*/
    public static void setSyncedTxt(final Activity mActivity) {
    	etFirstName.setText(((GlobalApplication) mActivity.getApplication()).getFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME));
    	etLastName.setText(((GlobalApplication) mActivity.getApplication()).getFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_LASTNAME));
    	etEmail.setText(((GlobalApplication) mActivity.getApplication()).getFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_EMAIL));
    	Log.d(LOG_TAG, "setSyncedTxt 0");
    	etOrgName.setText(((GlobalApplication) mActivity.getApplication()).getFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION));
    	Log.d(LOG_TAG, "setSyncedTxt 1");
    	etOrgUrl.setText(((GlobalApplication) mActivity.getApplication()).getFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION_LINK));
    	Log.d(LOG_TAG, "setSyncedTxt 2");
    	etPosName.setText(((GlobalApplication) mActivity.getApplication()).getFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_TITLE));
    	Log.d(LOG_TAG, "setSyncedTxt 4");
    	
   
    	
     }
    private void profileViewInit() {
    	Log.d(LOG_TAG, "profileViewInit");
        etFirstName = (EditText) findViewById(R.id.firstName);
        etLastName = (EditText) findViewById(R.id.lastName);
        etEmail = (EditText) findViewById(R.id.user_email);
        etPhone = (EditText) findViewById(R.id.user_phone);
        etOrgName = (EditText) findViewById(R.id.user_organization);
        etOrgUrl = (EditText) findViewById(R.id.user_organization_url);
        etPosName = (EditText) findViewById(R.id.user_position);
        etLkdnUrl = (EditText) findViewById(R.id.user_linkedin_url);
        etFirstName.setImeOptions(EditorInfo.IME_ACTION_DONE);
        
        Log.d(LOG_TAG, "profileViewInit 1");
//        imProfilePic= (ImageView)findViewById(R.id.profile_pic);
//        imProfilePic.setOnClickListener(profilePicOnClickListener);
        profilePictureView = (ProfilePictureView) findViewById(R.id.profile_pic);
        profilePictureView.setCropped(true);
        profilePictureView.setVisibility(View.VISIBLE);
        profilePictureView.setProfileId(MainActivity.HFID);
        Log.d(LOG_TAG, "profileViewInit 2");
        //cbShProfPic = (CheckBox) findViewById(R.id.checkbox_profile_pic);
        //cbShEmail = (CheckBox) findViewById(R.id.checkbox_email);
        //cbShPhone = (CheckBox) findViewById(R.id.checkbox_phone);
        //cbShFbId = (CheckBox) findViewById(R.id.checkbox_fbid);
        
        //setShChBoxDefault();
        
        String tmpStr=null;
        /*Cursor c = getContentResolver().query(
                ProfileProvider.Profile.CONTENT_URI, 
                ProfileProvider.Profile.PROJECTION, 
                ProfileProvider.Profile._ID + "=?", 
                new String[] {"1"}, 
                null);*/
        Log.d(LOG_TAG, "profileViewInit 0:" + ((GlobalApplication) getApplication()).getHFID());
        Log.d(LOG_TAG, "profileViewInit 0-1:" + MainActivity.HFID);

        Cursor c = getContentResolver().query(
                ProfileProvider.Profile.CONTENT_URI, 
                ProfileProvider.Profile.PROJECTION, 
                ProfileProvider.Profile.COLUMN_NAME_USERID + "=?", 
                new String[] {((GlobalApplication) getApplication()).getHFID()}, 
                null);
        Log.d(LOG_TAG, "profileViewInit 1:" + ((GlobalApplication) getApplication()).getHFID());
        c.moveToFirst();
        Log.d(LOG_TAG, "profileViewInit 2:" + ((GlobalApplication) getApplication()).getHFID());
        if(((GlobalApplication) getApplication()).getHFID()!=null) {
            if (c.getCount() != 0) {
                String firstName = c.getString(c.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME));
                if (firstName != null) {
                    etFirstName.setText(firstName);
                }
                String lastName = c.getString(c.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_LASTNAME));
                if (lastName != null) {
                    etLastName.setText(lastName);
                }

                String email = c.getString(c.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_EMAIL));
                if (email != null) {
                    etEmail.setText(email);
                }
                String phone = c.getString(c.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_PHONE));
                if (phone != null) {
                    etPhone.setText(phone);
                }
                tmpStr = null;
                tmpStr = c.getString(c.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION));
                if (tmpStr != null) {
                    etOrgName.setText(tmpStr);
                }
                tmpStr = null;
                tmpStr = c.getString(c.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION_LINK));
                if (tmpStr != null) {
                    etOrgUrl.setText(tmpStr);
                }
                tmpStr = null;
                tmpStr = c.getString(c.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_TITLE));
                if (tmpStr != null) {
                    etPosName.setText(tmpStr);
                }
                tmpStr = null;
                tmpStr = c.getString(c.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK));
                if (tmpStr != null) {
                    etLkdnUrl.setText(tmpStr);
                }
                //byte[] profilePicBlob = c.getBlob(c.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_PROFILE_PIC));
                //if (profilePicBlob != null) {
                //    imProfilePic.setImageBitmap(BitmapFactory.decodeByteArray(profilePicBlob , 0, profilePicBlob.length));
                //}
                ((GlobalApplication) getApplication()).setIsProfExist(true);
            } else {
                Log.d(LOG_TAG, "No DB found");
                ((GlobalApplication) getApplication()).setIsProfExist(false);
                setDefaultProfVal();
            }
        }
        
        buttonProfileSave= (Button)findViewById(R.id.profile_save);
        buttonProfileSave.setOnClickListener(profileSaveOnClickListener);
        buttonFbSync= (Button)findViewById(R.id.profile_fb_sync);
        buttonFbSync.setOnClickListener(fbSyncOnClickListener);
        Log.d(LOG_TAG, "profileViewInit 3");
    }
    private void setDefaultProfVal(){
    	Session	session = Session.getActiveSession();
	    final TelephonyManager tm = (TelephonyManager)getSystemService(Context.TELEPHONY_SERVICE);
        ((GlobalApplication) getApplication()).setFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_PHONE,tm.getLine1Number());
        etPhone.setText(((GlobalApplication) getApplication()).getFbSyncHFData(ProfileProvider.Profile.COLUMN_NAME_PHONE));
    	
        Log.d(LOG_TAG, "get cell number:" + tm.getLine1Number());
    	FBApiUtil.makeMeRequest(this,session);
    }
    private void setTransHFDatas() {
        Cursor c = getContentResolver().query(
                ProfileProvider.Profile.CONTENT_URI, 
                ProfileProvider.Profile.PROJECTION, 
                ProfileProvider.Profile.COLUMN_NAME_USERID + "=?", 
                new String[] {((GlobalApplication) getApplication()).getHFID()}, 
                null);
        c.moveToFirst();
    	if (c.getCount() != 0) {
        	for(int i=0;i<Constants.HFDataNumElmt;i++){
        		String tmpStr =c.getString(c.getColumnIndexOrThrow(ProfileProvider.Profile.PROJECTION[i+1]));
        		((GlobalApplication)  getApplication()).setTransHFData(ProfileProvider.Profile.PROJECTION[i+1],tmpStr);
        	}
        }
    }
    // listener for click "save"
    Button.OnClickListener profileSaveOnClickListener = 
        new OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getBaseContext(), "click profile save button", Toast.LENGTH_SHORT).show();
                // First part: update local database
                ContentValues values = setContentValues();
               
                if(((GlobalApplication) getApplication()).getIsProfExist()){
                    int update = getContentResolver().update(
                            ProfileProvider.Profile.CONTENT_URI, 
                            values,
                            ProfileProvider.Profile.COLUMN_NAME_USERID + "=?", 
                            new String[] {((GlobalApplication) getApplication()).getHFID()});
                    
                    Log.d(LOG_TAG, "update success:"+update);
                }else{
                    Uri tmpUri = getContentResolver().insert(
                            ProfileProvider.Profile.CONTENT_URI, 
                            values);
                    long myProfID =ContentUris.parseId(tmpUri);
                    Log.d(LOG_TAG, "inserted myProfID:" + String.valueOf(myProfID));
                    ((GlobalApplication) getApplication()).setIsProfExist(true);
                }
                Log.d(LOG_TAG, "ProfEditFinish!!" );
                //saveShChBoxVal();
                setTransHFDatas();
                Intent i = getIntent();
                setResult(RESULT_OK, i);

                finish();

               /*FIXMELATERVER 
                // Second part: update webserver database
                Bundle b = new Bundle();
                // Disable sync backoff and ignore sync preferences. In other words...perform sync NOW!
                b.putBoolean(ContentResolver.SYNC_EXTRAS_MANUAL, true);
                b.putBoolean(ContentResolver.SYNC_EXTRAS_EXPEDITED, true);
                b.putInt(Constants.SYNC_ADAPTER_OP, Constants.SYNC_METHOD_PUT);
                ContentResolver.requestSync(GenericAccountService.GetAccount(), ProfileProvider.CONTENT_AUTHORITY, b);
               */
        }       
    };

    // listener for click "facebook sync"
    Button.OnClickListener fbSyncOnClickListener = 
        new OnClickListener() {
            public void onClick(View v) {
            	Toast.makeText(getBaseContext(), "click facebook sync button", Toast.LENGTH_SHORT).show();
                setDefaultProfVal();
            }       
    };
    
    
    public ContentValues setContentValues() {
	    ContentValues values = new ContentValues();
	    values.put(ProfileProvider.Profile.COLUMN_NAME_USERID, ((GlobalApplication) getApplication()).getHFID());
	    values.put(ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME, etFirstName.getText().toString());
	    values.put(ProfileProvider.Profile.COLUMN_NAME_LASTNAME, etLastName.getText().toString());
	    if (profilePic != null) {
	        values.put(ProfileProvider.Profile.COLUMN_NAME_PROFILE_PIC, profilePic);
	    }
	    values.put(ProfileProvider.Profile.COLUMN_NAME_PHONE, etPhone.getText().toString());
	    values.put(ProfileProvider.Profile.COLUMN_NAME_EMAIL, etEmail.getText().toString());
	    
	    values.put(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION, etOrgName.getText().toString());
	    values.put(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION_LINK, etOrgUrl.getText().toString());
	    values.put(ProfileProvider.Profile.COLUMN_NAME_TITLE, etPosName.getText().toString());
	    values.put(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK, etLkdnUrl.getText().toString());
	    values.put(ProfileProvider.Profile.COLUMN_NAME_FB_ID, MainActivity.HFID);
	    
	    return values;
    }
/*
    ImageButton.OnClickListener profilePicOnClickListener =
        new OnClickListener() {
            @Override
            public void onClick(View v) {
                Log.d(LOG_TAG, "within profile popup window");

                View popUpView = getLayoutInflater().inflate(R.layout.popup_profile_pic_method, null); // inflating popup layout
                mpopup = new PopupWindow(popUpView, LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT, true); //Creation of popup
                mpopup.setAnimationStyle(android.R.style.Animation_Dialog);   
                mpopup.showAtLocation(popUpView, Gravity.CENTER, 0, 0);    // Displaying popup
                
                // button for cancel the popup window
                //ImageButton btnCancel = (ImageButton)popUpView.findViewById(R.id.btnCancel);
                btnCancel.setOnClickListener(new OnClickListener() {                    
                    @Override
                    public void onClick(View v) {
                        mpopup.dismiss(); //dismissing the popup
                    }
                });
                
                // button for pick picture from camera
                ImageButton btnCamera = (ImageButton) popUpView.findViewById(R.id.btnCamera);
               // btnCamera.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        mCamera = new CameraUtil((Activity) getBaseContext());
                        startActivityForResult(
                                mCamera.dispatchTakePictureIntent(), Constants.INTENT_CAMERA_ACTIVITY);
                    }
                });
                
                // button for pick picture form gallery
                ImageButton btnGallery = (ImageButton) popUpView.findViewById(R.id.btnGallery);
                btnGallery.setOnClickListener(new OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        Intent intent = new Intent(Intent.ACTION_GET_CONTENT);
                        intent.setType("image/*");
                        startActivityForResult(intent, Constants.INTENT_GALLERY_ACTIVITY);
                    }
                });
            };        
        };*/
    /*
        @Override
        public void onActivityResult(int requestCode, int resultCode, Intent intent) {
            Log.d(LOG_TAG, "onActivityResult requestCode "+String.valueOf(requestCode));
            if (resultCode == Activity.RESULT_OK) {
                switch(requestCode){
                case Constants.INTENT_CAMERA_ACTIVITY:
                    //TODO: has to handle orientation change crash
                    Toast.makeText(getBaseContext(), "return from fragment call intent", Toast.LENGTH_SHORT).show();
                    mCamera.galleryAddPic();
                    Log.d(LOG_TAG, "profilePic path:"+mCamera.getPath());
                    //profilePic = PictureUtil.setPicture(mCamera.getPath(), imProfilePic);
                    Log.d(LOG_TAG, "profilePic byte size:"+profilePic.length);
                    mpopup.dismiss();
                    break;
                case Constants.INTENT_GALLERY_ACTIVITY:
                    Uri selectedImageURI = intent.getData();
                    Log.d(LOG_TAG, "selectedImageURI = "+selectedImageURI.toString());
                    String mCurrentPhotoPath = PictureUtil.getRealPathFromURI(this, selectedImageURI);
                    Log.d(LOG_TAG, "gallery path:"+mCurrentPhotoPath);
                    //TODO: cannot get valid mCurrentPhotoPath
                    //Comment from Justin: I can get the valid mCurrentPhotoPath with out any edit. I did edit below line to switch mCamera.getPath() to mCurrentPhotoPath 
                    //to make it work but not sure this fixes your issue.

                    //profilePic = PictureUtil.setPicture(mCurrentPhotoPath, imProfilePic);
                    mpopup.dismiss(); //dismissing the popup
                    break;
                }
            }
        }
        */
}
