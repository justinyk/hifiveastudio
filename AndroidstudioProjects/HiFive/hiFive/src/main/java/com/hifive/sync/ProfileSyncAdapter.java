package com.hifive.sync;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Base64;
import android.util.Log;

import com.hifive.Constants;
import com.hifive.MainActivity;
import com.hifive.provider.ProfileProvider;
import com.hifive.util.DjangoJSONParser;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 * <p>This class is instantiated in {@link ProfileSyncService}, which also binds SyncAdapter to the system.
 * SyncAdapter should only be initialized in SyncService, never anywhere else.
 *
 * <p>The system calls onPerformSync() via an RPC call through the IBinder object supplied by
 * SyncService.
 */
public class ProfileSyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String LOG_TAG = "hifive.ProfileSyncAdapter";
    // Global variables
    // Content resolver, for performing database operations.
    ContentResolver mContentResolver;
    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    public ProfileSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
    }
    /**
     * Constructor. Obtains handle to content resolver for later use.
     * This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public ProfileSyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
    }
    
    /**
     * Called by the Android system in response to a request to run the sync adapter. The work
     * required to read data from the network, parse it, and store it in the content provider is
     * done here. Extending AbstractThreadedSyncAdapter ensures that all methods within SyncAdapter
     * run on a background thread. For this reason, blocking I/O and other long-running tasks can be
     * run <em>in situ</em>, and you don't have to set up a separate thread for them.
     .
     *
     * <p>This is where we actually perform any work required to perform a sync.
     * {@link AbstractThreadedSyncAdapter} guarantees that this will be called on a non-UI thread,
     * so it is safe to peform blocking I/O here.
     *
     * <p>The syncResult argument allows you to pass information back to the method that triggered
     * the sync.
     */    
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
            ContentProviderClient provider, SyncResult syncResult) {
        // Get sync adapter op from extras:
        Integer OP = extras.getInt(Constants.SYNC_ADAPTER_OP);
        Log.d(LOG_TAG, "in onPerformSync OP="+OP);
        // Creating JSON Parser object
        DjangoJSONParser mJsonParser;
        JSONObject mJsonObj;
        List<NameValuePair> mParams;
        
        String mUriString;
        Uri mUri;
        Cursor cursor;
        
        switch (OP) {
        default:
            Log.d(LOG_TAG, "OP code error");
            break;
        case Constants.SYNC_METHOD_GET:
            
            Log.d(LOG_TAG, "in djangoGet asyncTask");
            mJsonParser = new DjangoJSONParser();
            mParams = new ArrayList<NameValuePair>();
            String requestID = extras.getString(Constants.REQUEST_PROFILE_ID);
            Log.d(LOG_TAG, "request ID: "+Constants.REQUEST_PROFILE_ID);
            mJsonObj = mJsonParser.makeHttpRequest(Constants.getUrlDownloadProfile(requestID), Constants.HTTP_GET, mParams);
            Log.d(LOG_TAG, "All profiles: "+mJsonObj.toString());
            try {
                // Storing each json item in variable
                String mProfileIdTemp = mJsonObj.getString(Constants.TAG_PROFILE_USERID);
                String mFirstNameTemp = mJsonObj.getString(Constants.TAG_PROFILE_FIRSTNAME);
                String mProfilePicString = mJsonObj.getString(Constants.TAG_PROFILE_PICTURE);
                
                ContentValues mValue = new ContentValues();
                mValue.put(ProfileProvider.Profile.COLUMN_NAME_USERID, mProfileIdTemp);
                mValue.put(ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME, mFirstNameTemp);
                
                if (mProfilePicString != null) {
                    byte[] mProfilePicBlob = Base64.decode(mProfilePicString, Base64.DEFAULT);
                    mValue.put(ProfileProvider.Profile.COLUMN_NAME_PROFILE_PIC, mProfilePicBlob);
                }
                    
                Uri uri = getContext().getContentResolver().insert(ProfileProvider.Profile.CONTENT_URI, mValue);
                Log.d(LOG_TAG, "add new profile: "+uri.toString());                
            } catch (JSONException e) {
                e.printStackTrace();
            }
            break;
        case Constants.SYNC_METHOD_POST:
            // POST profile is only used when new account add.
            // Other activity should be PUT
            Log.d(LOG_TAG, "in djangoPost asyncTask");
            mJsonParser = new DjangoJSONParser();
            mParams = new ArrayList<NameValuePair>();
            
            mUriString = Constants.SELF_PROFILE_CONTENT_URI;
            Log.d(LOG_TAG, "try to post "+mUriString);
            mUri = Uri.parse(mUriString);
            
            cursor = getContext().getContentResolver().query(mUri, ProfileProvider.Profile.PROJECTION, null, null, null);
            
            if (cursor != null) {               
                cursor.moveToFirst();
                String mUserID = cursor.getString(cursor.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_USERID));
                
                mParams.add(new BasicNameValuePair(Constants.TAG_PROFILE_USERID,mUserID));
                Log.d(LOG_TAG, "params perpared");
                
                // sending modified data through http request
                Log.d(LOG_TAG, mParams.toString());
                // Notice that update product url accepts POST method
                mJsonParser.makeHttpRequest(Constants.getUrlUploadProfile(MainActivity.HFID),Constants.HTTP_POST, mParams);
            } else {
            }
            break;
        case Constants.SYNC_METHOD_PUT:
            
            Log.d(LOG_TAG, "in djangoPost asyncTask");
            mJsonParser = new DjangoJSONParser();
            mParams = new ArrayList<NameValuePair>();
            
            mUriString = Constants.SELF_PROFILE_CONTENT_URI;
            Log.d(LOG_TAG, "try to post "+mUriString);
            mUri = Uri.parse(mUriString);
            
            cursor = getContext().getContentResolver().query(mUri, ProfileProvider.Profile.PROJECTION, null, null, null);
            
            if (cursor != null) {               
                cursor.moveToFirst();
                String mFisrtName = cursor.getString(cursor.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME));
                byte[] mProfilePic = cursor.getBlob(cursor.getColumnIndexOrThrow(ProfileProvider.Profile.COLUMN_NAME_PROFILE_PIC));

                if (mProfilePic != null) {
                    // convert blob to string
                    String mProfilePicString = Base64.encodeToString(mProfilePic, Base64.DEFAULT);
                    // remove \n because django or tastypie doesn't like it
                    mProfilePicString = mProfilePicString.replace("\n", "");
                    mParams.add(new BasicNameValuePair(Constants.TAG_PROFILE_PICTURE, mProfilePicString));
                }
                if (mFisrtName != null) {
                    mParams.add(new BasicNameValuePair(Constants.TAG_PROFILE_FIRSTNAME,mFisrtName));
                }

                Log.d(LOG_TAG, "params perpared");
                Log.d(LOG_TAG, mParams.toString());
                // Notice that update product url accepts POST method
                mJsonParser.makeHttpRequest(Constants.getUrlUploadProfile(MainActivity.HFID),Constants.HTTP_PUT, mParams);
            } else {
            }
            break;            
        }

    }
}