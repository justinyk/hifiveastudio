package com.hifive.sync;

import java.util.ArrayList;
import java.util.List;

import org.apache.http.NameValuePair;
import org.apache.http.message.BasicNameValuePair;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.accounts.Account;
import android.content.AbstractThreadedSyncAdapter;
import android.content.ContentProviderClient;
import android.content.ContentResolver;
import android.content.ContentValues;
import android.content.Context;
import android.content.SyncResult;
import android.database.Cursor;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;

import com.hifive.Constants;
import com.hifive.provider.EventProvider;
import com.hifive.util.DjangoJSONParser;

/**
 * Handle the transfer of data between a server and an
 * app, using the Android sync adapter framework.
 * <p>This class is instantiated in {@link EventSyncService}, which also binds SyncAdapter to the system.
 * SyncAdapter should only be initialized in SyncService, never anywhere else.
 *
 * <p>The system calls onPerformSync() via an RPC call through the IBinder object supplied by
 * SyncService.
 */
public class EventSyncAdapter extends AbstractThreadedSyncAdapter {

    private static final String LOG_TAG = "hifive.EventSyncAdapter";
    // Global variables
    // Content resolver, for performing database operations.
    ContentResolver mContentResolver;
    /**
     * Constructor. Obtains handle to content resolver for later use.
     */
    public EventSyncAdapter(Context context, boolean autoInitialize) {
        super(context, autoInitialize);
        mContentResolver = context.getContentResolver();
    }
    /**
     * Constructor. Obtains handle to content resolver for later use.
     * This form of the
     * constructor maintains compatibility with Android 3.0
     * and later platform versions
     */
    public EventSyncAdapter(
            Context context,
            boolean autoInitialize,
            boolean allowParallelSyncs) {
        super(context, autoInitialize, allowParallelSyncs);
        mContentResolver = context.getContentResolver();
    }
    
    /**
     * Called by the Android system in response to a request to run the sync adapter. The work
     * required to read data from the network, parse it, and store it in the content provider is
     * done here. Extending AbstractThreadedSyncAdapter ensures that all methods within SyncAdapter
     * run on a background thread. For this reason, blocking I/O and other long-running tasks can be
     * run <em>in situ</em>, and you don't have to set up a separate thread for them.
     .
     *
     * <p>This is where we actually perform any work required to perform a sync.
     * {@link AbstractThreadedSyncAdapter} guarantees that this will be called on a non-UI thread,
     * so it is safe to peform blocking I/O here.
     *
     * <p>The syncResult argument allows you to pass information back to the method that triggered
     * the sync.
     */    
    @Override
    public void onPerformSync(Account account, Bundle extras, String authority,
            ContentProviderClient provider, SyncResult syncResult) {
        // Get sync adapter op from extras:
        Integer OP = extras.getInt(Constants.SYNC_ADAPTER_OP);
        Log.d(LOG_TAG, "in onPerformSync OP="+OP);
        // Creating JSON Parser object
        DjangoJSONParser mJsonParser;
        JSONObject mJsonObj;
        List<NameValuePair> mParams;
        
        switch (OP) {
        default:
            Log.d(LOG_TAG, "OP code error");
            break;
        case Constants.SYNC_METHOD_GET:
            Log.d(LOG_TAG, "in djangoGet asyncTask");
            mJsonParser = new DjangoJSONParser();
            mParams = new ArrayList<NameValuePair>();
            mJsonObj = mJsonParser.makeHttpRequest(Constants.getUrlDownloadEvent(), Constants.HTTP_GET, mParams);
            Log.d(LOG_TAG, "All events: "+mJsonObj.toString());
            try {
                JSONArray mJsonArray = mJsonObj.getJSONArray("objects");
                Log.d(LOG_TAG, "download objects:"+mJsonArray.length());
                for (int i=0; i < mJsonArray.length(); i++) {
                    JSONObject mJsonObjTemp = mJsonArray.getJSONObject(i);
                    // Storing each json item in variable                   
                    String mId1Temp = mJsonObjTemp.getString(Constants.TAG_EVENT_ID1);
                    String mId2Temp = mJsonObjTemp.getString(Constants.TAG_EVENT_ID2);
                    String mTimeTemp = mJsonObjTemp.getString(Constants.TAG_EVENT_TIME);

                    ContentValues mValue = new ContentValues();
                    mValue.put(EventProvider.Event.COLUMN_NAME_ID1, mId1Temp);
                    mValue.put(EventProvider.Event.COLUMN_NAME_ID2, mId2Temp);
                    mValue.put(EventProvider.Event.COLUMN_NAME_TIME, mTimeTemp);
                        
                    Uri uri = getContext().getContentResolver().insert(EventProvider.Event.CONTENT_URI, mValue);
                    Log.d(LOG_TAG, "add new event: "+uri.toString());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }
            break;
            
        case Constants.SYNC_METHOD_POST:
            Log.d(LOG_TAG, "in djangoPost asyncTask");
            
            mJsonParser = new DjangoJSONParser();
            mParams = new ArrayList<NameValuePair>();
            
            String mUriString = extras.getString("PostEventURI");
            Uri mUri = Uri.parse(mUriString);
            Cursor cursor = getContext().getContentResolver().query(mUri, EventProvider.Event.PROJECTION, null, null, null);
            String mId1;
            String mId2;
            String mTime;
            if (cursor != null) {               
                cursor.moveToFirst();
                mId1 = cursor.getString(cursor.getColumnIndexOrThrow(EventProvider.Event.COLUMN_NAME_ID1));
                mId2 = cursor.getString(cursor.getColumnIndexOrThrow(EventProvider.Event.COLUMN_NAME_ID2));
                mTime = cursor.getString(cursor.getColumnIndexOrThrow(EventProvider.Event.COLUMN_NAME_TIME));
                mParams.add(new BasicNameValuePair(Constants.TAG_EVENT_ID1, mId1));
                mParams.add(new BasicNameValuePair(Constants.TAG_EVENT_ID2, mId2));
                mParams.add(new BasicNameValuePair(Constants.TAG_EVENT_TIME, mTime));
                Log.d(LOG_TAG, "params perpared");

                // sending modified data through http request
                Log.d(LOG_TAG, mParams.toString());
                // Notice that update product url accepts POST method
                mJsonParser.makeHttpRequest(Constants.getUrlUploadEvent(),Constants.HTTP_POST, mParams);
            } else {
            }
            break;
        }

    }
}