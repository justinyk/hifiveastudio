package com.hifive.fragment;

import android.app.Activity;
import android.content.ContentUris;
import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.view.ViewGroup.LayoutParams;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.PopupWindow;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.Session;
import com.facebook.widget.ProfilePictureView;
import com.hifive.Constants;
import com.hifive.MainActivity;
import com.hifive.R;
import com.hifive.activity.HiFiveInfoActivity;
import com.hifive.activity.ProfileEditActivity;
import com.hifive.provider.ProfileProvider;
import com.hifive.util.CameraUtil;
import com.hifive.util.FBApiUtil;
import com.hifive.util.GlobalApplication;
import com.hifive.util.PictureUtil;

public class ProfileFragment extends Fragment {
    private static final String LOG_TAG = "hifive.profilefragment";

    private PopupWindow mpopup;
    private CameraUtil mCamera;
    
    
    // views
    private ImageView imProfilePic;
    private static TextView tvUserName;
    private static TextView tvUserTitle;
    
    

    byte[] profilePic;
    String phone;
    String email;
    private CheckBox cbShLkdnUrl;
    private CheckBox cbShEmail;
    private CheckBox cbShPhone;
    private CheckBox cbShFbId;
    
    private Button buttonProfileEdit;
    private Button buttonOccupationLink;
    private Button buttonLinkedInLink;
    private Button buttonEmailLink;
    private Button buttonPhoneLink;
    private Button buttonFBLink;
    
    
    public static ProfilePictureView profilePictureView;
    
    

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        View rootView = inflater.inflate(R.layout.fragment_profile, container, false);
        
        profileViewInit(rootView);
        
        if(((GlobalApplication) getActivity().getApplication()).getIsProfExist()){
	    	setProfDatas();
	    }
	    else{
	    	Intent i = new Intent(getActivity(), ProfileEditActivity.class);
            startActivity(i);
	    }
	    		

        return rootView;
    }
    @Override
    public void onPause() {
    	Log.d(LOG_TAG, "onPause");
        super.onPause();
        saveShChBoxVal();
    }
    @Override
    public void onDestroy() {
    	Log.d(LOG_TAG, "onDestroy");
    	super.onDestroy();
    	saveShChBoxVal();
        
    }
    private void profileViewInit(View view) {

       
        buttonProfileEdit= (Button)view.findViewById(R.id.profile_edit);
        buttonProfileEdit.setOnClickListener(profileEditOnClickListener);
        buttonOccupationLink= (Button)view.findViewById(R.id.userOccupationLink);
        buttonOccupationLink.setOnClickListener(occupatioinLinkOnClickListener);
        buttonLinkedInLink= (Button)view.findViewById(R.id.userlinkedinlink);
        buttonLinkedInLink.setOnClickListener(linkedInLinkOnClickListener);
        buttonPhoneLink= (Button)view.findViewById(R.id.userphonelink);
        buttonPhoneLink.setOnClickListener(phoneLinkOnClickListener);
        buttonEmailLink= (Button)view.findViewById(R.id.useremaillink);
        buttonEmailLink.setOnClickListener(emailLinkOnClickListener);
        buttonFBLink= (Button)view.findViewById(R.id.userfacebooklink);
        buttonFBLink.setOnClickListener(fbLinkOnClickListener);
        
        
        cbShLkdnUrl = (CheckBox) view.findViewById(R.id.checkbox_lkdn_url);
        cbShEmail = (CheckBox) view.findViewById(R.id.checkbox_email);
        cbShPhone = (CheckBox) view.findViewById(R.id.checkbox_phone);
        cbShFbId = (CheckBox) view.findViewById(R.id.checkbox_fbid);
        setChBoxListener();
        
        
        setShChBoxDefault();
        
        tvUserName = (TextView) view.findViewById(R.id.userName);
        tvUserTitle = (TextView) view.findViewById(R.id.userTitle);
        
        profilePictureView = (ProfilePictureView) view.findViewById(R.id.selection_profile_pic3);
        profilePictureView.setCropped(true);
        profilePictureView.setVisibility(View.VISIBLE);
        profilePictureView.setProfileId(MainActivity.HFID);
    }
    private void setChBoxListener() {
    	cbShLkdnUrl.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

    	       @Override
    	       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
    	       	((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK,cbShLkdnUrl.isChecked());
    	    	
    	       }
    	   }
    	); 
    	cbShEmail.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

 	       @Override
 	       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
 	      	((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_EMAIL,cbShEmail.isChecked());
 	    	

 	       }
 	   }
 	); 
 	
    	cbShPhone.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

 	       @Override
 	       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
 	      	((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_PHONE,cbShPhone.isChecked());
 	    	
 	       }
 	   }
 	); 
 	
    	cbShFbId.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {

 	       @Override
 	       public void onCheckedChanged(CompoundButton buttonView,boolean isChecked) {
 	      	((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_FB_ID,cbShFbId.isChecked());

 	       }
 	   }
 	); 
 	
    	
    }

    private void setProfDatas() {
    	String sTxt=null;
        sTxt=((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION);
	    if(sTxt!=null)	buttonOccupationLink.setText(sTxt);
	    sTxt=((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_TITLE);
	    if(sTxt!=null)	tvUserTitle.setText(sTxt);
	    sTxt=((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME);
	    sTxt=sTxt+" "+((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_LASTNAME);
	    if(sTxt!=null)	tvUserName.setText(sTxt);

    }
    private void saveShChBoxVal() {
    	//((GlobalApplication) getActivity().getApplication()).setIsShPref(Constants.SHPREFS[0],cbShLkdnUrl.isChecked());
    	//((GlobalApplication) getActivity().getApplication()).setIsShPref(Constants.SHPREFS[1],cbShEmail.isChecked());
    	//((GlobalApplication) getActivity().getApplication()).setIsShPref(Constants.SHPREFS[2],cbShPhone.isChecked());
    	//((GlobalApplication) getActivity().getApplication()).setIsShPref(Constants.SHPREFS[3],cbShFbId.isChecked());
    	
    	((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK,cbShLkdnUrl.isChecked());
    	((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_EMAIL,cbShEmail.isChecked());
    	((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_PHONE,cbShPhone.isChecked());
    	((GlobalApplication) getActivity().getApplication()).setIsShPref(ProfileProvider.Profile.COLUMN_NAME_FB_ID,cbShFbId.isChecked());
    	
    }
    private void setShChBoxDefault() {
    	cbShLkdnUrl.setChecked(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK));
    	cbShEmail.setChecked(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_EMAIL));
    	cbShPhone.setChecked(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_PHONE));
    	cbShFbId.setChecked(((GlobalApplication) getActivity().getApplication()).getIsShPref(ProfileProvider.Profile.COLUMN_NAME_FB_ID));
    }
    // listener for click "edit"
    Button.OnClickListener profileEditOnClickListener = 
        new OnClickListener() {
            public void onClick(View v) {
                Toast.makeText(getActivity(), "click profile edit button", Toast.LENGTH_SHORT).show();
	        	Intent i = new Intent(getActivity(), ProfileEditActivity.class);
	            startActivity(i);
             
        }       
    };
    // listener for click "occupation link"
    Button.OnClickListener occupatioinLinkOnClickListener = 
        new OnClickListener() {
            public void onClick(View v) {
            	String sTxt=((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_ORGANIZATION_LINK);
            	openURL(sTxt);
            	Toast.makeText(getActivity(), "click occupation link :"+sTxt, Toast.LENGTH_SHORT).show();

             
        }       
    };
    // listener for click "linked in  link"
    Button.OnClickListener linkedInLinkOnClickListener = 
        new OnClickListener() {
            public void onClick(View v) {
            	String sTxt=((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_LNKDIN_LINK);
            	openURL(sTxt);
            	Toast.makeText(getActivity(), "click  link :"+sTxt, Toast.LENGTH_SHORT).show();

             
        }       
    };
    // listener for click "facbook profile in  link"
    Button.OnClickListener fbLinkOnClickListener = 
        new OnClickListener() {
            public void onClick(View v) {
            	FBApiUtil.openFBProfile(getActivity(),MainActivity.HFID);
        }       
    };    

    
    // listener for click "linked in  link"
    Button.OnClickListener emailLinkOnClickListener = 
        new OnClickListener() {
            public void onClick(View v) {
            	Intent mailer = new Intent(Intent.ACTION_SEND);
            	String sTxt=((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_EMAIL);
            	mailer.setType("text/plain");
            	mailer.putExtra(Intent.EXTRA_EMAIL, new String[]{sTxt});
            	mailer.putExtra(Intent.EXTRA_SUBJECT, "");
            	mailer.putExtra(Intent.EXTRA_TEXT, "");
            	startActivity(Intent.createChooser(mailer, "Send email..."));
            	Toast.makeText(getActivity(), "send email:"+sTxt, Toast.LENGTH_SHORT).show();

             
        }       
    }; 
    // listener for click "facbook profile in  link"
    Button.OnClickListener phoneLinkOnClickListener = 
        new OnClickListener() {
            public void onClick(View v) {
            	String sTxt=((GlobalApplication) getActivity().getApplication()).getTransHFData(ProfileProvider.Profile.COLUMN_NAME_PHONE);
            	Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:"+Uri.encode(sTxt.trim())));
                callIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                startActivity(callIntent); 
        }       
    }; 

    private void openURL(String URLAddr) {
		if (!URLAddr.startsWith("http://") && !URLAddr.startsWith("https://"))
			URLAddr = "http://" + URLAddr;
		Intent browserIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(URLAddr));
		startActivity(browserIntent);
	}
    
    @Override
        public void onActivityResult(int requestCode, int resultCode, Intent intent) {
            Log.d(LOG_TAG, "onActivityResult requestCode "+String.valueOf(requestCode));
            if (resultCode == Activity.RESULT_OK) {
                switch(requestCode){
                case Constants.INTENT_CAMERA_ACTIVITY:
                    //TODO: has to handle orientation change crash
                    Toast.makeText(getActivity(), "return from fragment call intent", Toast.LENGTH_SHORT).show();
                    mCamera.galleryAddPic();
                    Log.d(LOG_TAG, "profilePic path:"+mCamera.getPath());
                    profilePic = PictureUtil.setPicture(mCamera.getPath(), imProfilePic);
                    Log.d(LOG_TAG, "profilePic byte size:"+profilePic.length);
                    mpopup.dismiss();
                    break;
                case Constants.INTENT_GALLERY_ACTIVITY:
                    Uri selectedImageURI = intent.getData();
                    Log.d(LOG_TAG, "selectedImageURI = "+selectedImageURI.toString());
                    String mCurrentPhotoPath = PictureUtil.getRealPathFromURI(getActivity(), selectedImageURI);
                    Log.d(LOG_TAG, "gallery path:"+mCurrentPhotoPath);
                    //TODO: cannot get valid mCurrentPhotoPath
                    //Comment from Justin: I can get the valid mCurrentPhotoPath with out any edit. I did edit below line to switch mCamera.getPath() to mCurrentPhotoPath 
                    //to make it work but not sure this fixes your issue.

                    profilePic = PictureUtil.setPicture(mCurrentPhotoPath, imProfilePic);
                    mpopup.dismiss(); //dismissing the popup
                    break;
                }
            }
        } 
}
