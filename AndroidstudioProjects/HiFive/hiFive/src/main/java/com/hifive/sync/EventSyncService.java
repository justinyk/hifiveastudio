package com.hifive.sync;

import android.app.Service;
import android.content.Intent;
import android.os.IBinder;
import android.util.Log;

/** Service to handle sync requests.
*
* <p>This service is invoked in response to Intents with action android.content.SyncAdapter, and
* returns a Binder connection to SyncAdapter.
*
* <p>For performance, only one sync adapter will be initialized within this application's context.
*
* <p>Note: The SyncService itself is not notified when a new sync occurs. It's role is to
* manage the lifecycle of our {@link EventSyncAdapter} and provide a handle to said SyncAdapter to the
* OS on request.
*/

public class EventSyncService extends Service {
    private static final String LOG_TAG = "hifive.EventSyncService";

    
    // Storage for an instance of the sync adapter
    private static EventSyncAdapter sEventSyncAdapter = null;
    // Object to use as a thread-safe lock
    private static final Object sEventSyncAdapterLock = new Object();
    /*
     * Instantiate the sync adapter object.
     */
    @Override
    public void onCreate() {
        /*
         * Create the sync adapter as a singleton.
         * Set the sync adapter as syncable
         * Disallow parallel syncs
         */
        super.onCreate();
        Log.d(LOG_TAG, "Service created");
        synchronized (sEventSyncAdapterLock) {
            if (sEventSyncAdapter == null) {
                sEventSyncAdapter = new EventSyncAdapter(getApplicationContext(), true);
            }
        }
    }
    @Override
    /**
     * Logging-only destructor.
     */
    public void onDestroy() {
        super.onDestroy();
        Log.d(LOG_TAG, "Service destroyed");
    }
    /**
     * Return Binder handle for IPC communication with {@link EventSyncAdapter}.
     *
     * <p>New sync requests will be sent directly to the EventSyncAdapter using this channel.
     *
     * @param intent Calling intent
     * @return Binder handle for {@link EventSyncAdapter}
     */
    @Override
    public IBinder onBind(Intent intent) {
        /*
         * Get the object that allows external processes
         * to call onPerformSync(). The object is created
         * in the base class code when the SyncAdapter
         * constructors call super()
         */
        return sEventSyncAdapter.getSyncAdapterBinder();
    }
}