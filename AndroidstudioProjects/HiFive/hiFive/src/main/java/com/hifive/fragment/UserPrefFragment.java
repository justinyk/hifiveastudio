package com.hifive.fragment;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;
import android.widget.CompoundButton.OnCheckedChangeListener;
import android.widget.RadioGroup;
import android.widget.Switch;

import com.hifive.Constants;
import com.hifive.R;
import com.hifive.util.GlobalApplication;

public class UserPrefFragment extends Fragment{
	private static final String LOG_TAG = "hifive.userpreffragment";
	private Switch fbShSwitch;
	private Switch upPicSwitch;
	private RadioGroup imgSrcRdGrp; 
    //[UI] fragment pages symbolic

    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
        Bundle savedInstanceState) {
    	View rootView = inflater.inflate(R.layout.fragment_userpref, null);
        userPrefViewInit(rootView);
        return rootView;
    };
    private void userPrefViewInit(View view) {
    	Log.d(LOG_TAG, "hiFiveViewInit ");
    	fbShSwitch = (Switch) view.findViewById(R.id.fbShareSw);
    	fbShSwitch.setChecked(((GlobalApplication) getActivity().getApplication()).getFbShareStatus());//set the switch to ON 
    	fbShSwitch.setOnCheckedChangeListener(fbShSwitchListener);
    	upPicSwitch = (Switch) view.findViewById(R.id.uploadPicSw);
    	upPicSwitch.setChecked(((GlobalApplication) getActivity().getApplication()).getUploadPicStatus());//set the switch to Off 
    	upPicSwitch.setOnCheckedChangeListener(upPicSwitchListener);
    	
    	imgSrcRdGrp=(RadioGroup)view.findViewById(R.id.imgSrcRdGrp);
    	if(((GlobalApplication) getActivity().getApplication()).getImgSrc()==0){
    		imgSrcRdGrp.check(R.id.imgSrcCamera);
    	}else{
    		imgSrcRdGrp.check(R.id.imgSrcHifive);
    	}
    	imgSrcRdGrp.setOnCheckedChangeListener(imgSrcRdGrpListener);
		if(((GlobalApplication) getActivity().getApplication()).getUploadPicStatus()){
			imgSrcRdGrp.setVisibility(View.VISIBLE);
		}else{
			imgSrcRdGrp.setVisibility(View.GONE);
		}
    	
    	
    }
    /*lister for facebook share status name button */
    CompoundButton.OnCheckedChangeListener fbShSwitchListener = 
            new OnCheckedChangeListener() {
    	@Override
		public void onCheckedChanged(CompoundButton buttonView,
				boolean isChecked) {
			// TODO Auto-generated method stub
    		((GlobalApplication) getActivity().getApplication()).setFbShareStatus(isChecked);
		}   
        };  
       
   /*lister for upload picture status name button */
   CompoundButton.OnCheckedChangeListener upPicSwitchListener = 
            new OnCheckedChangeListener() {
        @Override
        public void onCheckedChanged(CompoundButton buttonView,
    				boolean isChecked) {
    			// TODO Auto-generated method stub
        		if(isChecked){
        			imgSrcRdGrp.setVisibility(View.VISIBLE);
        		}else{
        			imgSrcRdGrp.setVisibility(View.GONE);
        		}
        		((GlobalApplication) getActivity().getApplication()).setUploadPicStatus(isChecked);
    	}   
        };  
   /*lister for upload picture status name button */
        RadioGroup.OnCheckedChangeListener imgSrcRdGrpListener = 
            new RadioGroup.OnCheckedChangeListener() {
            public void onCheckedChanged(RadioGroup group, int checkedId)
            {
                int selId;
            	switch (checkedId) {
                case R.id.imgSrcCamera:
                	selId=Constants.IMGFROMCAM;
                    break;
                case R.id.imgSrcHifive:
                	selId=Constants.IMGFROMHIFIVE;
                    break;
                default:
                	selId=Constants.IMGFROMCAM;
                    break;
                }
            	((GlobalApplication) getActivity().getApplication()).setImgSrc(selId);
            }
        };  
                                 
}
