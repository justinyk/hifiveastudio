package com.hifive.util;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.UnsupportedEncodingException;
import java.util.List;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.NameValuePair;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpPut;
import org.apache.http.client.utils.URLEncodedUtils;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.json.JSONException;
import org.json.JSONObject;

import android.util.Log;

import com.hifive.Constants;

public class DjangoJSONParser {

	private static final String LOG_TAG = "hifive.JSONParser";

	static InputStream is = null;
	static JSONObject jObj = null;
	static String json = "";

	// constructor
	public DjangoJSONParser() {

	}

	// function get json from url
	// by making HTTP POST or GET mehtod
	public JSONObject makeHttpRequest(String url, Integer method,
			List<NameValuePair> params) {
	    DefaultHttpClient httpClient;
	    HttpResponse httpResponse;
	    HttpEntity httpEntity;
	    String entityJson;
	    StringEntity entity;
	    
		// Making HTTP request
		try {
			switch (method) {
			// check for request method
			default:
			    Log.d(LOG_TAG, "wrong HTTP request code");
			    break;
			case Constants.HTTP_POST:
				// request method is POST
				// defaultHttpClient
				httpClient = new DefaultHttpClient();
				HttpPost httpPost = new HttpPost(url);

				entityJson = this.getQueryJSON(params);
				Log.d(LOG_TAG, "entityJson:"+entityJson);
				entity = new StringEntity(entityJson);
				Log.d(LOG_TAG, "entity: "+entity.toString());
				httpPost.setEntity(entity);
				httpPost.setHeader("content-type", "application/json");
				httpResponse = httpClient.execute(httpPost);
				Log.d(LOG_TAG, "httpResponse");
				return jObj;
				
            case Constants.HTTP_PUT:
                // request method is PUT
                // defaultHttpClient
                httpClient = new DefaultHttpClient();
                HttpPut httpPut = new HttpPut(url);

                entityJson = this.getQueryJSON(params);
                Log.d(LOG_TAG, "entityJson:"+entityJson);
                entity = new StringEntity(entityJson);
                Log.d(LOG_TAG, "entity: "+entity.toString());
                httpPut.setEntity(entity);
                httpPut.setHeader("content-type", "application/json");
                httpResponse = httpClient.execute(httpPut);
                Log.d(LOG_TAG, "httpResponse");
                return jObj;
                
            case Constants.HTTP_GET:
				// request method is GET
				httpClient = new DefaultHttpClient();
				String paramString = URLEncodedUtils.format(params, "utf-8");
				url += "?" + paramString;
				HttpGet httpGet = new HttpGet(url);
				
				Log.d(LOG_TAG, "URLs "+ url);
				Log.d(LOG_TAG, "paramString "+ paramString);
				httpResponse = httpClient.execute(httpGet);
				Log.d(LOG_TAG, "httpResponse"+ paramString);
				httpEntity = httpResponse.getEntity();
				is = httpEntity.getContent();
				break;
			}
			

		} catch (UnsupportedEncodingException e) {
			e.printStackTrace();
		} catch (ClientProtocolException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}

		try {
			BufferedReader reader = new BufferedReader(new InputStreamReader(
					is, "iso-8859-1"), 8);
			StringBuilder sb = new StringBuilder();
			String line = null;
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
			is.close();
			Log.d(LOG_TAG, "sb.toString:"+sb.toString());
			json = sb.toString();
		} catch (Exception e) {
			Log.e(LOG_TAG, "Buffer Error: converting result " + e.toString());
		}

		// try parse the string to a JSON object
		try {
			jObj = new JSONObject(json);
		} catch (JSONException e) {
			Log.e(LOG_TAG, "JSON Parser: Error parsing data " + e.toString());
		}

		// return JSON String
		return jObj;

	}
	
    //Utility function to encode the JSON to string
    private String getQueryJSON(List<NameValuePair> params) throws UnsupportedEncodingException {
        StringBuilder result = new StringBuilder();
	    boolean first = true;
	    for (NameValuePair pair : params)
	    {
	        if (first){
	            first = false;
	            result.append("{\"");
	        } else {
	            result.append("\",\"");
	        }
	        result.append(pair.getName());
	        result.append("\":\"");
	        result.append(pair.getValue());
	    }
	    result.append("\"}");
	    return result.toString();	 
	}
}
