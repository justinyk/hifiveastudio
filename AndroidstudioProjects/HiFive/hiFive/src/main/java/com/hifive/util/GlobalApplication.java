/**
 * Copyright 2010-present Facebook.
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *    http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */

package com.hifive.util;



import java.util.HashMap;
import java.util.List;
import android.app.Application;
import android.location.Location;
import android.util.Log;

import com.facebook.model.GraphPlace;
import com.facebook.model.GraphUser;
import com.hifive.Constants;

/**
 * Use a custom Application class to pass state data between Activities.
 */
public class GlobalApplication extends Application {


    private List<GraphUser> selectedUsers;
    private GraphPlace selectedPlace;
    private boolean pendingHifive=false;
    private boolean isFriend=false;
    private boolean isFbShSt=true;
    private boolean isUpPicSt=false;
    private String recvHFID=null;
    private String HFID=null;
    private String recvName=null;
    private String userName=null;
    private String HFLocationString=null;
    private int fragPosition=0;
    private int imgSrc=0;
    
    private boolean isProfEx=false;
    HashMap<String , Boolean> shPrefmap = new HashMap<String , Boolean>();
    HashMap<String , String> recvHFDataMap = new HashMap<String , String>();
    HashMap<String , String> transHFDataMap = new HashMap<String , String>();
    HashMap<String , String> fbSyncDataMap = new HashMap<String , String>();
    
    
    public List<GraphUser> getSelectedUsers() {
        return selectedUsers;
    }

    public void setSelectedUsers(List<GraphUser> users) {
        selectedUsers = users;
    }

    public GraphPlace getSelectedPlace() {
        return selectedPlace;
    }

    public void setSelectedPlace(GraphPlace place) {
        this.selectedPlace = place;
    }
	private Location HFLocation=null;

    public Location getLocation() {
    	return HFLocation;
    }
    public void setLocation(Location location) {
    	//Log.d("GlobalApplication", "setLocation");
        HFLocation = location;
    }
    public void setLocString(String locString) {
    	HFLocationString = locString;
    	//Log.d("GlobalApplication", "setLocString"+HFLocationString);
    }
    public String getLocString() {
    	//Log.d("GlobalApplication", "getLocString"+HFLocationString);
    	return HFLocationString;
    }
    public void setLatitude(double latitude) {
    	//Log.d("GlobalApplication", "setLatitude");
        HFLocation.setLatitude(latitude) ;
    }
    public void setPendingHifive(boolean hifive) {
    	pendingHifive=hifive;
    	//Log.d("GlobalApplication", "setPendingHifive:"+String.valueOf(pendingHifive));
    }
    public boolean getPendingHifive() {
    	//Log.d("GlobalApplication", "getPendingHifive:"+String.valueOf(pendingHifive));
    	return pendingHifive;
    }   
    public void setRecvHFID(String recvID) {
    	recvHFID=recvID;
    	//Log.d("GlobalApplication", "set recvHFID:"+recvHFID);
    }
    public String getRecvHFID() {
    	//Log.d("GlobalApplication", "get recvHFID:"+recvHFID);
    	return recvHFID;
    }   
    public void setHFID(String ID) {
    	HFID=ID;
    	//Log.d("GlobalApplication", "set HFID:"+HFID);
    }
    public String getHFID() {
    	//Log.d("GlobalApplication", "get HFID:"+HFID);
    	return HFID;
    }  
    public void setUserName(String Name) {
    	userName=Name;
    	//Log.d("GlobalApplication", "set userName:"+userName);
    }
    public String getUserName() {
    	//Log.d("GlobalApplication", "get userName:"+userName);
    	return userName;
    }  
    public void setRecvName(String Name) {
    	recvName=Name;
    	//Log.d("GlobalApplication", "set recvName:"+recvName);
    }
    public String getRecvName() {
    	//Log.d("GlobalApplication", "get recvName:"+recvName);
    	return recvName;
    }  
    public void setIsFriend(boolean is) {
    	isFriend=is;
    	//Log.d("GlobalApplication", "set isFriend:"+String.valueOf(isFriend));
    }
    public boolean getIsFriend() {
    	//Log.d("GlobalApplication", "get isFriend:"+String.valueOf(isFriend));
    	return isFriend;
    }  
    public void setFragmentPosition(int position) {
    	fragPosition=position;
    	//Log.d("GlobalApplication", "set isFriend:"+String.valueOf(fragPosition));
    }
    public int getFragmentPosition() {
    	//Log.d("GlobalApplication", "get isFriend:"+String.valueOf(fragPosition));
    	return fragPosition;
    }
    public void setFbShareStatus(boolean st) {
    	isFbShSt=st;
    	//Log.d("GlobalApplication", "set FbShareStatus:"+String.valueOf(isFbShSt));
    }
    public boolean getFbShareStatus() {
    	//Log.d("GlobalApplication", "get FbShareStatus:"+String.valueOf(isFbShSt));
    	return isFbShSt;
    } 
    public void setUploadPicStatus(boolean st) {
    	isUpPicSt=st;
    	//Log.d("GlobalApplication", "set UploadPicStatus:"+String.valueOf(isUpPicSt));
    }
    public boolean getUploadPicStatus() {
    	//Log.d("GlobalApplication", "get UploadPicStatus:"+String.valueOf(isUpPicSt));
    	return isUpPicSt;
    }   
    public void setImgSrc(int st) {
    	imgSrc=st;
    	//Log.d("GlobalApplication", "set ImgSrc:"+String.valueOf(imgSrc));
    }
    public int getImgSrc() {
    	//Log.d("GlobalApplication", "get ImgSrc:"+String.valueOf(imgSrc));
    	return imgSrc;
    }
    public void setIsProfExist(boolean st) {
    	isProfEx=st;
    	//Log.d("GlobalApplication", "set isProfEx:"+String.valueOf(isProfEx));
    }
    public boolean getIsProfExist() {
    	//Log.d("GlobalApplication", "get isProfEx:"+String.valueOf(isProfEx));
    	return isProfEx;
    }  
    public void setIsShPref(String key,boolean st) {
    	shPrefmap.put(key, st);
    	//Log.d("GlobalApplication", "set "+key+":"+String.valueOf(shPrefmap.get(key)));
    }
    public boolean getIsShPref(String key) {
    	//Log.d("GlobalApplication", "get isShProfPic:"+String.valueOf(shPrefmap.get(key)));
    	if (!shPrefmap.containsKey(key)) {
    		shPrefmap.put(key, false);
    	}
    	return shPrefmap.get(key);
    } 
    public void setRecvHFData(String key,String st) {
    	recvHFDataMap.put(key, st);
    	//Log.d("GlobalApplication", "set RecvHFData "+key+":"+String.valueOf(recvHFDataMap.get(key)));
    }
    public String getRecvHFData(String key) {
    	//Log.d("GlobalApplication", "get RecvHFData"+String.valueOf(recvHFDataMap.get(key)));
    	if (!recvHFDataMap.containsKey(key)) {
    		recvHFDataMap.put(key, null);
    	}
    	return recvHFDataMap.get(key);
    } 
    public void setTransHFData(String key,String st) {
    	transHFDataMap.put(key, st);
    	//Log.d("GlobalApplication", "set transHFDataMap "+key+":"+String.valueOf(transHFDataMap.get(key)));
    }
    public String getTransHFData(String key) {
    	//Log.d("GlobalApplication", "get transHFDataMap:"+String.valueOf(transHFDataMap.get(key)));
    	if (!transHFDataMap.containsKey(key)) {
    		transHFDataMap.put(key, null);
    	}
    	return transHFDataMap.get(key);
    } 
    public void setFbSyncHFData(String key,String st) {
    	fbSyncDataMap.put(key, st);
    	//Log.d("GlobalApplication", "set fbSyncDataMap "+key+":"+String.valueOf(fbSyncDataMap.get(key)));
    }
    public String getFbSyncHFData(String key) {
    	//Log.d("GlobalApplication", "get fbSyncDataMap:"+key);
    	if (!fbSyncDataMap.containsKey(key)) {
    		fbSyncDataMap.put(key, null);
    		//Log.d("GlobalApplication", "key not here:");
    	}else{
    		//Log.d("GlobalApplication", "key here:");
    	}
    	//Log.d("GlobalApplication", "return "+key+":"+String.valueOf(fbSyncDataMap.get(key)));
    	return fbSyncDataMap.get(key);
    } 


}