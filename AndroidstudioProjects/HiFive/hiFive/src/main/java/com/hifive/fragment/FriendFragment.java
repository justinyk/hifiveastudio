package com.hifive.fragment;

import android.database.Cursor;
import android.os.Bundle;
import android.support.v4.app.ListFragment;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.hifive.R;
import com.hifive.provider.ProfileProvider;

public class FriendFragment extends ListFragment implements
    LoaderManager.LoaderCallbacks<Cursor> {
    private static final String LOG_TAG = "hifive.friendfragment";

    // Identifies a particular Loader being used in this component
    private static final int URL_LOADER = 0;
    private static final String[] PROJECTION = ProfileProvider.Profile.PROJECTION;
    
    // The adapter that binds our data to the ListView
    private SimpleCursorAdapter mAdapter;   
    

    @Override  
    public View onCreateView(LayoutInflater inflater, ViewGroup container,  
        Bundle savedInstanceState) {
        /*
         * Initializes the CursorLoader. The URL_LOADER value is eventually passed
         * to onCreateLoader().
         */
        getLoaderManager().initLoader(URL_LOADER, null, this);

        String[] from = new String[] {
                ProfileProvider.Profile.COLUMN_NAME_USERID,
                ProfileProvider.Profile.COLUMN_NAME_FIRSTNAME
                };
        int[] to = new int[] {
                R.id.id, 
                R.id.first
                };

        // Now create an array adapter and set it to display using our row
        mAdapter =
            new SimpleCursorAdapter(getActivity(), 
                    R.layout.adapter_friend, 
                    null, 
                    from, 
                    to, 
                    0
                    );
        setListAdapter(mAdapter);
        
        Log.d(LOG_TAG, "mAdapter length"+String.valueOf(mAdapter.getCount()));

        View view = inflater.inflate(R.layout.fragment_friend, null);       
        return view;
    };

    @Override
    public Loader<Cursor> onCreateLoader(int loaderID, Bundle arg1) {
        /*
         * Takes action based on the ID of the Loader that's being created
         */
        Log.d(LOG_TAG, "onCreateLoader");
        switch (loaderID) {
            case URL_LOADER:
                // Returns a new CursorLoader
                return new CursorLoader(
                            getActivity(),   // Parent activity context
                            ProfileProvider.Profile.CONTENT_URI,        // Table to query
                            PROJECTION,      // Projection to return
                            null,            // No selection clause
                            null,            // No selection arguments
                            null             // Default sort order
            );
            default:
                // An invalid id was passed in
                return null;
        }
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor cursor) {
        mAdapter.swapCursor(cursor);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mAdapter.swapCursor(null);
    }

}
