-- Merging decision tree log ---
manifest
ADDED from AndroidManifest.xml:2:1
	xmlns:android
		ADDED from AndroidManifest.xml:2:11
	package
		ADDED from AndroidManifest.xml:3:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:versionName
		ADDED from AndroidManifest.xml:5:5
	android:versionCode
		ADDED from AndroidManifest.xml:4:5
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
uses-permission#android.permission.NFC
ADDED from AndroidManifest.xml:7:5
	android:name
		ADDED from AndroidManifest.xml:7:22
uses-permission#android.permission.INTERNET
ADDED from AndroidManifest.xml:8:5
	android:name
		ADDED from AndroidManifest.xml:8:22
uses-permission#android.permission.ACCESS_NETWORK_STATE
ADDED from AndroidManifest.xml:9:5
	android:name
		ADDED from AndroidManifest.xml:9:22
uses-permission#android.permission.READ_PHONE_STATE
ADDED from AndroidManifest.xml:10:5
	android:name
		ADDED from AndroidManifest.xml:10:22
uses-permission#android.permission.WRITE_EXTERNAL_STORAGE
ADDED from AndroidManifest.xml:11:5
	android:name
		ADDED from AndroidManifest.xml:11:22
uses-permission#android.permission.ACCESS_COARSE_LOCATION
ADDED from AndroidManifest.xml:12:5
	android:name
		ADDED from AndroidManifest.xml:12:22
uses-permission#android.permission.ACCESS_FINE_LOCATION
ADDED from AndroidManifest.xml:13:5
	android:name
		ADDED from AndroidManifest.xml:13:22
uses-permission#android.permission.READ_SYNC_SETTINGS
ADDED from AndroidManifest.xml:14:2
	android:name
		ADDED from AndroidManifest.xml:14:19
uses-permission#android.permission.WRITE_SYNC_SETTINGS
ADDED from AndroidManifest.xml:15:5
	android:name
		ADDED from AndroidManifest.xml:15:22
uses-permission#android.permission.AUTHENTICATE_ACCOUNTS
ADDED from AndroidManifest.xml:16:5
	android:name
		ADDED from AndroidManifest.xml:16:22
uses-permission#com.google.android.providers.gsf.permission.READ_GSERVICES
ADDED from AndroidManifest.xml:17:5
	android:name
		ADDED from AndroidManifest.xml:17:22
uses-feature#android.hardware.nfc
ADDED from AndroidManifest.xml:20:5
	android:name
		ADDED from AndroidManifest.xml:20:19
uses-sdk
ADDED from AndroidManifest.xml:22:5
MERGED from HiFive:facebookSDK:unspecified:20:5
	android:targetSdkVersion
		ADDED from AndroidManifest.xml:24:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
	android:minSdkVersion
		ADDED from AndroidManifest.xml:23:9
		INJECTED from AndroidManifest.xml:0:0
		INJECTED from AndroidManifest.xml:0:0
application
ADDED from AndroidManifest.xml:26:5
MERGED from HiFive:facebookSDK:unspecified:24:5
	android:label
		ADDED from AndroidManifest.xml:29:9
	android:allowBackup
		ADDED from AndroidManifest.xml:27:9
	android:icon
		ADDED from AndroidManifest.xml:28:9
	android:theme
		ADDED from AndroidManifest.xml:30:9
	android:name
		ADDED from AndroidManifest.xml:31:9
activity#com.hifive.MainActivity
ADDED from AndroidManifest.xml:32:9
	android:screenOrientation
		ADDED from AndroidManifest.xml:34:13
	android:label
		ADDED from AndroidManifest.xml:36:13
	android:configChanges
		ADDED from AndroidManifest.xml:35:13
	android:name
		ADDED from AndroidManifest.xml:33:13
intent-filter#android.intent.action.MAIN+android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:37:13
action#android.intent.action.MAIN
ADDED from AndroidManifest.xml:38:17
	android:name
		ADDED from AndroidManifest.xml:38:25
category#android.intent.category.LAUNCHER
ADDED from AndroidManifest.xml:39:17
	android:name
		ADDED from AndroidManifest.xml:39:27
intent-filter#android.intent.category.DEFAULT+android.nfc.action.NDEF_DISCOVERED
ADDED from AndroidManifest.xml:41:13
action#android.nfc.action.NDEF_DISCOVERED
ADDED from AndroidManifest.xml:42:17
	android:name
		ADDED from AndroidManifest.xml:42:25
category#android.intent.category.DEFAULT
ADDED from AndroidManifest.xml:43:17
	android:name
		ADDED from AndroidManifest.xml:43:27
data
ADDED from AndroidManifest.xml:44:17
	android:mimeType
		ADDED from AndroidManifest.xml:44:23
intent-filter#android.intent.action.SEARCH
ADDED from AndroidManifest.xml:46:13
action#android.intent.action.SEARCH
ADDED from AndroidManifest.xml:47:13
	android:name
		ADDED from AndroidManifest.xml:47:21
meta-data#android.app.searchable
ADDED from AndroidManifest.xml:49:10
	android:resource
		ADDED from AndroidManifest.xml:50:20
	android:name
		ADDED from AndroidManifest.xml:49:21
activity#com.hifive.activity.ProfileEditActivity
ADDED from AndroidManifest.xml:53:9
	android:label
		ADDED from AndroidManifest.xml:56:13
	android:name
		ADDED from AndroidManifest.xml:54:13
	android:launchMode
		ADDED from AndroidManifest.xml:55:13
activity#com.hifive.activity.HiFiveInfoActivity
ADDED from AndroidManifest.xml:58:9
	android:label
		ADDED from AndroidManifest.xml:60:13
	android:name
		ADDED from AndroidManifest.xml:59:13
provider#com.hifive.provider.EventProvider
ADDED from AndroidManifest.xml:69:9
	android:syncable
		ADDED from AndroidManifest.xml:73:17
	android:exported
		ADDED from AndroidManifest.xml:72:17
	android:authorities
		ADDED from AndroidManifest.xml:71:17
	android:name
		ADDED from AndroidManifest.xml:70:17
provider#com.hifive.provider.ProfileProvider
ADDED from AndroidManifest.xml:74:9
	android:syncable
		ADDED from AndroidManifest.xml:78:17
	android:exported
		ADDED from AndroidManifest.xml:77:17
	android:authorities
		ADDED from AndroidManifest.xml:76:17
	android:name
		ADDED from AndroidManifest.xml:75:17
provider#com.facebook.NativeAppCallContentProvider
ADDED from AndroidManifest.xml:79:9
	android:authorities
		ADDED from AndroidManifest.xml:79:19
	android:name
		ADDED from AndroidManifest.xml:80:19
service#com.hifive.sync.EventSyncService
ADDED from AndroidManifest.xml:83:9
	android:exported
		ADDED from AndroidManifest.xml:84:18
	android:name
		ADDED from AndroidManifest.xml:83:18
intent-filter#android.content.SyncAdapter
ADDED from AndroidManifest.xml:87:13
action#android.content.SyncAdapter
ADDED from AndroidManifest.xml:88:17
	android:name
		ADDED from AndroidManifest.xml:88:25
meta-data#android.content.SyncAdapter
ADDED from AndroidManifest.xml:91:13
	android:resource
		ADDED from AndroidManifest.xml:92:24
	android:name
		ADDED from AndroidManifest.xml:91:24
service#com.hifive.sync.ProfileSyncService
ADDED from AndroidManifest.xml:97:9
	android:exported
		ADDED from AndroidManifest.xml:98:18
	android:name
		ADDED from AndroidManifest.xml:97:18
service#com.hifive.accounts.GenericAccountService
ADDED from AndroidManifest.xml:115:9
	android:name
		ADDED from AndroidManifest.xml:116:13
intent-filter#android.accounts.AccountAuthenticator
ADDED from AndroidManifest.xml:118:13
action#android.accounts.AccountAuthenticator
ADDED from AndroidManifest.xml:119:17
	android:name
		ADDED from AndroidManifest.xml:119:25
meta-data#android.accounts.AccountAuthenticator
ADDED from AndroidManifest.xml:122:13
	android:resource
		ADDED from AndroidManifest.xml:123:24
	android:name
		ADDED from AndroidManifest.xml:122:24
meta-data#com.facebook.sdk.ApplicationId
ADDED from AndroidManifest.xml:125:9
	android:name
		ADDED from AndroidManifest.xml:125:51
	android:value
		ADDED from AndroidManifest.xml:125:20
activity#com.facebook.LoginActivity
ADDED from AndroidManifest.xml:126:9
	android:name
		ADDED from AndroidManifest.xml:126:19
